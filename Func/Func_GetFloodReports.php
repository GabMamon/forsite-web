<?php

include 'DatabaseConfig.php';
 $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 date_default_timezone_set('Asia/Manila');
 $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
 $re = mysqli_query($con,$Sql_Query);
 $ro = mysqli_fetch_assoc($re);
 
 $expiretime = $ro['Expiretime'];
 $stime = date('Y-m-d H:i:s', strtotime("-$expiretime hours"));
 $etime = date('Y-m-d H:i:s');
 
//all will be selected
 $SQL = "SELECT `FloodID`,`FloodLevel`,`Latitude`,`Longtitude`,`StreetNum`,`Street`,`Barangay`,`DateTime`, `RemarkCategory`,`Remarks`,`ImagePath` FROM flood_reports  WHERE Status =\"V\" AND (DateTime BETWEEN '$stime' AND '$etime')";
 $floodreports = array();
 $result = mysqli_query($con,$SQL);
 $numRows = mysqli_num_rows($result);

if($numRows===0){
     $temp = [
            'FloodLevel'=>0,
            'Latitude'=>"",
            'Longtitude'=>"",
            'Street'=>"",
            'Barangay'=>"",
            'ImgUrl'=>"",
            'RemarkCategory'=>"",
            'Remarks'=>""
            
        ];
        
         array_push($floodreports, $temp);
         echo json_encode($floodreports);
 }
else{   
    while ($row=mysqli_fetch_array($result)){   
		if (!$result){ die ('SQL Error: ' . mysqli_error($con)); } 
        
        $STFULL = $row['StreetNum']." ".$row['Street'];
        $temp = [
            'FloodLevel'=>$row['FloodLevel'],
            'Latitude'=>$row['Latitude'],
            'Longtitude'=>$row['Longtitude'],
            'Street'=>$STFULL,
            'Barangay'=>$row['Barangay'],
            'ImgUrl'=>$row['ImagePath'],
            'DateTime'=>$row['DateTime'],
            'RemarkCategory'=>$row['RemarkCategory'],
            'Remarks'=>$row['Remarks']
        ];
        
         array_push($floodreports, $temp);
    }
  
        echo json_encode($floodreports);
}

mysqli_close($con);
?>