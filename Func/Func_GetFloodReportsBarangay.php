<?php

include 'DatabaseConfig.php';
 $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 date_default_timezone_set('Asia/Manila');
 $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
 $re = mysqli_query($con,$Sql_Query);
 $ro = mysqli_fetch_assoc($re);
 
 $expiretime = $ro['Expiretime'];
 $stime = date('Y-m-d H:i:s', strtotime("-$expiretime hours"));
 $etime = date('Y-m-d H:i:s');


//all will be selected
 $SQL = "SELECT `FloodLevel`,`StreetNum`,`Street`,`Barangay`,`Latitude`,`Longtitude` FROM flood_reports  WHERE Status =\"V\" AND (DateTime BETWEEN '$stime' AND '$etime') AND (`Dup` =`FloodID`) ORDER BY `DateTime` DESC ";
 $result = mysqli_query($con,$SQL);
 $numRows = mysqli_num_rows($result);
 $barangays = array();
 if($numRows===0){
     $temp = [
            'FloodLevel'=>"No flooding reports recorded.",
            'Street'=>"",
            'Barangay'=>"",
            'Latitude'=>"",
            'Longtitude'=>""
        ];
        
         array_push($barangays, $temp);
         echo json_encode($barangays);
 }
 else{

    $stmt = mysqli_query($con, $SQL);
        
    while ($row=mysqli_fetch_array($stmt)){  
        
        if (!is_null($row['FloodLevel'])){
		        switch ($row['FloodLevel']){
		            case 1:
		                $flvl = "Code A";
		                break;
		            case 2:
    		            $flvl = "Code B";
		                break;
		            case 3:
    		            $flvl = "Code C";
		                break;
		            case 4:
    		            $flvl = "Code D";
		                break;
		            case 5:
    		            $flvl = "Code E";
		                break;
		        }
		    }
		$STFULL = $row['StreetNum']." ".$row['Street'];
        $temp = [
            'FloodLevel'=>$flvl.":",
            'Street'=>$STFULL,
            'Barangay'=>$row['Barangay'],
            'Latitude'=>$row['Latitude'],
            'Longtitude'=>$row['Longtitude']
        ];
        
         array_push($barangays, $temp);
    }
  
        echo json_encode($barangays);
  
}

mysqli_close($con);
?>