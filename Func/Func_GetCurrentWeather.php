<?php

include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);


date_default_timezone_set('Asia/Manila');

 function sendBack($ws,$pre,$hum,$rise,$set,$forc,$tem,$ico,$lst) {
     $todayis = date('D, M d Y', strtotime('now'));
       $sendbackjson = [
           'WindSpeed'=>$ws,
           'Pressure'=>$pre,
           'Humidity'=>$hum,
           'Sunrise'=>$rise,
           'Sunset'=>$set,
           'Forecast'=>$forc,
           'Temperature'=>$tem,
           'WeatherIcon'=>$ico,
           'LastUpdated'=>$lst,
           'DateToday'=>$todayis
           ];
    echo json_encode($sendbackjson);
    
    }


//Check weather needs to be updated

$check = "SELECT `windspeed`,`pressure`,`humidity`,`sunrise`,`sunset`,`forecast`,`temp`,`forcasticon`,`lastupdated` FROM weather ORDER BY `lastupdated` DESC
LIMIT 1;";
$result = mysqli_query($con,$check);
$numRows = mysqli_num_rows($result);
if($numRows  == 1){
    $row = mysqli_fetch_assoc($result);
    $exdt = $row["lastupdated"];
    $expdate = $row["lastupdated"];
    $expdate = date('Y-m-d H:i:s', strtotime($expdate.'+1 hour')); //Updates weather every 1 hr
    $datetimenow = date('Y-m-d H:i:s', strtotime('now'));
    $badrec="NO";
    //IF WEATHER NEEDS TO BE UPDATED
    
    if ($datetimenow >= $expdate){
        
        //FETCH NEW DATA
        $url = "http://api.openweathermap.org/data/2.5/weather?q=Mandaluyong,Philippines&APPID=08a0d75913c032f65e6694725f5633e4&units=metric";
        
        $json = @file_get_contents($url);
        $data = json_decode($json);
        
        $wind = $data->wind->speed;
        $pressure= $data->main->pressure;
        $humidity = $data->main->humidity;
        $sunrise = $data->sys->sunrise;
        $sunset =$data->sys->sunset;
        $forecast = $data->weather[0]->description;
        $temp = $data->main->temp;
        $forcasticon = $data->weather[0]->icon;
        $lastupdated = date("Y-m-d H:i:s");
                     
        $Sql_Query = "INSERT INTO weather (windspeed,pressure,humidity,sunrise,sunset,forecast,temp,forcasticon,lastupdated) VALUES('$wind','$pressure','$humidity','$sunrise','$sunset'
        ,'$forecast','$temp','$forcasticon','$lastupdated')";
        
        if (mysqli_query($con,$Sql_Query)){
            sendBack($wind,$pressure,$humidity,$sunrise,$sunset,$forecast,$temp,$forcasticon,$lastupdated);
        }
        else{
            echo "error";
        }
    }
    //NOT YET TO BE UPDATED
    else{
     sendBack($row["windspeed"],$row["pressure"],$row["humidity"],$row["sunrise"],$row["sunset"],$row["forecast"],$row["temp"],$row["forcasticon"],$row["lastupdated"]);
     echo $row["windspeed"];
    }
    
    
   
       

    
}    
mysqli_close($con);

?>