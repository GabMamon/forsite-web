<? 
session_start();

?>
<html>
<head>
    <style type="text/css">
        .card {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
        }
    </style>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title>Sign up</title>
    
</head>
    <body background="Admin\CSS\Images\blues.jpg">
    <div class="jumbotron text-center">
        <img src="Admin\CSS\Images\forsiteiconvar2.png" class="avatar" height="75px">
    </div>
    </br></br>
    <div class="container-fluid">

    <div class="card w-50 bg-light">
        <div class="card-body">
    <?php
         
        include 'DatabaseConfig.php';
        $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

                     
    if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])){
    
        
        $email =mysqli_real_escape_string($con,$_GET['email']);
        $hash = mysqli_real_escape_string($con,$_GET['hash']); 
        
      
        //Seach account to activate
        $sql="SELECT id,email, username, hash, active FROM users WHERE email='".$email."' AND hash='".$hash."' AND active='0'";
        $result = mysqli_query($con,$sql);
        $numRows = mysqli_num_rows($result);
        $row = mysqli_fetch_assoc($result);  
        
        if($numRows > 0){
            // Match Found; Display captcha
            $_SESSION['id']=$row['id'];
            ?>
            <h5 class="card-title text-center">Almost there.</h5>
            <h6>Hello <?echo $row['username'];?>, fill the inputbox below to proceed.</h6>
            <center>
            <img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" />
            <form class="verify" method="POST" action="VerifyAction">
            Type the text: <input type="text" name="captcha_code" size="10" maxlength="6" />
            <a href="#" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a></br></br>
            <input type="submit" value="Submit" class="btn btn-success" />
            </form>
            </center>
            
            <?
            
        }
        
        else{
            // No Match
            echo '<h5 class="card-title text-center">The Link is either invalid or account has already been activated.</h5>';
        }

      
    }
    
    else{
        // Invalid Link
        //echo '<h5 class="card-title text-center">This link is invalid.</h5>';
        
        // Match Found display captcha
            echo '<h5 class="card-title text-left">Redirect</h5>';
            
    }
        
    ?>
     
 
        </div>
    </div>

    </div>
</body>
</html>