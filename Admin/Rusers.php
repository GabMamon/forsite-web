<?php
$u ="";
if(isset($_GET['u'])){
    $u=$_GET["u"];
}

session_start();
if (isset($_SESSION['stat'])){
  $uid = $_SESSION['id'] ;
  $ufullname = $_SESSION['fullname'] ;
  $usrname = $_SESSION['username'];
  $accslvl= $_SESSION['accesslevel'];
  
  if(!strcmp($accslvl,"ADMIN")==0){
    $condtion = "$accslvl";
    $contionz = "AND (`Barangay` = '$condtion')";
    $identifier = "(".$accslvl.")";
  }else{
    $condtion = "";
    $contionz = "";
    $identifier = "";
  }
  
 include 'Prosusers.php';
 
?>
<html>
	<head>
	    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <link href="CSS/simple-sidebar.css" rel="stylesheet">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.css"/>
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato"/>

                <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
                
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
				
	<title>
      Forsite Admin| Users Reports
    </title>
	<style type="text/css">
      .navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
      .username{
          margin-right: 40px;
      }
      .nav-link[data-toggle].collapsed:after {
        content: "▾";
      }
      .nav-link[data-toggle]:not(.collapsed):after {
        content: "▴";
      }
      td.details-control {
        cursor: pointer;
      }
      td.details-control:before {
        content: "\f06e";
        font-family: FontAwesome;
        position:center;
      }
      
      tr.shown td.details-control {
      }
      tr.shown td.details-control:before {
        content: "\f070"; 
        font-family: FontAwesome;
        position:center;
      }
  </style>
						</head>
						<body>
							<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
							    <a href="#menu-toggle" class="btn btn-outline-secondary" id="menu-toggle">
									<i class="fa fa-bars"></i>
								</a>
								<a class="navbar-brand" href="http://forsitefloodapp.xyz/Admin/admin-home">
									<img src="https://forsitefloodapp.000webhostapp.com/Admin/CSS/Images/forsiteiconvar2.png" height="40">
									</a>
									<span class="navbar-text">
									Users Report 
								</span>
									<div class="collapse navbar-collapse" id="navbarText">
										<ul class="navbar-nav ml-auto">
											<span class="navbar-text username">
												<i class="fa fa-user"></i> Hello <?php echo $ufullname; ?> 
											</span>
											<li class="nav-item">
												<a type="button" class="btn btn-outline-secondary" href="logout">Log-out <i class="fa fa-sign-out"></i></a>
											</li>
										</ul>
									</div>
								</nav>
								<div id="wrapper">
									<div id="sidebar-wrapper">
										<ul class="sidebar-nav nav-pills">
											
											</li>
											<li class="nav-item">
												<a href="admin-home" class="nav-link rounded-0">Dashboard</a>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#CurReportSub" data-toggle="collapse">Flood Monitoring</a>
												    <div class="collapse" id="CurReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodmap">Flood Map</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodreports">Flood Incidents</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0 active" href="#ReportSub" data-toggle="collapse">Reports</a>
												    <div class="collapse" id="ReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rflood">Flood Reports</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0 active" href="Rusers">User Reports</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<?if(strcmp($accslvl, "ADMIN")==0){?>
										<li class="nav-item">
										    <a href="SettingAdmin" class="nav-link rounded-0">Settings</a>
										    </li>
										    <?}?>	
										</ul>
							
									</div>
									<div id="page-content-wrapper">
										<div class="container-fluid">
										    <div class="row col-12">
                                                 <div class="dropdown col-auto">
                                                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Report Range
                                                      </button>
                                                      <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#" onclick="TodayIs()">Today</a>
                                                        <a class="dropdown-item" href="#" onclick="YesterdayIs()">Yesterday</a>
                                                         <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#" onclick="ThisWeekIs()">This Week</a>
                                                        <a class="dropdown-item" href="#" onclick="LastWeekIs()">Last Week</a>
                                                         <div class="dropdown-divider"></div> 
                                                        <a class="dropdown-item" href="#" onclick="ThisMonthIs()">This Month</a>
                                                        <a class="dropdown-item" href="#" onclick="LastMonthIs()">Last Month</a>
                                                         <div class="dropdown-divider"></div> 
                                                        <a class="dropdown-item" href="#" onclick="ThisQuarterIs()">This Quarter</a>
                                                        <a class="dropdown-item" href="#" onclick="LastQuarterIs()">Last Quarter</a>
                                                         <div class="dropdown-divider"></div> 
                                                        <a class="dropdown-item" href="#" onclick="ThisYearIs()">This Year</a>
                                                        <a class="dropdown-item" href="#" onclick="LastYearIs()">Last Year</a>
                                                      </div>
                                                    </div> 
                                            <div class="col-4">
                                                <form action="Rusers" method="post">
                                                    <div class="input-group input-daterange">
                                                    <div class="input-group-addon px-3 fa fa-calendar border"></div>
                                                        <input type="text" class="form-control" name="startDate" id="sDid">
                                                    <div class="input-group-addon px-3">to</div>
                                                        <input type="text" class="form-control"  name="endDate" id="eDid">
                                                        <input type="hidden" name="hidden" id="hiddenField" value="<?php echo $accslvl ?>">
                                                        <input type="submit" value="Show" name="Show" class="btn mx-1" id="submitDate">
                                                    </div>
                                                        
                                                </form>
                                                </div> <br />
                                            </div>
    <?php
    $TotUserSQL = "SELECT UserLogEmail FROM user_logs WHERE (UserLogDate BETWEEN '$startDateVal' AND '$endDateVal') GROUP BY UserLogEmail";
    $ParUserSQL = "SELECT COUNT(*) AS COUNT FROM flood_reports WHERE (`DateTime` BETWEEN '$startDate' AND '$endDate') $contionz GROUP BY UserReported";
    
    $res1 = mysqli_query($con, $TotUserSQL);
    $total = mysqli_num_rows($res1);
    
    $res2 = mysqli_query($con, $ParUserSQL);
    $participate = mysqli_num_rows($res2);
    ?>
    
    
										  
	<div class="row">
	    <div class="card-group col-12">
	    <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Users Participation <?echo $identifier?></h5>
                        <canvas id="participationChart"></canvas>
                </div>
            </div>
        </div>
        
<script>

        var ttl = <?php echo $total ?>     
        var par = <?php echo $participate ?>  
        var npr = ttl - par;
        console.log(ttl);
        console.log(par);
        
        var pieChart = document.getElementById("participationChart");
                                                
        Chart.defaults.global.defaultFontSize = 14;
                                                    
        var data = {
            labels: [
                "Participating",
                "Not participating"
            ],
            datasets: [
                {
                data: [par, npr],
                 backgroundColor: ["#ADD8E6", "#949393"]
                }]
        };

        var myPieChart = new Chart(pieChart,{
            type: 'pie',
                data: data,
                options: {
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                            var dataset = data['datasets'][0];
                            var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
                            return "" + data['datasets'][0]['data'][tooltipItem['index']] + ' (' + percent + '%)';
                            }
                        }
                }
        }
    });
    
        $(' .input-daterange').datepicker({
        endDate: "0d",
        todayBtn: "linked"
    });
                                                    
</script>        
        
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Users w/ most validated reports <?echo $identifier?></h5>
                    <canvas id="validChart"></canvas>
                </div>
            </div>
        </div>
 
<?php
    $SortValidateSQL = "SELECT `UserReported`,COUNT(*) AS Num FROM flood_reports WHERE 
(`DateTime` BETWEEN '$startDate' AND '$endDate') AND 
(`Status` = 'V' OR `Status` = 'V-D') 
$contionz
GROUP BY `UserReported` ORDER BY Num DESC LIMIT 5";

    $users = array();
    $stmt = $con->prepare($SortValidateSQL);
    $stmt->execute();
    $stmt->bind_result($usrnm,$num); 
     while($stmt->fetch()){    
        $temp = [
            'UserReported'=>$usrnm,
            'Num'=>$num
        ];
        
         array_push($users, $temp);
    }
    $usrjson = json_encode($users);
    
?>
<script>
    var barjson;
    barjson = <?php echo $usrjson ?>;
    var barlbl = barjson.map(function(e) { 
        return e.UserReported; });
    var bardta = barjson.map(function(e) { 
        return e.Num; });

        var chart = document.getElementById("validChart");
                                                
        Chart.defaults.global.defaultFontSize = 12;
                                                    
        var data = {
            labels: barlbl,
            datasets: [
                {
                label: "Valid report(s) sent.",
                data: bardta,
                backgroundColor: "#7EC0EE"
                }]
        };

        var mychart = new Chart(chart,{
            type: 'horizontalBar',
                data: data,
                options: {
                    scaleShowValues: true,
                    scales:{
                        xAxes:[{
                            ticks: {
                                suggestedMin: 0,
                                beginAtZero: true,
                                fontSize: 8 
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                callback: function(value) {
                                    if (value.length > 6) {
                                        return value.substr(0, 6) + '...'; 
                                    } else {
                                        return value
                                    }
                                }
                            }    
                        }]
                    },
                    tooltips:{
                      enabled: true,
                            mode: 'label',
                            callbacks: {
                              title: function(tooltipItems, data) {
                                var idx = tooltipItems[0].index;
                                return data.labels[idx];
                              },
                              label: function(tooltipItems, data) {
                                var gram = "";
                                if (tooltipItems.xLabel == 1){
                                    gram = "";
                                }else{
                                    gram = "s";
                                }
                                return tooltipItems.xLabel + ' report'+gram+' sent.';
                              }
                            }  
                    }
        }
    });
                                                    
</script>       
        
        
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Users w/ most invalidated reports <?echo $identifier?></h5>
                    <canvas id="invalidChart"></canvas>
                </div>
            </div>
        </div>
        </div>
	</div>
	
<?php
    $SortInvalidateSQL = "SELECT `UserReported`,COUNT(*) AS Num FROM flood_reports WHERE 
(`DateTime` BETWEEN '$startDate' AND '$endDate') AND 
(`Status` = 'IV') 
$contionz
GROUP BY `UserReported` ORDER BY Num DESC LIMIT 5";
    
    $users = array();
    $stmt = $con->prepare($SortInvalidateSQL);
    $stmt->execute();
    $stmt->bind_result($usrnm,$num); 
     while($stmt->fetch()){    
        $temp = [
            'UserReported'=>$usrnm,
            'Num'=>$num
        ];
        
         array_push($users, $temp);
    }
    $usrjson = json_encode($users);
    
?>
<script>
    var barjson;
    barjson = <?php echo $usrjson ?>;
    var barlbl = barjson.map(function(e) { 
        return e.UserReported; });
    var bardta = barjson.map(function(e) { 
        return e.Num; });

        var chart = document.getElementById("invalidChart");
                                                
        Chart.defaults.global.defaultFontSize = 12;
                                                    
        var data = {
            labels: barlbl,
            datasets: [
                {
                label: "Invalid report(s) sent.",
                data: bardta,
                backgroundColor: "#ED4337"
                }]
        };

        var mychart = new Chart(chart,{
            type: 'horizontalBar',
                data: data,
                options: {
                    scaleShowValues: true,
                    scales:{
                        xAxes:[{
                            ticks: {
                                suggestedMin: 0,
                                beginAtZero: true,
                                fontSize: 8 
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                callback: function(value) {
                                    if (value.length > 6) {
                                        return value.substr(0, 6) + '...'; 
                                    } else {
                                        return value
                                    }
                                }
                            }    
                        }]
                    },
                    tooltips:{
                      enabled: true,
                            mode: 'label',
                            callbacks: {
                              title: function(tooltipItems, data) {
                                var idx = tooltipItems[0].index;
                                return data.labels[idx];
                              },
                              label: function(tooltipItems, data) {
                                var gram = "";
                                if (tooltipItems.xLabel == 1){
                                    gram = "";
                                }else{
                                    gram = "s";
                                }
                                return tooltipItems.xLabel + ' report'+gram+' sent.';
                              }
                            }  
                    }
        }
    });
                                                    
</script>   	
	
	<br>
										    
	<div class="row">
	 <div class="col-12">
       <div class="card">
                    <div class="card-header">
                        <h4>Active Users Details</h4>
                            <button class="btn-primary" onclick="GeneratePDF()">Export as PDF</button>
                        </div>
                    <div class="card-body">
                			<?php
                        		echo '<table class="table table-hover" id="userstable">';
                        		echo "<thead>";
                                echo "<tr>";
                                echo "<th scope=\"col\" >#</th>\n";
                                echo "<th scope=\"col\">First Name</th>\n";
                                echo "<th scope=\"col\">Last Name</th>\n";
                                echo "<th scope=\"col\">Username</th>\n";
                                echo "<th scope=\"col\">Email Address</th>\n";
                                echo "<th scope=\"col\">Date Created</th>\n";
                                echo "<th scope=\"col\" class=\"text-center\">Account Status</th>\n";
                                echo "<th scope=\"col\">Last Login</th>\n";
                                echo "<th scope=\"col\"></th>\n";
                                echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                        		echo "</tbody>";
                                echo "</table>";
                        	?>
                    </div>
                </div>
    </div>
    </div>
    </br>
    </br>
    
    <div class="row" id="target">
	 <div class="col-12">
       <div class="card">
                    <div class="card-header">
                        <h4>All Users Details</h4>
                        <!--<button class="btn-primary" onclick="GeneratePDF()">Export as PDF</button> -->
                        </div>
                    <div class="card-body">
        			        <?php
                        		echo '<table class="table table-hover" id="alluserstable">';
                        		echo "<thead>";
                                echo "<tr>";
                                echo "<th scope=\"col\" >#</th>\n";
                                echo "<th scope=\"col\">First Name</th>\n";
                                echo "<th scope=\"col\">Last Name</th>\n";
                                echo "<th scope=\"col\">Username</th>\n";
                                echo "<th scope=\"col\">Email Address</th>\n";
                                echo "<th scope=\"col\">Date Created</th>\n";
                                echo "<th scope=\"col\" class=\"text-center\">Account Status</th>\n";
                                echo "<th scope=\"col\">Last Login</th>\n";
                                echo "<th scope=\"col\"></th>\n";
                                echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                        		echo "</tbody>";
                                echo "</table>";
                        	?>
                </div>
                </div>
    </div>
    </div>   

     


		
		
		</div>

        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/sl-1.2.6/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script>
function TodayIs(){
        var now = moment().format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = now;
        document.getElementById("eDid").value = now;
        
        document.getElementById('submitDate').click();
    }
    
    function YesterdayIs(){
        var yes = moment().subtract(1, 'days').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = yes;
        document.getElementById("eDid").value = yes;
        
        document.getElementById('submitDate').click();
    }
    
    function ThisWeekIs(){
        var tws = moment().startOf('week').format("MM/DD/YYYY");
        var twe = moment().endOf('week').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = tws;
        document.getElementById("eDid").value = twe;
        
        document.getElementById('submitDate').click();
    }
    
    function LastWeekIs(){
        var lws = moment().subtract(1, 'week').startOf('week').format("MM/DD/YYYY");
        var lwe = moment().subtract(1, 'week').endOf('week').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = lws;
        document.getElementById("eDid").value = lwe;
        
        document.getElementById('submitDate').click();
    }
    
    function ThisMonthIs(){
        var tms = moment().startOf('month').format("MM/DD/YYYY");
        var tme = moment().endOf('month').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = tms;
        document.getElementById("eDid").value = tme;
        
        document.getElementById('submitDate').click();
    }
    
    function LastMonthIs(){
        var lms = moment().subtract(1, 'month').startOf('month').format("MM/DD/YYYY");
        var lme = moment().subtract(1, 'month').endOf('month').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = lms;
        document.getElementById("eDid").value = lme;
        
        document.getElementById('submitDate').click();
    }
    
    function ThisQuarterIs(){
        var tqs = moment().startOf('quarter').format("MM/DD/YYYY");
        var tqe = moment().endOf('quarter').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = tqs;
        document.getElementById("eDid").value = tqe;
        
        document.getElementById('submitDate').click();
    }
    
    function LastQuarterIs(){
        var lqs = moment().subtract(1, 'quarter').startOf('quarter').format("MM/DD/YYYY");
        var lqe = moment().subtract(1, 'quarter').endOf('quarter').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = lqs;
        document.getElementById("eDid").value = lqe;
        
        document.getElementById('submitDate').click();
    }
    
    function ThisYearIs(){
        var tys = moment().startOf('year').format("MM/DD/YYYY");
        var tye = moment().endOf('year').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = tys;
        document.getElementById("eDid").value = tye;
        
        document.getElementById('submitDate').click();
    }
    
    function LastYearIs(){
        var lys = moment().subtract(1, 'year').startOf('year').format("MM/DD/YYYY");
        var lye = moment().subtract(1, 'year').endOf('year').format("MM/DD/YYYY");
        
        document.getElementById("sDid").value = lys;
        document.getElementById("eDid").value = lye;
        
        document.getElementById('submitDate').click();
    }
    
    function GeneratePDF(){
        //Process Date
        var date1 = '<? echo $startDateVal ?>';
        var date2 = '<? echo $endDateVal ?>';
        var dateform;
        var timenow = moment().format("M/D/YYYY h:mm A")
        if (moment(date1).isSame(date2, 'year')){
            dateform = moment(date1).format('MMMM D')+'-'+moment(date2).format('MMMM D, YYYY');
        }else{
            dateform = moment(date1).format('MMMM D YYYY')+'-'+moment(date2).format('MMMM D YYYY');
        }
         var docDefinition = { 
                footer: {
                        alignment:'right',
                        margin: [0,5,20,0],
                        stack: [
                          'Generated by: Gabriel Mamon'
                        ],
                        fontSize: 10
                    },
             	content: [
             	    {image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABNcAAADxCAYAAADoUVEcAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAKGWlDQ1BJQ0MgUHJvZmlsZQAASMe1VmdUU9kaPffe9EJLQDqh19BLAOkdRIp0UQlJgFAChARQsSMqOIKIiKAiyKiAA45KkUFFRLENChZU1AkyCKjPwYINlXcjP3TeW+v9erPXOufuu9f3nfOd7/w4GwDyJEABoyNDIBKG+3kyYmLjGPj7AAFqQBHoAS02JycL/Deg7/Td3fm/W0zpbPzB5XHr68iWfM9Pf17d5EgF/xtyXF4OB13OG+UrE9HNUd6DcnpiRLgXym8DQKBwM7hcAIgSVN+a/C2GlCKNSf4hJl2cwUf1AqmewWPnoLwM5XqJaVkilJ+S6sL53Mvf+A+5Ih4HXY/0ENUpuWIeuhdJ2pcteSJpLll6fjonSyjlhSh34KSw0RhyN8rN5uv/Bq0caQMDfLzsrRzt7ZnWTCtGYjqbk8bI4bDTpav+vyG9q3mmdxAAWbS29hscsTB3XsNIJywgAVlABypAE+gCI8AE1sABOAN34AMCQQiIALFgOeCAFJABhCAPFIANoAiUgDKwG1SDWtAAGkELOAE6QDc4Dy6Ba+AmuANGgASMg+dgGrwDsxAE4SEqRINUIC1IHzKFrCEW5Ar5QIugcCgWSoCSIQEkhgqgTVAJVA5VQ3VQI/QrdBo6D12BBqH70Cg0Bb2GPsEITIHpsAZsAFvALNgDDoIj4GVwMpwNr4IL4R1wFVwPH4Pb4fPwNfgOLIGfwzMIQMiIEqKNMBEW4oWEIHFIEiJE1iLFSCVSj7QgXUg/cguRIC+QjxgchoZhYJgYZ4w/JhLDwWRj1mK2Y6oxRzHtmD7MLcwoZhrzFUvFqmNNsU7YAGwMNhmbhy3CVmIPY9uwF7F3sOPYdzgcTglniHPA+eNicam41bjtuP24VlwPbhA3hpvB4/EqeFO8Cz4Ez8aL8EX4vfhj+HP4Ifw4/gOBTNAiWBN8CXEEAWEjoZLQRDhLGCJMEGaJckR9ohMxhMglriSWEhuIXcQbxHHiLEmeZEhyIUWQUkkbSFWkFtJF0kPSGzKZrEN2JIeR+eT15CrycfJl8ij5I0WBYkLxosRTxJQdlCOUHsp9yhsqlWpAdafGUUXUHdRG6gXqY+oHGZqMuUyADFdmnUyNTLvMkMxLWaKsvqyH7HLZVbKVsidlb8i+kCPKGch5ybHl1srVyJ2WG5abkafJW8mHyGfIb5dvkr8iP6mAVzBQ8FHgKhQqHFK4oDBGQ2i6NC8ah7aJ1kC7SBun4+iG9AB6Kr2E/gt9gD6tqKBoqxilmK9Yo3hGUaKEKBkoBSilK5UqnVC6q/RpgcYCjwW8BdsWtCwYWvBeWU3ZXZmnXKzcqnxH+ZMKQ8VHJU1lp0qHyiNVjKqJaphqnuoB1YuqL9Toas5qHLVitRNqD9RhdRP1cPXV6ofUr6vPaGhq+GlkaezVuKDxQlNJ010zVbNC86zmlBZNy1WLr1WhdU7rGUOR4cFIZ1Qx+hjT2ura/tpi7TrtAe1ZHUOdSJ2NOq06j3RJuizdJN0K3V7daT0tvWC9Ar1mvQf6RH2Wfor+Hv1+/fcGhgbRBlsMOgwmDZUNAwxXGTYbPjSiGrkZZRvVG902xhmzjNOM9xvfNIFN7ExSTGpMbpjCpvamfNP9poNmWDNHM4FZvdkwk8L0YOYym5mj5krmi8w3mneYv7TQs4iz2GnRb/HV0s4y3bLBcsRKwSrQaqNVl9VraxNrjnWN9W0bqo2vzTqbTptXtqa2PNsDtvfsaHbBdlvseu2+2DvYC+1b7Kcc9BwSHPY5DLPorFDWdtZlR6yjp+M6x27Hj072TiKnE05/OTOd05ybnCcXGi7kLWxYOOai48J2qXORuDJcE1wPukrctN3YbvVuT9x13bnuh90nPIw9Uj2Oebz0tPQUerZ5vvdy8lrj1eONePt5F3sP+Cj4RPpU+zz21fFN9m32nfaz81vt1+OP9Q/y3+k/HKARwAloDJgOdAhcE9gXRAlaElQd9GSRySLhoq5gODgweFfww8X6iwWLO0JASEDIrpBHoYah2aG/heHCQsNqwp6GW4UXhPcvoS1ZsaRpybsIz4jSiJFIo0hxZG+UbFR8VGPU+2jv6PJoSYxFzJqYa7GqsfzYzjh8XFTc4biZpT5Ldy8dj7eLL4q/u8xwWf6yK8tVl6cvP7NCdgV7xckEbEJ0QlPCZ3YIu549kxiQuC9xmuPF2cN5znXnVnCneC68ct5EkktSedJkskvyruSpFLeUypQXfC9+Nf9Vqn9qber7tJC0I2lz6dHprRmEjISM0wIFQZqgL1MzMz9zMMs0qyhLku2UvTt7WhgkPJwD5SzL6RTR0QfmuthIvFk8muuaW5P7IS8q72S+fL4g//pKk5XbVk6s8l3182rMas7q3gLtgg0Fo2s81tSthdYmru1dp7uucN34er/1RzeQNqRt+H2j5cbyjW83RW/qKtQoXF84ttlvc3ORTJGwaHiL85barZit/K0D22y27d32tZhbfLXEsqSy5PN2zvarP1n9VPXT3I6kHQOl9qUHynBlgrK7O912Hi2XL19VPrYreFd7BaOiuOLt7hW7r1TaVtbuIe0R75FULarq3Ku3t2zv5+qU6js1njWt+9T3bdv3fj93/9AB9wMttRq1JbWfDvIP3qvzq2uvN6ivPIQ7lHvoaUNUQ//PrJ8bD6seLjn85YjgiORo+NG+RofGxib1ptJmuFncPHUs/tjNX7x/6WxhttS1KrWWHAfHxcef/Zrw690TQSd6T7JOtpzSP7WvjdZW3A61r2yf7kjpkHTGdg6eDjzd2+Xc1fab+W9HurW7a84onik9SzpbeHbu3KpzMz1ZPS/OJ58f613RO3Ih5sLtvrC+gYtBFy9f8r10od+j/9xll8vdV5yunL7Kutpxzf5a+3W7622/2/3eNmA/0H7D4UbnTcebXYMLB88OuQ2dv+V969LtgNvX7iy+M3g38u694fhhyT3uvcn76fdfPch9MDuy/iH2YfEjuUeVj9Uf1/9h/EerxF5yZtR79PqTJU9Gxjhjz//M+fPzeOFT6tPKCa2Jxknrye4p36mbz5Y+G3+e9Xz2RdG/5P+176XRy1N/uf91fTpmevyV8NXc6+1vVN4ceWv7tncmdObxu4x3s++LP6h8OPqR9bH/U/Snidm8z/jPVV+Mv3R9Dfr6cC5jbu4Hb2KO2hLGd1/izUtii9NFDKlh8cpMzxQLGUuy2Bweg8mQmph/zKck7gWgYzMAyg++ayhC5z/zvu0//eXfAH/PQ5TQYYNKDd+1zHoAWDOoXpbDT/6meYVHMH7oAzOcl8QT8gToUaP4vDy+IBk9v4DLF/EzBQy+gPG3Nv1zfu27Zxbx8kXf6szMWinkJ6eIGAECEU8oYEsrYqd/ux2htMacTKGIL84wY1hbWjoCkJNkYz3fKQrqnbF/zM29MQAAXwHAl9K5udm6ubkvaC+QEQB6xP8GCj/Z9mmahTMAAAAJcEhZcwAADsEAAA7BAbiRa+0AAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMjHxIGmVAADAXElEQVR4XuydB3gTR/r/kVzpEAgkIb2HXKoTAsSWLLliWihOSOVSjrskP1KoqRjbarYhkB7SQ7VlG1MMmOZeKamElnK5+1/P3aVdGgm8//ddrUCIMZZs7Wolve/zfJ6VR9LuzDuzM9qv35npBgCdgo2NjY2NLVxNNO4xDMMwDMMwDMOIECb6AhsbGxsbW7iaaNxjGIZhGIZhGIYRIUz0BTY2NjY2tnA10bjHMAzDMAzDMAwjQpjoC2xsbGxsbOFqonGPYRiGYRiGYRhGhDDRF9jY2NjY2MLVROMewzAMwzAMwzCMCGGiL7CxsbGxsYWricY9hmEYhmEYhmEYEcJEX2BjY2NjYwtXE417DMMwDMMwDMMwIoSJvsDGxsbGxhauJhr3GIZhGIZhGIZhRAgTfYGNja3rhveSLicnR38ysrOzYxMTEy/uDGazeQh+P0p0XjeYDZ0rN2xsbG7zHvMYhmEYhmEYhmHaQ5joC2xsbH6ZLi0trWdmZmYfN6mpqX0NBsNdRqPxRTy+2h74/uvJycn/Q773F/zuXjzH697n9AQ/93v83LmeeSMSEhJi5LyzsUWcicY9hmEYhmEYhmEYEcJEX2BjY3OZ0WiMRuKHDx/enaCIMVmwut8N/j0T2Yn85MUv+P7hjsDPQSc5IjqfJ/iZX5CfEe+85SMP4GeOlgO5D+lF5cRjPAlwFBmHbuDoN7awMtG4xzAMwzAMwzAMI0KY6AtsbBFmehKSiKFDh8YajcZLDQZDmslkSsfXjyAUIfYlga+/Tk5OJtHqiBci8UvLeOefhLpfPcrZiliRR/C90eiLq4YNGyZFvMmCGxtbyJpo3GMYhmEYhmEYhhEhTPQFNrYwtiij0RhNa53h8VzkwuTk5AykAV/vwCPxV6QrEWXhyL/QP++Tj5KSku5NTEwcSr5DaMppHB7Jpyy6sYWEicY9hmEYhmEYhmEYEcJEX2BjC3GjaYw6En1oiuMNN9wwODEx8XwSg5KTk+fgkdY5exOPFIUmEpIY3/kv+vEdPL6BPEE+Jl+PGDFiEPkf64E3VWDTnInGPYZhGIZhGIZhGBHCRF9gYwsx09F0zpEjR/Y2mUznmM3ma5FhRqPx/5KTk/PwWIXHH/D4Kx5DcQpnqCBNLcUj+XoLHh9GjAaD4Xr8W9pUAY/x8i6mbGxBM9G4xzAMwzAMwzAMI0KY6AtsbCFgUmRaYmJif6PRONxgMNyMx8eRcoSmdVJE1SE8ikQgRh0OYR1QdCDVxRrkKayne/DvFIpsw7/jkWiqS1eVsrGpY6Jxj2EYhmEYhmEYRoQw0RfY2DRm0oYDtE5aYmJiAkVCISOSk5NnGl2L7u9G/iELOoy2+Qrr6lM8LsLjbLPZPAVf34D1eQVFH1I9Y32z2MamqInGPYZhGIZhGIZhGBHCRF9gY9OA6WiBfIpMS0pKGmkwGGg9r/nI9uTk5BqkHhGJN0xo8f+wTuuQjVjP87Ge5yHXpKWl9eQNEtiUMtG4xzAMwzAMwzAMI0KY6AtsbEEwHa3FNWbMmB5JSUmnGwyGi4xG4yPJycnP4bEW+Qmh9bxEAg0TBlD9ynW8Beu/EI8P4t/n0hRSilqU2wkbW5dNNO4xDMMwDMMwDMOIECb6AhubiqY3utbeuk4WUyyIkwQWPP4Tjz8ivAlBhIF1/z0eae28Tfj6bZPJdFdqaurZCQkJPTiija2rJhr3GIZhGIZhGIZhRAgTfYGNTWGTBLWUlJTz8Xi3wWB4CY+7kL8i/01OTv7FLbIwDLaJn/D4GR7rsa2U4fH+kSNHniFHs/H6bGx+m2jcYxiGYRiGYRiGESFM9AU2tkAbTfkkMYTW0zIajanJyclbkDaE1tz6H4koDOMDJLySCNuEvIGvRycmJhppjTZsZnpXa2NjO7mJxj2GYRiGYRiGYRgRwkRfYGMLkOkuvPDCuMTERIpQm5ycnLzaYDB8hK+/QGhtLZ7uyXQKbD+H8UhTR/+M/BH/rkxJSbkmKSnpPHwdLbc/NjahicY9hmEYhmEYhmEYEcJEX2Bj64pRlBpFEhkMhsuMRuMC5LPk5OR/IT/LoohQMGGYLnAI29ZfsM3tN5lMi2mXWV6bja09E417DMMwDMMwDMMwIoSJvsDG1gnTJSQkxAwfPvyUpKSkSUajcXeyS1D7AeEINUYtqK1Rm/t/yINms3kUtsV4ap+uZsrGxuIawzAMwzAMwzC+I0z0BTY2H01nNBqjU1NTz0hKSjIkJycvMxgMNEXvG3zNEWpMsPkB2+LXeFxtMpnM+PpcjmZjIxONewzDMAzDMAzDMCKEib7AxtaRUZSayWS6xGg05iIfk6CG8DpqjOagdon8j9ppUlLSfchZ+Jqi2dgi1ETjHsMwDMMwDMMwjAhhoi+wsYmM1lIbNmxYH6PReJrJZCJRbS9CEWosqHmRkZEB48ePh7Fjx9anpKQswLR8f0C/WiZPnrxzypQpdA7Acwivw/jFEfQrCcB/w+MSbMMXYLPmHUYj0ETjHsMwDMMwDMMwjAhhoi+wsXma0WiMHj58ePfk5GQj8pPBYPgFjxE/7ZMENDxuRP+sHTVq1NJp06bdkpiYeOr1118/gPxF0X3yNERa78tv6Lt4zhg6T2pqal86N/H73/8+Py0trZyui+n1yAl5YzqEBOFS9OF0s9k8hNo4+pwtQkw07jEMwzAMwzAMw4gQJvoCGxuatOOn0Wi8MDk5+QE8UrTP9x7iRKRwCPkZ+RzZfXP2za0TJkzINJvNQ/F4JokyFNGH/iJBTE3T03WnT58eN378+LNMJtPlt91224NZWVk7MU978O8v5XxTpJaoXIwM+uvfeHyYdrclURR9q3ZdsqlsonGPYRiGYRiGYRhGhDDRF9gi1nSZmZlxo0eP7m80GhORbchnyREy7dNkMtHxJ1Ny8tf4+q8ZGRkHzWbzw/g6a8yYMWcPHTo0VvaTZo2i3ZC+mN9r09LSJmNdLsMyHMBy/RPhjSZOgrwZxwt4HIHHM2XRlC0MTTTuMQzDMAzDMAzDiBAm+gJbRJmOph0OHz78FKPR+BvktwaD4dXk5OT/h0SCEHPEZDL9gPwlPT19Z2Zm5mtjRo168Ld33DHy9ttv70P+cbkpZE0STJEss9k8E9mM7MFy/9tsTqHpvSKfMMnJFXgvpKalpQ3CI08ZDTMTjXsMwzAMwzAMwzAihIm+wBYZRqJLamrq2SaTaVyyayH9BqPR+D8PgSFsoXKazeYvsexV6IdXxo0bd9fUqVNPk9dIC5jl7NkT+4CzptfsNY293dzz+poO8fw8kePcE4s3Z1eFPh1N9Z0yZco1KSkps8eMGbeSyo+vKaLtB28fRTgUrfmjwWB4BtvKGGwrQ+h+kf3IFuImGvcYhmEYhlEO/F0Vj5ypMPGiazMMw3QVYaIvsIW3kYBEYoHRaLwTB6ElCE0bpLXFRCJDWEGL/2O5qxFHWlrao3g884477ugpuyYgZtu6d8Csze/deGdxS1bmG9XTkp9bl5P6YlWe+fnKPMOiNXkji1bnXWcvzrvW6sSjM+9ahI5uhhc48xIXrskzP1spkYnfG/vK5mm3L2scN3vLgaycmv0D8UbtktBGUx6nTZvWF/1xxpgxY55CPywwmUw16BOh3yIV9MthhNbbex6PU5BzAy3AsqlvonGPYRiGYZiug7+Z2hPRLkCEv7cCCF1DdG0W3RiG6RLCRF9gC08zGo3RJpMpAQeY8fj6beQLfB32oprZbKb11D7C18+NGjUqNykp6dpALFw/vaT+1Dve2HxD2gulN6QuKstIX1SWd9mTr+ddUVD2yg0vbmm+8vlt+y54ZtuXZzs2/3pu4VbAI5xp2wRDrBvhDMsGOD1/A5yWvxFOs2yE0yU2SOlD8PVZ1k1wjq0KzkXOt2+Eiwqrvrxy0faDI1+q23d14YYlVz71Vr65oDjvt+9snZG8aM0NN728ftj/Ld18npw1f03avCI9PT1h7Oixuegnimj7MyL0ZySC9wptDnFQvm9uJVGSRbbQNdG411mwXdBDxOlMSMEPWQzDMAGA+lPEW8hSQ0TzF5HoFtZjAZUPEY2BXYHHTyZiESb6Alt4mRypNhg7xNFGo7EajyQ0hf0mBVjmQ+np6Z8iNOU1IzExkTZq6PT6WTk17/Wb/NKmS6a9uf2pW9+sXjRiwZplQ+2r68+e76w/K2/1exfa1sLg3DXQ37IJetuqobutDmJtTRBjbYZoa4tElPRa/tvWAnokyt4KUXREKC2GsCD5zRBrQfKbIN7SAN0ttdArfzsMsFTBkLw1cKFtDVyxqPLbIbbyhgtt5fXDilavznqxctGdb22dfutbNZdat304OAfAr0X5qa2kpKScj9yCPitEH75vMpkiYkMLHyEx+hNkFfplIt1XLLKFnonGPX/BNuD+0XoOImorjHahOuMHBIZhmE5A/SfiFqi0KKT5iqfgFlZjApUHUeL3CY+fTMQiTPQFtrAw3ZVXXtmT1lQzGAz3YEe4AvkQCXeh5IjRtW7csvT09JlZWVmZ6IO+5A+XW3y3ZzcejLt/RUP/nPL6B8Y/U1x4XYHz7ctsa6t/Y1v39eVFW345I3/T4f65m6HX/C3QK7caeuTXQaylEaKtraC37QC9fQfo7C1IUzs0gs7RBN0KGqEb/t3NgX/L6Xo86m1NEIWQGOcS6JogxtYAsSTa2auhu2MbdC+ogh4Fm6CPYxOcVlB1+DxH5S+X2Nf+/TeONTXGotXOW19et+ietzcbpjl39c12gs8iEAlG6MeBmZmZY1NSUiz4ehf6NCKmDvsC+oM2+9iLxyWJiYkJJLKh23h30RAx0bjnL1j/JKwJ2wcTEvADAsMwjB9Qn4mEuqDWHmEjtFH+ESX/8cfjJxORCBN9gS2kTYcP/PFJSUnDDAZDIXZ+tEnBn5CfPDrFsISmMZrN5neRB/H1BePGjevd2aiix9a03HDzKxvsiQvXVA0rWv3XCy2l356aW/FTP+tm6Jm/HXpa6yHe0ghxtmaIJeHL4opIi7JSNFor6CWBrQV0BY1IPVInoNZ1LMRjER2PpesdmOaow2MdRNvc1EOUrcGFowGiC+hYh3/XQJS9FmIxLd5eD73w71PyN8OQvPWHLrZU/G+obc1Hw4rWrZ/0+tbc6RVNI6XNEXw0WsR/zJgxA7H9DEdoyuifEY5kO8Z36JfdSHliYuL506ZNi5Fdx6ZhE417/oD1rvQPV0YdThfVL8MwDOMC+8lwiVLzh5AW2jDPavzzj8dPJuIQJvoCW0iaLiEhISYlJWUAdniLkI/wgf9rPIa9EEKiWmpq6v+j6Z9Y/mtGjhzZm/zhcotvRgv83//G1vMnLam658ZF65ZfZi3ff7Z13Ven5lce7mepgl627RBvr4VoErUcjVJEGUWXRRU2QXRhM+jsDaB31EMUCV6S6EVQBFrHuAQ4imJrkKPWZOga0nXw3PZ66ShFtpFoZ2tDSMBrhWh8HWPHY0ErXrNFIhq/F2ethx6WWuhjqYaBls2/nFuw8b+X2NccyHppc9ttr2767R1FSwdh0X31k57aFvp4TFpaWhn6m3cX9QDvNVqT7QNsi7PRT1eQv1xuY9OiicY9f8C65qi18IAfDhiGYQRg/xjOUWr+QOUPKYEN8xuR4hrmidoslZ2j6hhFECb6AltoGUXL4MP91chyfLin6XvfIJEQXURl/GdGRsaryLWjR4/uTyKZ7BafLCcH9I+WNA/77dL6uusWbNwzxLLun/1tm3/sadsGcbYaiLHWQpTNJWxJQthxNLgEMYl6AbIYZifxzX/0hM39t3sqKb72ENdcAlsb0iJ9/oTvI9LUUlsDxNlroKdtC5xu3wwX5Fb88wb76vfueWNry/+t2DZsya5dPkVcZWdnx6Kfh6SlpRlSU1Pr0f9hHxHpJ/9B9hkMhrRhw4b18bc9sqljonHPH7COWVwLD1hcYxiG8QD7RRbVTsQdyRYSog3mM+LENcyP54wCOrLIxgQcYaIvsIWM6dPT008xGo33ITQ1jYQOWgvKuwMMOyhabdSoUX80m80m8oG/IkZNTU30rUvWDZy4eE3OdbmrPjxvfsWv/eZvPNLdWg2xR6PEvMU0reAW2jyFO9HnjkfvoCi4RoixNkAvSy0MmL/pyDlPOQ9fm7fiH+OeW73pkeLqG6e+9Va87KKTGk23zcjIOH306NHT0tLS/kX1IaqnCIVE338ZDIZteE+m0kYa6LIu7UzLFlgTjXv+gPXL4lp4wOIawzAMgv0hi2odExIiG+YvYsQ1zIc7Wk20VAeLbExAESb6ApvmTTd8+PDu2FnQ7p/b8PgVHn/Fo3enEpakpKTATTfdtJzEHSy337t/Plxaf8Ntb9W8PdxW9uW5T5f+MGh+5eHeeVuhu7UOoq01EEVoXmDzD9fmCM2gt7qi3qJsOyHO3gp9bLVwqnUTDJ5X8svljpL/jHymtGJqeXOm7KoODf0fbzKZzslMT883GAwR0wZ95Bfkv8gy9FM/3lVUOyYa9/wB65TFtfCAxTWGYSIa7AdZVPMft8imSaEN8xT24hpe/2Simjc81jMBQZjoC2zataFDh8big/q5BoPhOTx+j0SUoJGamrqJpoBOmzath+wSn+3pqp1pic+UT75hweqvT7eu/6UP7fRprYVYayNE25sg2tEMMY4GiLZWA20SQBsKSFM/u4RY7FIbvY3K1QAxVopea8Wy7QJ94W583SZNGaX15LrbtkA/+/pfLnKUfzXu1Q3l96zceo3suo5MN3ny5O6ZmZlZWEc0VVRYdxEM7bL6nslkSqdptbLP2IJoonHPH7A+WVwLD/gHN8MwEQn2fyyqBQbyn6YENsxP2IpreF1/RDU3PNYzAUGY6AtsmjT98OHDaQroYuwk/oHQAuqiDiRc+dP48eNtqampfdEXfk2xu7uk/tTRL62fen1h+X9Oy3Ue6mdZD92t24B24IxyNENU4Q7QOVqQZtDZ6iGaduCkXTvx2M1R30UakMagcVRcs2O5bNUQY6txReQVYFmxzHoHbYLQBtH4N30mtrAeelk3w5D88sPDbSu+vvnlCvud76weILvypEZTc9PS0gZlZWWtwvr6t1f9RTR439J07a+QN1JSUi7Gv/2OuGQLnInGPX/AemRxLTzgH9wMw0Qc2PeRQMGiWuDQlMCGeQlLcQ2vSe3WH1HNDY/1TEAQJvoCm6ZMhw/ivZCrTSZTK3YQkbBRwXFkZmYezMjIuMRfQQLbsu7pitZZpmdWf3C2peJQr5w10NuxBXo6aLOCbRBjq4Yoey3oJCGqCboVNCNN+DcJYiKhrDNoQ1yjDRb0WO4oROegqDya9ura9TTa2ihF78VaGiHa1oh+aYB4aw30z98E5+eVfZ/6zOqm219fl5LtdPo0rRHrKT41NfXWtNS07/B1xLXXDjiCPmlFxqBR9CWvxRYE8x7z/AXrkcW18IB/cDMME1Fgv8fCmjKQTzUxTRTzEFbiGl6rM9Fqbug7vOYaExCEib7Apg2jNZpowX60YuwYIk6kwHKTsPbaLbfccha6wy8R4smy7eckL1w979L88i/7z1sH3a21EFdIYlI1RNurJXEtzrYVYvEYLUVzybt7eotS4QKVrbBWopsUlUfiWj3EWOshDom31EP3fDzmN0AUrctm3wFR9lboaa2G0/MqYOSCsiN3vVG5YvaKLRf5snkEfSYlJeUarMMqUd0yyTSl+3G8vweju3hHUZVNNO75A9Yfi2vhAYtrDMNEDNjnsbCmPEGPYsPrh4W4htfoiqjmhsd5JmAIE32BLahG0z+7JyYmno8P338wGAyRNv2TptD9mp6e/s2YMWMeys7O7iX7pUPLAdBnO2t6pTy/Pvlaa9mOM3PKj/SzboPudhKMmkFnaQSdjaZINkA0Tf+01SG10tElrlEUm0t40tJ6aYGim70JaQaK0qO/9eiLKButw1YPsegPCSv6ykZTRttAX7hDmi4aZ9kK/fMq4ezcUkhdXPHFuBfWGMYsWedL1JXu5ptvvgrrcY3ZbKZ2zFFsHmA7P4z39weICV/38nfHW7bOm2jc8wesPxbXwgP+0c0wTNiDfR2JFLy+mnoEVWDDa4e8uIbnpzbbFVHNDY/zTMAQJvoCW1BMl5iYeCo+ZA9HnsbO4P8hkShG/JSSklI5ZsyYRH8Wf5+2ZEnM75ZuH5n1woa1F1vW/9A/fwv0sNHOn9UQ5aiHqEJaW60NX+8AvbUV9HZaZ60Nj62gszdLC/6TwKa310oc28xALFSJIdHKJVxpjyYsY4tMM9D0TxIZpTIXEPjagWkO2vAAPy9F8dVDNKbHFTZBjLUBulvrYYBlI1xkLfsq9fn1r2e9WHnZtCW7YuQqaNdGjx7dPzU1da7ZbP4T1i+tPSaq94gF73faUbQISSFhXXYbm4ImGvf8AeuKxbXwgH90MwwT1mA/x9FqwSFoAhteN2TFNTxvIKLVPOFxngkYwkRfYFPXEhISeiQmJtIi54uRg0hE7QAqQ0Lin0eNGrVs7NixV6JbfJ0Gqrvzna0DRj234aYEx5pNZ+WtO9wjfzvQDpjSwv2SSES4BCZJSHOQoEbQ9MdmOZ3eJzHNJSqFX+SaS/jT29x4vEe+OYrsC/KB5AdE8iVteNAM0fh3D+tWOCOvAm5YULE7Y9HaiTPLtp9D9eCqDrHRbqKTJk16KDU1tQnr+XuPemdk8L7/Cx4njRgx4hyaEi67jk0BE417/oD1xOJaeMA/uhmGCVuwj2NhLbgERWDDa4akuIbnDFS0mic8zjMBQ5joC2yqmE5eU20w3vhTkVLkW7kjiDRITPzYaDT+ISUlhXam9ElYo2mgtzy/+uLUBc6c6wsq/jU4v9IVrXZUUDoZLrFJ/B7jEhllsdEjnaLbomx10NNeDQPz1sGV9jVfZT2/bvm0NzYmYt/RUb3pDQbDFSaT6W2s72886p85xv/D++AdvA+uT09PPwV95qvIzOaHeY95/oL1xOJaeMA/uhmGCVuwj6OpoKK+j1EP1QU2vF5IiWt4rkBHq3nC4zwTMISJvsCmrOHDc3RSUtLpJpNpHL5ejDf+H5GIXI8KfXA4xWz+MCUl5fZx48b1Rvf4JiYA6H6/vPrGpMVrSs7Ld37T31IJ8bQDKE0BlaLUGCXQOyiKrQ5iC+qhu70a+tk2wbn5pZC0oHTXhBcrR+c493S4Rh62+XOxvp83m80ssAlA/1BfsMVgMNiGDx8+JCEhocOpt2z+mWjc8wesHxbXwgP+0c0wTFiC/RtHrWkHVQU2vFbIiGt4HiWi1TzhcZ4JGMJEX2BTzuhBGR+er8Ob/TnkI3wdcRsWuDGbzbQb6GejR4++a8yYMbRAvs9204vrjIlF5fXnFFQe6VGwXVobjNYPo2mLJAB1YwKMaxfVbnb0cwGJbOTnWoh1VEPP/Co4LbcCrnKs+WjyG7WFDzjbTpOrqV0bP378WVlZWQszMjJ+ELUNRhLZyDfL8Jgtr8XGUWwBMtG45w9YLyyuhQf8o5thmLAD+zYW1rSHagIbXieUxDWl88rjPBMwhIm+wKaI6WiaF97kNAW0Bh+Y/+dx40cihzMyMj4YP378Tffccw9FrPlkj1Y1n3LPiupFl81f1nbG/HLobt0O0YVtoCvcCXpHi7xQfz10czCBRRbYaI06B62/1gBRBQimR9lrId5aDQPyNsFvCjf+mL6w/K3pyzaeKVdZuzZ58uQhkyZNKsS28DHCO4m2z17sLxanpaWdh25jgS0AJhr3/AHrhMW18IB/dDMME1Zgv8bCmnZRRWDDa7C4dgwe55mAIUz0BbbA2oUXXhiHD8fJiBP5VHDjRxyjRo3666Sbbhqfk5Ojl93UoeWsqxk45e0tzsuLKn7qP3819LRshWhrPUTZd0CUbSdE21oRebdLaa0wJnDI00LtTehrgl5TWhN0k3YYbYI4SwP0zdsKF80r/fHG3KWvZhc6O4xgmzp16mkmkykd28S7CAts7YD9Bm0C8RYef4PEy+5j66SJxj1/wLpgcS084B/dDMOEFdiv8Tpr2uZMUb0FErwGi2vH4HGeCRjCRF9gC4yRcGQ2m4fgjT0ToeiTQ3iMdAGByr9h4sSJk6ZOneqzSDDNuavvxCWVpRdYSg71cmyBGPt2iLLWQbStGaKQaGsLgsej4hqjBFG2BvSxeydWEtaaJUhko6jBeEsD9MvdDOc8Xfr11fOWLkorKh8kV2G7hvdFtMFgSMJ20ebRTpgT+Q59VY2+uicxMbE/us5nYZrteBONe/6AdcHiWnjAP7oZhgkbsE/jqDXto3j0Gp6fxbVj8DjPBAxhoi+wdd0yMzPjTCbTVfgw3Iw39r+8bvSIZfTo0YfQJ5dOnz49TnZVhzZjxbqBWS+trjgzf9WPveybIcpWLwloUQUtEEXCjsMVWcWimtLQFNA6CVrXTlfQBLrCRuhWQPVBEYT1ktgZY2uEvtZtR86dX/6fhJyVhRNe2tyhwCavRXg1Ui9qN8xRfkH+ijyHvroQXcfTRDthonHPH9D/LK6FB/yjm2GYsAD7MxbWQgdFBTY8N4trx+BxngkYwkRfYOu8UbRaWlpaT3zwXYA39B/x+KvXTR6xoC/KMzMzr0E3+Rxxc+/KbYMzni/bdqa15KfeC7dLAo7OStFptO4XCWuNoCsgKJKK8BSDmEBDGxnoHHUun0u+x9eOWimNdhHVYZ3EFO6A2IJm6JO35ch581d/c12+02FcsG6gXKUnM/3IkSMvwfvnJ1H7YY6DotiWmM3ma/HI00T9NNG45w/ofxbXQh/anUy13dsYhmGUBPuzUJsOSgIT5bkrhLKYqNj0UDw3i2vHYHGNCRjCRF9g65TpbrjhhsEmk2kiPuzuQCJ9w4LjyMrK+mbs2LFXop98Ftaml2w8ddJrG7efZS39ubu9CqKkCCmagujmeOGHUQE5StC91toJ72O63t4CekczRDnqoZdlC5yTv+ab6+zlT4xfVNFPrtqTmX7KlOx70tLSInYXXT/4AfuZvdjnjEtISKDddjmKzUcTjXv+gL5ncS20YWGNYZiwgfozRMtC00Xy0VNQ63IfTOfwOJ8noSC6UR4VGYfwvCyuHYPFNSZgCBN9gc0/wwfc6NTU1DPwBq5AvsK/D3vc1BFPSkrKz2PHjk3Lzs6Okl3WoeVUvNdv/LOla8+fv/KnXnnrIa6gFrpZqyG60FvMYbQGrcsmTdEtaIIYex30z98El89b+VVy7hsPjSt4vcOdYSdPntx99OjRD2O7oSmQwjbFHOUw9jefmkymZ/FIO7SywOaDicY9f0C/qy2ukRhE12QCAwtrDMOEDdinkaAkGjuCiaeQ1k8+qtL30nXk6xFaFtoUiV7D89I4J7peIGFxjYk4hIm+wOa70VpR+GB7M968B/HhlqeAeoE++clsNmeTn2SXdWg5NbsG3v52VflFecU/DrBWQbylRhJpohzVEFVIUxDrhaIOE3z0NhLXsK4kgY0i2JogzrodTp1fAZc9/vp3yTlvPpD9grOXXNXtWmpqat+UlBSLqE0xJ0CbhNBmKc8MHz58CLrPZxE7Uk007vkD+lqNH65uOMqKYRiGEULjA6IFAclTTFNNSOsIyodXvrQktlFeAu4nPKfSv1EC9rsEz8PiGhMyCBN9ga1joyispKSks4xG4314436CRPouoCK+Rf9Mxgf+7rLbOjTb1r0D7np7c/nF1uJDfe2bIZZ2ArW0SjtURhfWgK5wO3SjKDZHPaNBSPiMstdAjA2PtlbQFbRBVEET9MC0U+avhastJT9nLii9Z8ySdTSN8WSmo2hQbD+LEY4E9QH0068Gg+E/eEz1R8yORBONe/6A/lZTXOMfhgzDMIwQHCNIMBKNHWqiiEikBJRPREsiW8Cj1/CcVEYSwETX6yoB/YcfnovFNSZkECb6AtvJbejQobH4EHsb3rDv4oMsrw0lwGQyQXZ29ubJkyefKrvNJ7t/Ze3z1xWW/9zXXglxC5shqnAXRFt3QLStCfQkqhXVCEUdRhuQuBZtq4FY6zFxTV/UBlFFLRBHU0TzNsA19uIfbnl9bfb0jRs72jFWN2nSpFFjRo/5m6iNMWKwT/oGjzfiMVr2I5uXicY9f0D/srjGMAzDBBUcH4IdtUbXJqEqJIQ1TyjPct6DLbIpIkzSORH6rRJIkS3gkfR4PhbXmJBBmOgLbO2aDh9YeyG3InsENzDj4ojJZGpLSUk5n3zmcl0HBqCb/NJa89XW0l2n5lZArH0b6AubQO9ogyhbizTdUF/QIKGzM1pFj0Tb6qRIQ72NNp1ogW4FrdCtsBXrrgVi8b0BuWsg+dnV+7JeKDtHrv12LTMzMy41NfV32J7+JWhnTDtg//Qv5LqRI0d2uMZdJJpo3PMH9DGLawzDMExQwfEhmFFriohCakNlkMsiKqNaKLlzqFtkCwRKiIB0XpFPAgX/hmIChjDRF9hOtOzs7Fh8WL0Ub9JFCC+0fhLQT61ms/nKnJwcH3cGBd0ja3femP5CVeNZlkrobdsOsdIaaw2gL2yEKElUa5TW7xKt88VoC1pvzQ2JorSDqISjWarDHgX1cHreakhevNZ5+6s1Z2Knc1IBlnbCxDb1KLatf3q3NaZDnHgv0jpsvNGBh4nGPX9Avyr9Y9AT/mHIMAzDnACOD8ES18JCWHNDZZHLJCqrGoSVP/0By83iGhMyCBN9ge0402VkZJyOD/dTkP14k/L6T+1DEWt/Hj169D3Z2dkdLlovGYBuwjObTr/WvnbhEMv6/3W3bodoSy1E5dVCjA2PBUhRHeiLaMqhW6g5Xsxh1IQi1ETpblxCGu0WqrdTBFstxFrrpGmi0bZ6KT3K0QLdrTVw2tNOmPjKpi33vlZ1itwa2rWbb775N3gfFmMb+59Xm2M6APutbWazeVhmZmYfdCWLbGiicc8f0K8srjEMwzBBA8eGYAlCYSkEUZmQYE4TVSx6TctguVlcY0IGYaIvsLkMH0qjTSYTzS9/Gfmvx43KCEBffY0P8Q/LD/E+2QPOPb1SF1fNvMiy/tue+Vsh2k4RT40Qi8TQrpOOOtAX1rumhJJ4I0VCMUHDcUxc6yaJnd7IUWr4Wm8nQY2ENRcx0jpsjRBlb8H0Ruht3Q7n566GrJc33/toSXOHm17g/Xh1SkrKdmxrLHD7CfruL8j/paenk5AZ8QKbaNzzB/Qpi2sMwzBM0MCxIRhRa2EfYUXlk8spKr+ShL1vRWCZWVxjQgZhoi+wdetGaxUlJSXdjjfla/hQyg/zHfNjampqMT68X4ru8+nhPcfpjJ38WlXaFY4Nrf3ytkKMRRbQJGEGkYQ093pecjqLa5pHWh/vKC6xlHCnRdmapL/jHM3QM7cKhi/c8PXvXtv0+46mEdP7Y8eOvd9sNn/O96T/oM/+gTxmMpkuwGNEb3YgGvf8Af3J4hrDMAwTFHBcCIYAFDHiD5YzWNNtIy56DcvM4hoTMggTfSGSLTs7OyoxMfFig8HwBPIPwU3KCEhNTX0/A438J7uyQ7vlxaqzkhZUrBqSuwZ6WGok0UXP0z5DHhLQROluSHAjdFjfsdYGGJxfCebCkn9Ne2djotw02jWj0dgvMzPzWZPJRDtiCtsic1L+i/3am4iJdj2W3RpxJhr3/AH9yOIawzAMExRwXFBb/ImoqCoqq1xmkS+UhMW1wMO/oZiAIUz0hQg1XWJiYn+8CTOQ1fgQ/7XHjcmcnM8zMjJmTJ48+VTZlx3atHW7ekx8rcp6Tn75v3vnb5J2kdTbmqR11bo5GpgQRpo6ejLstPZaA0TZWyHa2go9bbVwWq7zSMYrFY2TS0o6mh6qmzRp0jVms7kM290PXu2Q8Y0fsX/bbDAYRuGR1kaMuGmionHPH9CHLK4xDMMwQQHHBTXFtUidrsjRgSqA5WVxjQkZhIm+EIGmS01NPQMfNJ9FdiO/Cm5ORswhZFlKSsr55EeXOzs03R1LG64a9szav/SxboJYO0Wt1UtRTS6Bpp4JYXQdoJeJtrdAlLUVouxN0L1gM5zlWAVZz69akJOT09GURd2oUaPGYLv7s0c7ZPyDhMl3sa+bRX0f+dTl2sgw0bjnD+g7FtcYhmGYoIDjgpriWkQutE9Q2b18oQYR5W8sL4trTMggTPSFSLJp06bF4ANmssFgWIVHmmp2xOumZE6CyWRqxqPRnylmOTmgT3lmw9OD89b/GksbFjiajk4l9I6CYkIPYbSaJwW0QUUdRGN90+YGuoI2iFrQAN1tayHBserbae9suU5uKu3a+PHj+2HbexzbHkeYdo2/InNHjBgxCN0aMQKbaNzzB/QZi2sMwzBMUMBxQS3RJyKj1txQ2WUfiHyjFCyuBRb+DcUEDGGiL0SKZWZmxuFNdy+yA/lRvgkZH6F1r9LS0qyTJ0/ucKdHT5uzZseUobmlf+trq4FoRyPoHS2gczQjjS7ktbmYMEWq8wZpJ9EoWn8N619f1AbdC+rh9NzVcKN95fLsHGdHYq1utHn0kNTU1A+wLfLmBl3AaDT+1WAwzMH+kKZ1R4TAJhr3/AH9xuIawzAMozo4Jqgp+ERs1Job9IHaAhuLa4GFf0MxAUOY6AsRYLrhw4efgjfcA8hengbaOUwm0+5x48ZRlJFfD+Q3OEpy++eshu6WWoizNUK0vQn0Bc2gK+DNDCICB62tR5tX1EG0rVbayCLasRPibTuhX241XJRb+u20lQ0Wubm0a9ndsqPGjRlzN7bDr0Ttk/Ed7AOlnURTUlIGoGvDXmATjXv+gD5jcY1hGIZRHRwTOGpNZdAPPA1XIbC8LK4xIYMw0RfC3HS0PhjtmIcPk/9mYa1zmEym70ePGv3c1KlT42W/+mTZOTmxl1pX/TfeshG62xsgztII0TQltKAZuhW0QDcSXrzEmG4czRZmNEvo7fUQZa/B+m+AGEsbxOXtgt7WNhhs2XIkwVLyQf7auvPkZtOuZWdn90pPT1+LbfIX7zbK+M1X2B/OTkxMpAg2vcvD4Wmicc8f0FcsrjEMwzCqg2OCWkJPxEetuUFfqBm9FlGiJpaVxTUmZBAm+kK4Gj44xpvN5mF4dBoMBp4G2gVSUlLek6eR+WwFjft6Z7+yfvNp81dCD8d2iLU3Qqy1GaIRnaOlXXHNE/fabCeDPtMRou8x6kI7hkoCG9ZHlNQOWqT20MNSA2fkrf4qZUHJ/XLTOallZWWdg+3xW1E7ZfyG/Pge9pFXo2ujXB4OPxONe/6APmJxjWEYhlEdHBNYXAsC5A8v/yhJxPgey8riGhMyCBN9IQxNhw+LA5HpyAG80WiHS9ENyPiAyWT6Nj09/Xbyq8u9vtkDr669/Ib8pYcG5q+B6Lyt0m6RJKxJ4hq+7uZo7lBcY8IPT8GTjhTJ1i9/05EbHKWfFVTtvkVuPiczPbbHQmybHL0WGGgNu89kgS0sI9hE454/oH9YXGMYhmFUB8cEFteCAPqD17pTACwri2tMyCBM9IVwM1lYcxgMhp/wJuPdQLuIyWT6YOzYsYNl9/ps2S+tdZ7x+NIjvS1VEGOvA73NJaxFW2n9rSZJWKMpoB3hLc6cgPfulCJE31MC0bW9EX0vommA7vnb4HzLmp/uerv21Rznng53or3pppsuw3uco9cCxxHsLz/Be/0cdG/YrcEmGvf8Af3D4hqjCbB9xCIDAkys6FpM4EFf0wM79SedIWymjlFZvMrWVcJ2Wh2WTQ1xjddbE4A+UUvYjBj/YznpfhX5IFBE7G+oUaNGXZWWlvZKIKFziq4VKQgTfSGMTNq4AB+6n0J4R8HAcHjMmDFPkm9dLvbNFmxou/jq3OVf9s9ZC3G0S2hhC+gdrRAlCWwUsdQA3Ry+QeJLu9D7jvqOEX030GgpL5rHJaq5ibXXwWDbNhia41wzfGGJL7vRRk2YMGEltk8WzwPHEew39+NgSmvfhZXAJhr3/AF9o/SPQU9YXGOozbUnog1GRO2mK9A5va/DgluAQF96CknnIKI68AX6rvs8REg8jFM+vfJNdMUPIrx9Q4SFWIHlYHEtSKBPeGpogMFy0r0pKn+giIjfUCIhLSUlpUbgjy5B5/S+TiQJbsJEXwgT05lMpksMBsNfWFgLHHhT/W3ChAmDZB/7bPe8viH3orw1v/bI2waxBS2gQ6ILd4De1gRRtnpp58hujvqAIBSwvBB9j1EXV13UuY4kqh1No/bQCD0sdXCpveqrcc9Wznt248E4uSm1a1OmTLmBpiyL2i3TebD//AK5NCEhIUZ2dcibaNzzB/SL0j8GPWFxLULBuvcU1JQQ0fzBU3ALWaEN8x6oCCm/hQf6jvzdQAtJbjwFJc0II5QXj3wpWf6O8BbcQlI8wnzztNAgQT7x8pFSBETcpHMgnm0+ELSbL3rP67O+oHR/4H3fdxbN9ReegpoSQpqveApu4S60CRN9IdQNHwTj09PTL8Xj30SNgOk848ePfxZd7FcUS7bTGTVpSdXXg3I3Qqy1AfQFraArasNjixSxpCdxTRZZRCKMv3iKNO0h+h6jHqI6OQZFrzVDjK0FTrVuh6sdazbdv6r2Mrk5tWvZ2dlR2D43i9ot02W+xP70xZEjR/aW3R3SJhr3/AH9QT+0RH5SAhbXIgisby0Jau0RkkIb5pUe/AL1IEfn8flhiz4rf0d0LiVwP1AG5YGQritfn1Cz3P4QVB91FsyvGgLPRUg/0fUjGfQJtWs11l3rsrCJ51Cqz6FznnDPUJr8nug74YCw3GqjFUGtPcJdaBMm+kIIG21ccBo9BBoMhk9Flc50HrxRvkJ6yr722R5YXnPnBU+X/Ng7d7tr8wIS1QqbQe9ogihpt8g6l7hWQMKLe/qneL014vj1uQT4Mh1T9D0l0FJeQgZag68Fou2t0MNaC5cXVv54yxtVj+UAdLi4fnp6+oXYRr8TtV+myxzCvtWBPj4FXR3S00RF454/oC9YXGMCBtZxKAhq7RESQhvmTYkHvw4ftuh9JJgCE11XNQGJriNfL1jl7QxuH6nmp66AeeTItSBCfvHykxJ0KXKNvosoeQ/SuY/mj17LaaLPhhPHlVsttC6otUc4Cm3CRF8IRcvJydHjg9+FWJlv4pGngQaeX81m8x9kd/tsFLWW8fzGVwfM3/BLnKUBomxNoCOBrMA1FTTaViuhd9RCN0lcU0Zw0tvpeoRrTS/RZxjtEGVtwnbRDLH4erB9yy9XF214ZcIzm06Xm1W7Rv0AduJ2QftlAgD2rd/gMQ/7giHo7pAV2ETjnj+gD+ghTOgjBWBxLUzBunWLaqEmqLWHW2jTnMiGeVLqnm33YYvS5fdF31Mbyoci4hGdUz43EeoP2G4/KeKrQID5YnEtiJBfvPykFJ32P36X2q/onIHk6G8Teu31Xjijym+yUBXU2oPKEA4CmzDRF0LMKFqtH5KJledEfvGsTCYwmEymZvRxL9nnPtuYJesG/ibf+WUfaz3EWJulHUKlaK6COklci5JxR665ornc4pdbCHPTKG2AoLPRdFI80mdIqHODf+ttjRBtr4do/FuPnyFibPUQn78F+li3QI/8zRBrq8HzuK/hdQ75PDoHiYCInaBzNcjCHL2PR0kAlP+mc7mR0vCz0vXpNU13ldcVI6iMUjkprdaFdE5P6HveaZFFNNUj+jMGfdHbUg3n5m1oG25dZ5Sb1Ult2LBhfbDNHvRuw0xgwH7g3waDoTApKYk2OugwmlCLJhr3/AH9oOYPSRbXwgys03AT1bzRnMiGeVHynj3hHsU0rUZyUJ4CJhrRueRziq4V6gTUV4EC88TiWhAhv3j5SSlYXNMmiv8mIxEqHAQ1b8Ihkk2Y6AuhYrTGEj7oDcQHvRl4/AwrjiPWFMBsNtMNcRe63O9IlVEvrrlrwLyS//WwNUOU1SWKuaZ9ymITCVGSGEXQFFESlkjYcv1Nn3Gv00WL3EdbW0Fv3eES2EgIK8DPy6JcFJ4nxtoIcbY6iMX3ouxt+LoF+udugQst6/89tKCy5jz7+m3nLKg7Emetw2vJYg4JYXgegq7bjUQ1aeoqXstBAh2Jdg3S+V35JGGwBql1rReH55LA11H4vkRhA0QjUVQGSWDDz9F38RpRhKMWqYZoe7XrvJgX6TpyhN1Rgc0t9oUhUpnbIQrfJ4E0FuuyR349DMnb+tPIBVtyHq54r5/ctE5mOuy4LaK2zASMfyFugS3kIthE454/YNlZXGM6BdYnCWvhKqp5Q+XUhMCG+VDqnj0P6eN1La0LTpQ38kenhSP6rnyOcBXW3GhOYMP8qCXu8I6hAtAnLK65iFRxTbE+wR2tFo7CmjehGskmTPSFUDCj0RiNpGIFPYPHf+DxiGelMYHDbDb/fcyYMZfKrvfZHqisOW3kMyXv9rOsPRxnbwW9FHFGwtkxwcwltJCo5SkouaLbXNFiLiHG9dkGKXKN1uNyn4emk0pCFUWr2RohVppOWA8x9maIszbDIGstXF9U9a+sF6tmZb9dnXBz8btXJb3WeqinpQZiLSTW0eddUVKEW9xzRb3REZEENRLGSMCrxfNuh575W6GnZSvEI3HWbRBn2w5x9mqIxbzEFuH1F2B+FpDA1ojfpTJgfqWdUVvwOq0QY22BOEsTfpeuT+n4Oam8dH0ZKrMU4Sb7JUJwiYwkrjVI4lq8pR76zd8Kw4o2N0x9q2Y49lEdijlZWVkmtJ9F7ZkJGH/DvteKnIsuD6kINu8xz1+w7Gr+kGRxLQzAegz3aLX20EQUG15fyXvW8yEzlCK5/O5b8DuRIqp5QmWlMmtCaMJ8qCXuEBy95gX5xMtHSsHimnYJ+O+ycI1WOxmhKLAJE31B6zZ06NBYg8EwFiumGvnRs6IYRZhLYqbsfp/t7nc2pV5sXfGf3vZNEEUbGUjiWqMsGHmJRpKQRGIZCSsuYYvWZ6MNEGgqqWszAxKcECnCjCK8avEztGYbTTkloaxZIgq/S9FrgyxbDw0t3Lo0+/Xae+5YulnaiMFWUX/ntY51v/bO3wbxlgaIy2+CeJk4OgeJXA4S1GgKKk1ZJfFuO8QW4Oetm2BAfiVcYNkAl+ZX/utSy/rPLrCu++w82/rPznZU/uWCwo0wxLoOTs1fA/3y10JPy0Y85xbMUzWeF/NpbUCaIMbSArGWNoiz7IBY6w6ItrdI5ZVEOLy2FDknvXaLax7CY4RAIqdUr1gHdOxh2Q6XWVd/m7LAefu0JUtipAZ2Ehs+fHh3s9m8UtCWmcDyV+wbHKEWwSYa9/wBy63mD0kW10IYrL9IFdW8CarIhtdV8p6V7lE8hpKw5nf0BX1e/p7ofJGAJkQ2uj6ixo6VBEeveYH+8BbXaGdVt6/ovUDRab/jd1lcUw6/+86TEUnRaiKo3KE0TVSY6AtaNllYm4AV0oAPdT95VxITcH5BP/8GXe/3g/NNz5e9c3pe8U89HNsgytEC3ezyemtucU0WjChKiSLPYuy1EGuvkaKVYq0UhdYMMZZWeTqpHL1Ga7UV1UtRZBTlRQIcRbPRZ+gYjZ+Lt22H0/LW/pr5yvZPJr9eOyLb6Yyl/NBi94+uqi455/EVhynqLNZaL0WPdc9vRlqk60WRsFfYBDqa0on5ibdtgb7WtXi+kp/Mb9V/d1XRenvq4k1PpC+quiXRWpEyoqAi5Xpk2DNrx2e8tOWJYfbSJy57etkTV9pKFxuer/r46kWbvj3bXvHjoNyyIwPmr4ZT8tZBH0sVdLduxXNXQ7y9DqKxXDRVVF9Awh75SI7MI8HN1iiJa65IP/dOqqGPqx20j0swpUhBV13HOqrhLOsaGPvKxhVPrm0lIeekRlPGscO+B9svTxVXnn9iH7EAoQi2kBDYROOeP2CZWVxjOgTrLpKmgPoK+UN1gQ2vqdQ9K00LRUJNePKrX8HPR7qw5klAH647A16fxBdR3pSAo9c8QH/QveApgvWTj5oRITEvLK4pR8B+k0VitFp7hEoUmzDRFzRquszMzDh8gBuDlVCPR57ypQ5NiYmJ/eU68Nnyt398zsgFZXsHWCuPUNRWTFEbUPSZK+KMNjDAo4OmWzZBFK2TZqmDPrZt0D+/EoYUbYdBRY3Qy9YEsZYWiLa2gB5fk+jSrYA2AWgAnTQ1lKaa7sTz7ZCmnMYUNEIP+zY4174Wrnc4l9319vb0nHW7eshZ6vZESfOQUc9t/PPgeesgjnYolaeSUiRZjLUVoul8mMfoonqIW1ALPa1VcI6jEq5csHbDDQtLf3f3mh2pY5bUDJzr3NV3yS44LnrK6YQoSs92OPumOpb0zX6h8rTb36y9Pv3FKpPpxfV3Zb1cuShrSdXLNy4qf/mKRetfvub59d9funA1nF1QAX3yKMqtCuKt26A7+iXW7topM8bRBtEFO7C8FM12bCptOOCKymsft7gmTZPF+oguqoV+1kq4rnD1vqxFa26Q3X4y02FHfQ223795tWdGAbA//jce7ampqWeQ711VoF0TjXv+gGVlcY05KVhvLKy1j+oCG15PyXuWxJZQEp58Fofocwj5joW14wmqwIbXVlNc4+i1EAPri8U15QjIbzIW1k6E/KH1KDZhoi9o0GhH0NOQp5BPEY5YU4kxY8Y4srOzpcgvf2zsy2vThuSV/bWnvRb0lkaILSKRiMQSGRLaCigyi6ZJ1kOvvO0w8OnVkFBQ9lXaWw2O617buXuArebXOAtNpXRNDXVFutVJa5fpbK14vl143CVFrHW3VsMA60a4rHDtr6nPrV0+5cW1Vxpzao6byjr2hZoLL8tf/0NvvF6UdH2KFKPIOZqWSVFrtMbXduhlXQ+n2dbA9Ys3/m3Es1X3JL5Uef40Eul8WOtLZDk1NfEzy5sGzapsO23qW5WnZSHJr6y54bpnS0feW1xnTXpmzbZrC9ceOi+vDAblVEC/vCroYa2FGCyn3kHllKfTRhCuaaFY31hH3YrqQVdYA3HWzXCxfd2PY17Z8oeZm9+XpvmezMaOHTs4a1TWZlG7ZgKPW2Cjvhrdr2mBTTTu+QOWk8U1RgjWF08D9Q1VBTa8ViQ9/HWET30Kfo6j1U4O+YbalerCE15TTXGNYIEthMC6YnFNObr8m4yFtZOj5Sg2YaIvaMx0I0aMGGQwGOYhX4oqgVEGk8kEWVlZV1MduKrCd7v1rarVp9o2/RxrbwNdXjNESbtu1kOUzSWukWiiK3Clx1vqoH/OZrgyb/U3WQtXz767pP5U85LaNwflVf5Amwe4dpB0Ra+5xTnXpgetEO1ogt7WbXDGvFIYZiv5dNzza2bcuWTd2TlewhrZ5CXbVp6eW/VrHE0jpemfJKwVYj6K8LxFDRBn3wb989bApdZVh0YuLF0y/qVNSdkv1PSSv66I2Ss/7D/+xYqzxr+6KWn0s2unjXt+3f+7yuKE0/PWQj8bbZRAO5KSqHi8+BTuSHVOrwuxnguxvmmzCHsNnJq3Ca4v3Fg4+dmNp8oubNeMRmN8amrqH6gdi9o3owhfod8XU5+NVaBZgU007vkDlpPFNeYEsK44Ws0/VBPY8Dosrh2jwz4FP8PCmu+oHsWG11NbXCNYYAsRsJ5YXFOOTv8mI8EoktdX8wetCmzCRF/QkLkj1h5F/iNyPqMc2LArUlNT+8p14bPl1NREj1i0Znd/Rw1EWXdBVP4OKXqN1kOj6X5RNC20oAG6FZJY1gQ98qvhQts2GPNS4+rpJa1D8RS68S9VvnyWZfX3cbbNoHfg5yVxjQS6WvxuDegLayEKj71sVXCereKw6Zk1WyY9WzHyAWdNL2zEJzzU52zbf2WCffVf++VUHenhoI0SXLt0Suuc0bTDwu3Q27IWfmNzHhnzfMU7d76z4WLvyDclLdvpjJq8sKT7797ccsXNL224L3HBusoLc8t/PSVnPfSw0IYIJCo2SKKTjtauO4pLiPIWp0IdSVyjjR0Kaaou1o8krtVDn7ztcOOiqn/d9NLmBNl17RqtsYeD2A0mk2mvqH0zyoB99f/wuGj48OGnyFWhORONe/6A5WNxjTkOrCcW1jqHKgIbXoPFNRcdCkH0vvw50fcZMaoKbHitYIhrhHvRfhbZNAzWD4trytGp32QcreY/WhTYhIm+oBHTjRw58gx07nb5YU3oeEYZKNoHG3T+5MmTu8v14bNNL9/x4AX5a/5JO3bqrW0Qa2+VpvnRtMtoaffHWkkw0RU1Q0xRE/TP3wAJRRt33rWsNZ1EJjrH6JeqXj7Tsub77iSuWWvwu00QVUBRTHUQtWA7xBduhv7WCrjUVvxl5ovr83+/rH6o+7siu6N8Z9r5hZXfdrdsgThrHXS3NkE8rWtW0AgxhdXQr3ADXOAo/cvYVzcufnxty5U5OaCXv6qqkSA0/dmNcalLtva95dUtc5MdZf88L6cMBlhpqmgNRFsaQE/RgLYd8tTYRtAVUCQfRbedLMKNNkgQpWsXaddWajuY91j8mza56GOphWuKNh66681t93mup9eeZWRkXJKWlsaDmfrQLs62YcOG9ZGrQlMmGvf8AcvG4hpzFKwjFta6BvlO0Z1E8dwsrrk4aX+C77Ow1nlUE9joOggJXaJ8qAFHsWkYrBsW15TD799kLKx1Hq0JbMJEX9CA6cxm8xCj0diK/CpyNqM4/zUYDJdRXbiqxDdbsmtXzOTXti8cYtn8TSxNvbQ1QTRN5aTplw6aFkriWh1IO4QWNUPvhXVw6cL1f7/ptar7jTWuSLGcqj0Xmp7duHOwZdOReGs1xFN0maUGdPiaNhrou2AbnGEpO2JavHrf9OJtj2Ru3BhHopSUgXZs2oqaPYNta4/QrpPxBU0QT6JNbj3E4jlPcWyAywtL/jn+jfUzJy8s6R4sYc3bpmO55i7dftXYZ8teHZqz7OtT56+F+Pxq9OcO6Gbfhf6gzRxoaq1rgwgS11xTaL1pkhAJWFpGKhttNGF1bWoRk98IPeZvhXPnl8H4Zytenv5Gx1NDab1Ak8n0O+xHjgjaOKMsP6HvF6LvFZ1a3RkTjXv+gGVjcY2RwPphYS1wKBbFhudlca0D8Yfekz8j+i7jG2oKbMGKXnPDUWwaBeuExTXl8Os3GQtrXUdLApsw0ReCbDracQ4fyN5Hh/IDcZBIT0//MjMz80y5Tny2exeWnGK0rGw6Pb8KYkhcI8HHUQe0eYFLXKPdQVulqCuKbBucv/bQsMKytx9cvXWAfIpuM8qaLr3WWv5e//wtECsJa7XQ3V4nrUF26tOlcENBxc/3Lq/dc+9r2wafLFrN025avOaP/eetwzw1QozVtRNnz4JmGGDZCFdYV31138rqHIoYkz+uGcP7UUci2/+VNz5xZf6K/wywrodYEtMKaKfUNmmn0zgLlcklpIlEqlCF2g5tZhFFu8WSuGZvge6WGjj1yTK4LmfZq2k5S2ldr45Ml5aWNgU7Zu5LggD9cwR5ZejQoZoS2LzHPH/BsrG4xlA7YGEt8CgisOE5WVw7SV+C77GwFjhUEdjwGsEW19y4RTYW2jQC1gOLa8rh828yFtYCh1YENmGiLwTTMjMz+6AT2xB+GA4io0ePLho3blxvuVp8tskLS065Zt6KlgF5VdKmATpaN4umK9KUTmlnzjbQ296FGGsb9M/dBFdbS7+4f1nNHfLXJZux7sNLr3FseK+frRZiC5uhV1ET9H5qHZw/rxTSi5w7Z7yzKcNXUc1taYVrvjgldxvEOtogKq8J4jBfffH6l+au+s/kl9aalyzZFSN/VGmjSMDjoM0Xlqzf8bDgvaNGkXm3La156rzCtd/0tJJwWYc+xHJYmiE+v1maMuna6KE9QmxTBMwzra9HomGUtFtsC0Q5WqXpoX1zN0DSwso/Tnyu6lLZPSc1bMsJ6enpjaJ2zqgC9eXPGI3GeLlKgm6icc8fsDwsrjHUDmgqo6jOmK4xQOTvroDnZHGtnb4E01lYCzyK99t4jWBPDRXB0WwaAP3P4ppy+Cye0+YFXt9luoAWBDZhoi8Ew+TpW3eh80jhZWEtiNB6a8htmZmZfkdy3fXK+md/Y1n9v9752yRxrZskrjXikV63gL5gF6a/B73zm+Cy3DVfZz+/dr781aM2dUXTpRfml73X314NPew1MCBnHVxXuO6Xe9/c/vLUt2r8fkB/vHT3lQn5lf/oNb8aYhw7oKe9CQbmVcLlucX/zXphM+2GqqgVrNhykeWdqnscJS1P569s+nb+isYjTy9vOfL4stYjc95pPjJn5Y4jj63aCU+saDvy1LJmpPHIU0vrjuSuqt9vK2kqsC7bPt26bOv0meW73shauuu7sxyboW/+FuiRXwtxVlrHjnY/dQlSEiRmHhXVjtEtlCigdflcGzlE2Rpdu81i+yGxlnaQvTCvAq7MK70G3XucCCky7IzPR1aJ2jqjHkaj0aIVgU007vkDlofFtQgH64Wj1pQj4NFreL5IF9eED4SUJr8n+g7TeSItes0bFtmCCPqdxTVl6fB3GUetKUOwBTZhoi+obWlpaT1NJtMsg8FAi2ALncmoR3p6+te333J7ck5Ojl+7Zc5es6/3b55e9s5puet+jLdUS9P6dAUtQMIa7WwZXdQG0YVt0CO/Hi5xbD80/sXNJaK10h54Z+v439jKPzstbw2ck1v6S1Jh2bZ73t5s6OxaaLnrdv/hmvw13wywbofe9ho4q6DqSGLe0j/d/VLl+fJHAmJUFoqAcyxrPdOyqvG31pWNu/NWNv0wf1XbT0+v2nXkqZKP4HHnxzDXuR9mlx6EmRKfwMOl++CR0r0wo2QvzC7B95HHnHvgsdL3kXfhiZJd8ETxbphRugd+v/oLuK3sL3D1glYYMG8bxOXXQfSCNmlzCF2BWFQjhAKWliFxrYA2sqCdYhtB2tygsAXL2SKt4TfwqTWQ9cKWO3N8EFuNRmM0tusHkR882zmjOkewLhYnJCT4vQNxoE007vkDloXFtQgG64SFNeUJqMCG54p0cU3Yj1C61+eYwKG4wEbnR7QWveaJ55RRFttUAv2strgWSSJ9h/c1C2vKQhGBIr+rgTDRF1Q0HT5sDTSbzU8aDIbvRA5k1MdkMn2QlJR0nlxHPtvtS2qvv8xaufMUWzVEW+tBRxsZUOSaoxmibQ0Q76iDXpYtcImj6shtb9S05azbJZzWd/eilStumPfmz9flr/rn1DeqXrx/RUN/+a1O2bMbW/uMsq3488X5ZTBk3qr/3PTm1o8eXlVzrvx2l40EtYUlNRcuWFWfYivZOWPeyraP5izf+dOc4o9gbukemOP8GGaX7kMOwCxZVJtR9gk8Un4Q+QQede5HMK0E3ys+CLPw9azS/TCz7GOYWf4RzCr/GNkPM/Czj5T/CaaX/w3uWPknSHhmF5ySXwdxBS0QVdTiIa7VHRXVjsM95TIUwPzSWn0UtSaJa5JQi+mFtIFDA/SdVwm3vlG77aGVLYPlamjXSPTEPub32K6/F7V3RlV+wT7/BeQ0uXqCYqJxzx+wHCyuRShYHyEprFFEuihd4wRMYMPzRLKIJHwYpDT5PdF3mMCghsCm1eg1ESy2qQD6VVVxTb5mJPQnLKxpgGBGrwkTfUElI2HtTMSCjvrW23FM8MhIzyjNysry++E36/mqJ8/K3/SfOFonS9rMoF6arhiNxFtqYEDeRrjIvubfU97Yutu67cMr5a8dZ29V1PS7y/pi2WTb6y33vrLutpx1u3rIb7VnuiXr1vV4q6Kin/y30P7w7PKK5PmvfJr5zMrp1poPfVqrqyNbgnlbsLrtYltx3e3WVfU7clc2HXm6+D14zLkPZpUchEeLD8IM5wGYUYo4D8JMiQMwk4Qzib0wCz87u3g/zCJRrfgT5FP8Lh4lgW0ffmaP9LmZTvw8pj1a+ik8Uvo5TC/7E9xe/Ee49rkdcCrtqGqrda25RiJUu3gJWFrGQRthYDuiKa8S+DeVr6AJojC91/zNkLR4y7uTX607DzutDqeGTkDLzMz8o3dbZ9QH+/yv8fgkHoM2RVQ07vkD5p/FtQgE60LTwtr48ePhkUceode0Fpw3PQVpAx5++GFITU094VwaIiDrr+F5IllcO6EPwTTNPginp6fDgw8+SK+pznxm3LhxJ5xLIyjah+P5tR69djJYbFMA9CPdEyJ/B5L2+pUT7s2ToFYfRNcRXd9fOmyfobDOmtls3kT57Aj6nOj7WiBYApsw0RdUMJ3JZKIOtUh+0BI6jgkOWDdZWC9+TQmdsWTdwBGFa4sH26p/jXbshKiCHVK0Wqy9QdrhcVDeJkgoWPtF8jNlcxY0fJggf+0Ee6Ny8w25b6x05Ly9YlhOTc1J80ARSfc/s+L8GQtevfW5ZaVJcrLQFq7fNsRase0m2jxATuq05QDoi5Y2DVpQ1jJjvnPnxpySHfDYqvfgsZKPYY7zgCSszSz5BPkMZpQiZZ/BTDzOdiKln0gRbHNK98Fc5154DJlTvBdmF++DWcX0XRLWPoVZ0ucOIvthTglBEW0k1O2HR5FHkOlrPoc7Me3GF1pgsHU7xFrrXBFedlrnrglfyxFfoSiuIXpaZ402MrC2QDRt2CBFQ5Jw2wA9c7fBdYUbYeJr1Tf6srnFjTfeeDa26eWi9s6oC9YD7SDaZjAYsoYPH95driJVTTTu+QOWg35kCcunACyuaQCsB80Ja6kpKXD33XfTa7dY1qkoL/qe+xw333zzCdcJMgGJXsNzqHnPao3j+hD8W3PC2pgxY+jo8wOsCPqe+xwPP/SwdH/gay3QYbRLV8HzkzAlunao4Sm2sdDWSdB3avR3Xf5tgudQoy9S/P5zo9WoNbeYhs/3dvz7FuQMUf69oc/R5+l7WhTbKE+ifCuJMNEXlDQSRFJTUy9CpzyPD1j/9XYUE1ywU/gVG6s5Ozvbr904b3+2fPhl81c29c3fCtGOXRBdsAvibE3Qz1YNp+euOWR6dtMnd72x/e6OhJBlmzad/sbG+lOdPggmUxavHXxz0fI3phW+XmJ9qywg0WgdGeV/YdmOSXnFO57PK9398xznHphTth/m0JTPEsR5EGY7SRhD8Diz9FOYUUZi2aeYhmDaHPwMiXBzS/a71l+TIfFMilijcxwV1w7iZwjX96SppWUuZpQdhBlr/ghTV+yFYc82wym2Goi11UlTcaPsJEg1QYyVIr5omigScgJbE+gpag3LESWJa1gW2twA3+ueXw0XWtdB4nObfRLXMjIyTsd+5w0cIHizFG1A00NbDQbDWDyqHsEmGvf8AfOv5oM6i2saAOuBxCdR/agO9mcwd85ceh3QRf8JOicy4A+//8MJ1w0iXRbY8Ptq3rNa4oQHS/xbM76466676Ej5CfjDL52Tzp01atQJ1w0Cij7g07mRUI1eaw+Oausk6KuQENcIOo/XeQONKr+htCaskRDmr5jWEXQeOqdWRLZgRK8JE31BKUtzbVxgwIep15H/iBzFBJfU1NR92FCvwOrqcKqdp2W+vG3yOZZ1+7vnb4MYewv0tDXDgLytcGXRpl+utZc8/1Bx46SOItH8MRJUxhQVv5E8/40/3b2o5NYlu3bFyG8pZi9UNp2TV9x8V+6qtv1Prnz3CG084BLEOsdsGdF7nrg/N7vk+O/MLvsEZpd/Bo8gtxd/CkMXt0AvSzVEOxokcS3G2gyx0hTdWtAVhKK45g1F5NF04yYsVy0MyV8DVz2z1idx7Y477uiJg8E8bOM8BV07/GIwGBpwLMjszM7EXTHRuOcPmHc1H05ZXAsyWAeaiFpLMUuROJ2OUPMHuoZ8La1E63VJYMPvakZQagcSXyiPJ6MzER7H9R/4tyai1u679z46UpkUF0zoGkjQy4wo2pfj+cNRYPPEW2xjwa0d0C90b4l8GEhYXJPRkrDmIaoFRFATQefWisimdvSaMNEXlDB8gOqF/BapR2fw5gXaZQvWkd+L/ae81jT/7ILtP1BEUe+87XBu3ga4yrK6LuXZjfdnv1V5GjYsv8S6k9nUt2riU60r5t0wf9kRc95bzVNfwPMrbDllDefPW1a36qninZ8/UfzekceLP4Q5JbRm2olimBpQRBttjkDTR2eVfwbT1/w/GLviEzjDVg3x1mppnTvXWmU0PbQh9DY0aBcS2BohxlYLg/PWQvrLVXdlO/fEytV0MtMnJSX9Adv2PwRtngkSWB8/I3UGg2EUHlWLYBONe/6AeVfjh6sbRSMemI5B/wc9ak1eg0pxUc0buiYy4Pbbbz8hT0Gg0+uv4XfVvGf9wS2qdXiP02fkzxK+ikXe4lpQ/TBp0iQ6+lTeQELXk68bTJFN8b6czo+Es8DmjafgxuOkDPpCjfucxTUZEngE11UVEveUFtW8oWsFW2RTO3pNmOgLgbaEhIQe+PB0Fz487UV+FTmH0QYZ6Rkbs7KyzpGrzmcb8ULdwnMLtkH/eZVwcf66rzIWrd9808LVxmlLAh9Rdttb2xcPzVv930tzSg/durh8aY5v4kqnLW9Vy2VPlex2zln17k/SNNDiD2Huqg9grrQL6H5pqqaSzEJmysejadI0UjcH4NGyz+C+8j/ByCW7YaCj2jU9VI70cgtS3RwNIQVtaiCmHqJttTDQsgFuerFyy9S3ak66mYVsOux8f4ed8N9F7Z4JKodwXGjF42i1BDbRuOcPmFe1H1DdD+CMGMUeqvDcWoha6/K0yK5C13/g/vtFeVOTTvsBv0ftRHTOYOB5P3eq7dL3PM7RnmB0nJhDr+U00WfV4Lj8BAO6PnJ6elqaKH9qoPiDPl6DhCbRtcMdjmyTwXKr0d+xuCYTbHGNxC21p0d6gnkgkW2Vd77UQk2BTZjoC4E0mu6DBR+PD00fIoe9HcJoC2ycj2dnZ/eVq89nMy2uXHiFZTXcWLD6m9SFa2fe9eray5UQ1jIXlF5zhWPtlwNyN8D5uaVf3f/25nT5LUWsYM2OS55ctaNiTtnHPzxSehAeoUgx5z6YW/IhzHF+BLNL9x0VvJSC1lebKa+zdjSN1mmTX9P6bXMwb9Odn8BtpZ/BlS+0Qm/rdoiS1lprAr2jRdocwCVa1YcMUrRdO9BU1375G8FoWX7QmLNkoFxdJ7VJkyZdhx1wrajdM0HnF2SHwWAY4e96j50x0bjnD5hXNX64Mr6j2EM7njdoUWv4+4mOqkwD9QXKBxJsobFT0Wv4Pa3cswFvq3Q++bzu81NZieOuI6d550ctgi6seUJ5ke8vtVHcD3R+JJKi106Gt+CmmTaoJFhONe51FtcQEnVI3BFcVxVkUUu1aLX2oDwEU2BTa3qoMNEXAmVDhw6Nlaf7vI9wxFoIYDabb8K68ntttLueW7nwty+W//eeVzfMuPOdrQOwIQVsGqinTX6l+v2B8yoP98ndBNcsXP+PqRU+RS11ymyr6i+et6J17eOlH/zwSOkBeKj8j/BwOe3+SWLWxzAXcUWuuTYeUAqa/jlTPnq/R5sczHUegMdKaFOEA/BA2eeQ9fZHcHZhDcTbayCqQN51E3FNq2wIC2iThl6WzZAw722fxbUxY8YMxDa+wrvNM5qBNjmwjBgxYhBWlyL9h9tE454/YF6D+ZDKiFFCtAiamCTvnqgJUc0TyhMSTIEtlMU1xYQVOi9CZRSeX36fri/Kl9JoSlhzQ3kaO2asKL9Ko0b0GgtsYiJCbMNysbh2DEXvt2BFrZGgp/Y00I6gvFCegiE2qhW9Jkz0hUAYRR8YDAYTFvgAC2shxejORI4sKl6fYVmxPmP6so19sBEp8mA8r+Lde6/ILf+lz/wqGJS7Dka9uObvC0uau8tvB9Tyyt69aN6qnRVzVu7+8bHy/fBo6UF4uOxTeKTsE3i0bB/MctJmBnR07/KpHDM98Eyjo7TJATKneB88gXl8pPgTuLf0j3D9Szugj30bRDtqXWKUJK65poeGA7SOXA/bVrhm3tKDCT6Ka9gP9cP2vdSrvTPagnaQnouD5ACsMsUENtG45w+YRxbXtElAf0Tj+YIStTZu3Dg6ak5Yc0N5Q4IlsHVqaih+J9j3bFAFJrx2sMqvSWHNDeWNdt/1yK8aqOITugbCAtvJ8RTbwkZow7KwuHYMxcS1YEatkYglypMWCJZf1IheEyb6QgBMZzabr8SH2U+xsDwVNETAOiPlN5Xqz1WNvpvTuSd2yZIliu7YOfa5DXNOfWz1r31yNsN5T686fN9b61/LycnRy28HzGyr2y5+ctXOkjnFH/5IUy5nO/fDoyV7YUbZPpjpgXtapnc0mRpQJJvn37NK9sHjdFx1AB4u/xOMXboXhhRVQ6x9O0TRNMoC2thALFSFJg0Qb6+GS59cfjBhhm/iGna6PbF9v4kDkrD9M5rhK+SJ0aNH95erLuAmGvf8AfPH4po2CXlxTZ6qpllhzQ3lEQmWwOZ39Bp+J5j3bLCFtWBFrWlaWHNDeQyCwKZoNI0bvA4LbL4TNlFtmHcW146h2L0WjKg1Eq20FrEmQs6jsAxKQb5ROnpNmOgLXTSd0Wg8E9mDBT3iXXBGu2CDBHygTZbrUXN2TV753FPnbfx1sGUrXP7467/Yq1qvkt8KlOlynA1nP72i+a25xe//6IoWozXWaMrlXmkq6JzSfTALkdZAC6K45s1MWgcO8zm3+AA8vOpTmFr6R7j65TboW7gdYgvrQU8bAQhFqhAFyxNfUAtnPFn8yWmPl5wq199Jbfr06XHjxo57DQdDYftnNMX3yBMjR47sLVdfQE007vkD5o3FNW0SsB/ReC7VxaPU1FQ6al5Yc0N5RYIhsPkdvYafD9Y9G3SBCa8fjLKHhLDmBvOqto9U8w9dB2GBzX/cYlvIiWyYZzXac0SLa8GKztJyxJonmNegrMGmdPSaMNEXumC6xMTEi41G434sIAtrIcakiRPhzltvNcp1qTm73lIy97T5a38dkrMGblq8+lCgo9Yc5a3D5zt31cwtefeX2c49cLxw5hbStCGmnUDJPphdvBdmFe+HR0o+gftXfw6j3nkfznRsgXjbdogqbIZuBS3QzeESp/S2EzlBwAo6NJVVjN7WBHHWWrjEvv7wH8rfn5JTAx2uE4jtJXrChAlPY8dLkVHCe4DRFD8bDIYrsOoCHp0qGvf8AfMWrAd15uQE6sd+sESjTq0nFkwwzyEhsOFng3XPKhY14Qt4/WBErYWUsEZQfuV8i8qjFKq1DbwWC2ydxzOiLSTaNeaTxbVjKHKfBSNqTRarNB2x5gnlVW2BLdzENYpYuxQfhv6KhWNhLQSZOHHin2+//fbr5frUnE15ddPNlz7+xs9XPfYqPPBG5a1yckAsx+mMfXxZy8NzVu7+9dGSD2Cmc69YxNIiTqRkH8wp3gszSVzDvx8s/wRuWfUxXLawFnpYt4G+oAm6OYjjBSxPYU2bApsYmuIaa62Fix2VMK109x0+7kyrw/7pZuynDoraP6NJdiYlJZ2HdRdQgU007vkD5ovFNW0SqB/7wVhrze9oLK1A+b733ntFZVIan8VI/Gww7tmgi0x4fVXLfdttt9Ex5CJ9CMr3fffed0KZFERV4RWvxwJb1wkJoQ3zxuLaMcJCXDObzZvwGDLCmhvKs5x3YbkCTTiJa/qRI0degA+ufxIVlAkZXk5NTT1DrtOg2CvF5fZXStaMzsmpEUQigW7Wso13TH1+VfILzppecmKXjSLgnqnYlWRZ/cFHj5XvgxkIbVows/TYBgLBhTZNaJ/ZtFNoyd6jkWuPYtr08gNwT8UnMHzJDuhvq4Zoe4MkrLkRCVahBG1oIIlrdv/EtRtvvJHEtQOCts9oFBxXdo8YMeIcqj9XNXbdROOeP2C+WFzTJiEpro0fP56OISmsucH8B0OQ1Lq4pqp4IoLy4JUnpQl6mbsC5d+rPEqiuq/wmiSwkTDEIlvXIR9qUmDDfKnRjgM13iqd14DfZ2pPCVVjLTElUXP9NaV9JUz0BT9Nn5iYeD4WaC/CEWuhzcsjR44Mmrj2yMvFN/7forfefnjR6wZsh4rtFOhtC1ZtvbhgRc2OJ1e9CzNL98HDZfvhkfIDMOO4ddW0zSznXphZQuyHGcgjWIb7134Omcs/hiFF9RBrrZPWKesWUhwTA72hssTaWFyLFLDO3h0xYsQguR67bN5jnr9gnoLxoM50TKB+7KstFIXcdFBvsAzBmB7K4tpJwOurPdUx5KaDekP5l8shKl+gCZq/6LoIC2xdR5ORbJgXFteOEfB+WO2otVBZZ609sAxhE70mTPQFP0yPDz3XGQwGelhlYS30CZq4Nvv113vfbnvp1XueXbblodfX/0ZOVtyWLNkVY1/ecNvT7zT9/Fjxx9IunA+XfwKPrj4IM8sPwKwysZilNUhckwQ2KZrtAMwo3Q8PVHwK2c5P4JJFLdAjvxb09nro5gglxMIaweJa5EECGw6YARHYROOeP2B+WFzTJl3+EY3nUFUkuuWWW+gY0lFrbqgco0ePPqGMCuLzVFr8nNr3bMRNCUWCKiYGCiqHV7mUJGg+w2tzFFtg0UwkG+ZDjTYckeKa2lFroTod1JtwiV4TJvqCj6bDCh+CDzwfY0FYWAsPgiauZc+d23f0jLzXx86x3psDEPAFzNsz2+q2AfkluysfL94Dc0iYKj0Aj5Z9ghx0Ra6FirhWthdmliOUXyexXxIJ7y7/Aq5/cTf0t9RBtK0edEIRS4v4ELnG00IjDoPB8FBiYmJ/qktXlXbOROOeP2Be1H5oZXwjEOIaR611ASqPV/mUxif/4efUvGc1EcGFeYi4MgcCKsddd90lKqMSBF2QxDxwFFvgcEeyRYKwHpHiGketdQ4sS1hErwkTfcEH02Fln4MPqQXIt6JCMSFJ0MS1vDcrrviD9fniu3KKJstJihu2dZ1l9QfjH1/1/pE50sYA+2BW6V5pauiM0gMaWnOtY2aU7ZWYWbof5pQcgMeK90tlmFb+BaS8/jGcbmuAOGudtFYZrVkmFKy81jXTMq4112pYXIswDAbDjzj2PN7VKaLeY56/YF5YXNMmISWuZWdn0zEsotbcUHnwR+0JZVUQLYprQRdMCMqHV76URBNlDhRUHq/yKYVW2gpHsQWWoIpseF012m9A2i6dx+u8gSag95ia4lq4RK25UdN3ISeuUeQAZrwI+cazIEzIEzRx7bnSbUnWN8sefuyF1y6UkxS3RRXv9Zvv3L1rZsk+mFH2Ccwq3QdzSj+C2aUfyxFhHjtyBhvaEdQTj/dmITPK9sOj5SQOHoC5JQfg8eL9MKtkPzxY/gXctPwzONfRBN0tdRBlrz8qUIWqsEaQuBYjiWvrWVyLPOgfOlOzs7Oj5Hr120Tjnj/g9dV8aGV8p0s/ovH7aq8bFlZRa26oXF7lVBIW1wRgHlRbO2zKlCl0DIuoNTdUnvT09BPKqgCaivijvCAkCrHQFhjIh6rXL16TxbVjhKy4ppRAFCzCYWqoMNEXOjKz2TwYH1CLMfM/eheGCWmCuqEBmmqbGJAtLGua+VTxDjlijcQ0Etc+Rvbia3/EtU/EOD+RosroM67jAZhThp8nKM2DY8IZfu449kvMwdfeuK/vFtdmlO+XprE+hu89TjuI4nF6+R/htpI/waVFLdAzv1aaGqo/Tqhq8ngdSjRAnL0GLrWvg/tXs7gWYfxoMBjmJCQk+FLnQhONe/6AeWBxTZt0VVxTTRRKd0V3hVXUmhssl5rimk/rruFnIk1ci6jyKsHsWbNEZVUCTfoP88XTRQOD6lFseC0W144R0PtLTXEtXKaEusEyhfzUUGGiL3Rk+HAajQxHyjHzLLCFD8EW11QzWtfNVtL4wdMlu+Gxko9gjnMvzCndB7NLad01wiVcdYxb5HIJarM8cX4CM2URjDYaICRhTQLfl6LlPoUZzoMwq+RTmO38FPOBaSX4+eIDMLPkAL7GPDn3YToJbB5I15XPTZTh5+U14ubi+4+VYBnwvNPLPoe7nF/AVQtboE9+DcTY6iHK5hKo9HiMsjVJf9Nr4kQRS6s0QHxBDVxiW/PVNOeuxGyn05coJhbXQhysu8N4fAd/cFyO9dnptRlF454/YB5YXNMmXfoRjd/XXMRVKIJl01wEIH6GxTUFSE1NpWNYRa25wXKp1WY0Ka4RmDfv6aIXyUfGf1SLYsPrqNF2I05cU3Mzg3CbEupGzei1kBLXyGhKTlJS0rX4sLMWC3DIu0BMSBIx4lpBSd0fnl7Z9M3TzvfhiZKPYHbJx1KEGK2zJq235jx+TbN2kcU4WuuMkCLI6Cifx7V220H5NYlrB2AOHiUhz7kXr7sPZlKUWfFB/NsV7Taz5BN4tORTiUfwb0l8kxBcH6G8uq7pysMcPN8cPC+991DZZ3BP2Rcw7Pkd0M9aC7G2Oi9xzUUoimvdHdthqKX089Tn1/jaZllcC3Gw7g4aDIbLcnJyurTpiWjc8wfMi5oP6ozvsLimEah8XuVVEi2Ja1rYJVS1KaEPTX8ILynOR6iD5YuYNtMRlD+ERLZ+CEezdR5VBDa8BotrxwiYuMZTQrsOlu0W77IqRciJa7Lp8WHnaixABfKLZ4GYkCRixLV5y6o/mVeyG54o+QDmFn8Ec0n4IrFKFsOOTtc8CZKoVrYPkUU1fD2jnNZvozQZ2iChZA+em3Yj/QAeX7ULnlzZCk+taHKxvBmPbfDUyp3wxKr3MS/42WI8R8kBSVh72PkpPFr6CcyQ+PQoM/FvyidFr7nycQCvS3k/gNeisuyFWc598HDZp/D71X8C45L3YICtFmKtJK7RpgYNXuIa/e1KDwVozbUe9i2QYFl50JizYqBcrR0Zi2shDNbbv5CburLWmttE454/YH5YXNMmISGujR49mo5hOSXUDZXPbDafUHaF0JK4FrAHuc5CefDKk5IEvbxKgWWLnzRpkqjMShAyfsS8eq7L5hnVxvgG+Yv8ppjIhudmce0YAbu31BTXwm1KqBssm2pTQ0NVXOtGEQTopKFYiJX44ENTdoQFZEKCiBDXnt24sc9Tyxq+f7z4fZhT/AHMlsQvmhJ6vLjmmnbZPpKgVU6ilmtapiuKjdZro3Xb9sBjzg/gyZLdML+k7Ttbxe7aeaXvTn16WcOEp5dtm/D029sm5MpYMC0XeWJZ24Sc4t1P5Zbs+sdTq3YdduXPlTfK06Nln8Ij5Z/DI2V/xNefS4IbXZOmtNKR8kL5oKmtJK6RsPcI5u2Bij9B+psfwan2Ooi11kKUrR50Dtfaay5xjcQqVxrRLQTQY1l6WjbC8PxlB405S1hcC3Owzv6N48wEPMbLddklE417/oB5YnFNm3T6RzR+V7WpjA8/9DBeUpyPcOL+P9wvLL8CdLjuGr7P4poyhIwo1BkefvhhUZmVIGT9iHn3FNtYaPOdM0X+DAR4bhbXjhGQfPKU0MChlkgZsuIaGQlsSUlJ5+GDz3IszBHvwjEhQ0SIa7a3qm5/atWuX+c6aa21jxGaorlfEsyOiWsnimne0LpqM8pcUWSz8PtzS/bCE8498MSK3fD08tZf81c0HCpYVb9uYWn9HYvXv3tRTs0f45cs2RWzZMkSLyhtVwwtyu9w7upbVNxkcJQ0/i5/VdOi3BXNf3yqePeRuZjPGRSJ5jwID5d9Dg85acoo5pWmf0oC3F6gCDrXumtUFhLc9sKj+PdDFV/AuKX74DRHPcRL00Jpx9C6E8Q1t8AmErO0BolrvfM3QFpRycExCzhyLZzB+vovclOghDUy0bjnD5gvFte0Sad/RON3eUpogKFyepVbSU7qU3yfxTVlCFlRyBeofF7lVYqw8COWw3uNNqZ9FJsiiudVo90GpM3SebzOG2gCkk81o9aUEIW0hFq+VGLHUGGiL3TSdCNGjDjHYDD8W1RARvuMHj267qabbrpMrs+wtblvVT/4WPF7R2h3UFeU1wEXkmDmGbl2cmgdtBkln8DM4n3w2Ko9kLPqfchbufOPluVNy60ra2/OX7X9gqKlmwe99VYNDZ4+74TqdELUwpLm7osqavrlrahOyHmnuvrJZa2/zi3+QBLYHin9VBLXaOqoS1z7CPPysSSskcA2q4zK9bEETU19pOIzmLhsPwwpbJTFtQbQSYKaW1wLvTXXorEcA60b4a63qg9OW1HD4loYg/W1PCEhoa9chwEx7zHPXzBfaj68Mr7T6R/R+F0W1wIMldOr3ErC4poM5cErT4owefJkOmp6rbCuguWLmHYTSLA83lNH3bDodjyKCGx4TjXaLYtrChHu4loob2ogTPSFzhqthYMPQr+hhyFRARltM27cuEO33HJLklydYWv5ZTv+Mce5xyWOOQ8cg4Q1PFIUmvemAe1Ssg/mFH8EOcW7wFHcWlOwouUiij7LcTpj5ct1yWhX04UlNRcWljS8Nn9l63ePFb8v7Tr6sPMTj8g1l7hG+ZE2VSjF16UfwexSLGPpXpix+lOYvGw/nFXUDHHWeoi2N7nENVlY07a4hnk97rXr7xhbLQzKXQtJz1SOp4g/2V0ntZzs7NiJEyc+l56WxhuwhAg4AC9PSko6C6vPZ3HaFxONe/6AeVPrgYvxD82Laxnp6XQM6/XW3FA58YftCT5QCC2IaxG1mQESVoKQCCqjV5mVIux9SWA5ObLtRAIusOH5WFw7RsiJa/jbdzUeaeH/sATLtwqPwrIHmpAX12TTp6SkDDAajW9goXiKaAiRmZlJ0WvJcj2GrT25ouVnilib6bExgJtj00HF0WqezKH1zZwfwFPFu/5bUL7rgWdX77zA6XR2ebF1b8N7UkeRbAucjffNW9X85ZySD+BRimBzYhlK9kri2mwnRapR5BqtA7cPZpbvgVllsrhW/incvGw/nLOwBeKsdRAjC1R6WzNESTSFkLjWDDpHA8TaamBI3hpIWLAuMdtHn0+fPj1uzJgxr6ampgrbP6MpaOxYqYSwRuY95vkL5o3FNW2ieXHtt1On4uXEeQhHpmJ5RX5QAC2Ia0EXSCgPXnlSjFGjRsG4sWNh8qTJMHHCRLjpppuETDjKhE4xUUb0ni/4+n0qx/jx42HixIkwftx4mDBhAmRhGUVlVwDN7xgaSKisiHc020XyMRIJqMCG52Jx7RghJ64xgSNcxDXJbrjhhsFGo7FMVFBGm5hMJpqClUlr6MnVGJb22PIdP9M0UGmttFLXFFAXB6TjHKeLuchjJL6V0IYCNN3yAMwu+wRmY/qjK/bCY+Wf4mfeP/L0iroPcnJqovHeCbgI4Gk0XfSZkoaHn3i79rvZxe+5did17oW5pXthdgltokAbKlAZaM01mhq6V4pym1H+Ody8/CCcs6AV4ix1EE0imiSkNct4CljaggTAaCtBAiCC+aVIux6W7XCJdQ0kP7fhRl/FtXHjxvXGNv62qO0zmmMVLTOA1aZIX+Q95vkL5o/FNW1BD6ZUJ51+OMHvqjWFMSKmhLqZPXu2yAdKwOIagnnog5znkScmdAh6+wkGWG630NZPPkZqZFvABDY8D4trx2BxLYIJK3ENTZeSknKx0WjcJioso01MJlMWTe+V6zDs7JX1LSueKH7/MIlnJ66r5opYk4S1Ejyu2gePle6HObR2WfGH8Ojy92DGig9hLglzK/dJxydW7jhkWd54tXx6xY2Ez+fKG5+YX9z44+zyD2FOxQGYW7YfHi/DI+ZnDkLlkI4krJUcgEfL/wg3ryBxrQViLbWyuNYAx6ZZalRcczRKwlqshfLdJEXZ6e2tEIP575O/Fa4rWAsTX9vss7iGfRH9cFvq3eYZbWEwGP6E/dBVWGWKifyicc8fMJ9qi2tu8YgR0+UHEjwHi2sKQOX1Kr9SsLiGUB688sSEDhEprolAX3iv2RYpYltABDY8R8j0d3Qer/MGGhbXIphwE9ckISAlJeUaLNx278Iy2mT06NG/mzZtWg+5CsPOnitrLCFxbXaJx7ppJ3BQik6bXbIfaBOBx1e2/GApbWt8smz3OZbV721/3Pmh9P05xXvBUtz6r6VrW8+TT6+K0X21aG3zI3NXNR+aU74XHlm1V86vK/+zS1yviRlYhkdXf+4lrjVAN0lcO17M6uY4/u+g42iQItZiLIi1EfSOZtAXtElrxvXN3QI3ONbXZr1QeZrslg6NxTXtg3X0DXJrQkKCon2QaNzzB8yrmg+wETVdKBigf2ORwbK/lYbFNWVgcQ2hPHjliQkdWFxrB/RNJIltZ4p84A94DhbXjtHlfNKOk7TzpODcjMYJO3GNjIQALNwNcgTbYc8CM9pjwoQJ6+68886z5eoLK8O2rVtYUud8bOV7hyUx6oTINReznJ/AoyU0ZfQTeKz4PcgvbmizlGwbQudYWFZrenpF649znfth9rKdh63FTb+RTh4Es5TUfjq7+L0jj0prx33usW7cJ3I5PMW1Ax7iWv0J4hoJa5oS1xyYP0c9RNkbJDEwGo/6wmbQFbVJa8adkrMRbn5t+7Z7V24bLLujQ7vtttuyMzMzD4raPaMJvsVx4j6kl1xlipn3mOcvmFc1H2D5gUth0MdqCUAEi2vKQOJouxtF4HssrjFah/t6H0FfhfOabV2OXsPvs7h2jC7nk6PWQpewFNfIaJqhwWAw4UNTLRaUd+rTMNgIN6SmpoaluPbO1rYBlqWbPnyi+IMjNPXTW1Q7ivMTmOH8TBLZnizeBbnLG0bl1NRE0zleKm8alLOs4dMnnXuPPPVO8/YlPu5UqYQ5nA1nP7GyZd8s5z6YUUYCm6e4RlFsNC2UxLXP4OYV++GcBTTFskYSqnT2Og8xqwG6Oeo1BuYP0ePrKHs9HhtAX9QEUUUtEIv5HThvHfy+uMWZs27XQNkdHZnebDb/Htv4P7zbPBN0aPOCfyEP4xjRT64vRU007vkD5pXFtTACfczimkJQeb3KryTt+hbfY3GN0Trc13cC9JtozTZP0S0U6ZLAht9lce0YLK5FMGErrpGRwGYymdLlCLYfvQvPaIbtKSkp58vVFna2oLimZM6yXdKaa56CGkV5zSyh3TeR4gMwo/gTmFOyD3JXtHxbtKohmTYTkE/Rbf6bW97NK9l9JOed+hfkpKCZteTdoU843/1mVuk+oM0MXOIalWc/lmEfPIpleKT8U5i8fB+cXdQEcZZaiHXQFEuKDNOuuKYrqAM9Qnlz72IaVdAsRbL1tFbDuXnrDl1kqaC17nzaRIL6H2zbD2D/80+v9s4EEayPw8in+Pr3aglrZKJxzx8wvyyuhRHoY1UEIHmn4najq8IRKi/+pjjBFwoR0eIaXp9EBppGLsobo324rw8g6M9Qn0ra6emh+F0W147B4loEE9biGhk+PEXLEWyVWODvvR3ABB+z2fxJeno6LSau6M6XwbLnyppK5q58t0NxjdYre7LkI7AVNy8rWNN4hvx1yRav3P5W0fLtP768pm2CnBQ0o8X8Lc6m+sdK3sNy0G6hrsg1V3n2wQxJXPsMJi3dB2cWNkG8pQ5iBNM/tTcttB70BfV4pM0WXLuaRuHrGGst9J2/Ea5yVL47fOHaC2U3dGi0Uyh2sLkmk+k7UbtnggJFrB3E8eB3mZmZfeSqUsVE454/YL5ZXAsj0MeqiGt33H4HXk6ch3Bm3NhxQn8oQKSLa2r2S0zg4b5eQdC/brEtVEQ2FtcCA4trEQytlUdr5onqtTMIE31BSZMjSG5E1iM/uAvPaAOz2UykkRAqV1lY2XOlLbmPr3z/19lOzw0MXFMoSYwiZhXvh8edeyGvZOc/HaUto7ynfr66auvFLyyrtOG9ogkB8oW1zf83r7j1L3Mwz1QuN7NKXNNCHyn7DCa8sxfOcDRAvLVeWrPMJWIdv0uo1sQ1XQFFsTVDN3srprVIGxnE51fD4KdXw7C8Va+m5SwdJLugQ8M2PQR522QyCds9ozokrH2G/cz/IapFrLnNe8zzF8w7i2thBPpYFXHtgfsfwMuJ8xDOjB8/XugPBWBxTZwvJjTgvl4F0M+i9dq0SKenhuL3WFw7BotrEU4go9eEib6gtMlrsCVhgUlg4ymiGiM9Pf3uyZMnd5erK6zs5bKWi+aufPeQZ+TaHOc+eNz5ETxe/D7MLf4Q5hTvgSdWvfsPy8qm3KLyJp8FnGCZ0+mMylvVtOjJEsy7JKxRmQ4geyWx8OGyz2DcW3vhNHu9JK5F0zRLWwPoJXHNjZe4pQqe1/cE36NpqwU0dbUF/26DKHsrxNiaoUfeNjhvXumRcQvLC+59reoU2QUdWmZm5tCUlJR1ovbOqI57KugD2Nf4XIeBNNG45w+YdxbXwgj0sSri2qyZs/By4jyEM/fde5/QHwoQTHEt6Lv60vXlfIjyx2gf7utVBn2udaGtU9Fr+D0W147B4lqEExHiGpkssI3AQpfigxatg0SRDEKnMOqCjdBpNpt93oUx1Gz2sh0/k6g2kwSo0gPwWPEHP+WV7a7JL2lbnFuy68DTq3bDvJWtK+wrGkJm7TnatdRWsuvwYxSBR8JhyT6YW/IRzHbugenln0HGm3vhVEcjxFobIEpewyy4kIjmmu4pBt8vwGPBDtDbWzDPTRDraIVellq4trDy50mvbJqY43TGysXvyHQZGRkTU1JS/ihq74yqUD//56SkpHtTU1P7yvWjuonGPX/AMrC4Fkagj1UR1+bOmYuXE+chnJn2u98J/aEAwRTXNHGfUj688sWEDtzXBxH0vxanjXYqeg2/w+LaMVhci3AiRlwjkwW2K+hBC49/RwewwKYNPsA6OU+uprCzx5e3/jyTdtiUorv2wROrdn27aM2OgsVrWwbbS5oetJU0v1tY1nj/knW7eshf0bw9u/Fg3ILipp/nrNrjWnNNEtc+hNmle+CB8s8h+bWPob+jSYpa0wvFLrXxjFRrh4IW6FbYBjpHM+htddJ01j552yDjpe3/u+vtumFy0Ts06mdMJtP9CE9DDz6/GI3Gt9VeY83bROOeP2A5WFwLI9DHqohrSLviTzhz/x/+IPKFErC4pm7fxAQW7us1ANaD1kQ2v6PX8Dssrh2DxbUIJ6LENdl0V155Zc+kpKTb8aHrr+gEFtiCTEpKynejR49OxfoIy3XX8kp3Nc8q+VjavGBOyUfwxPLGfYvLd/yG3ntmxdbzXyqvfnBJ+dbr8V4ImU0dcmpqootKmhbNXfk+zCyl9db2wlznhzCrbC/8vuxzGPHSB9DHTlFrtEkA7Q56ItJUTKEQFiQcTdCtoAV0BU0QZa+DOFsNDMnfAKmLN77yu2V1p8tF79CmTp16WlZW1vPYtg97t3VGdRoSExOvxGoJ6r3lPeb5C5aDxbUwAn3M4pqCPPjAAyJfKAGLa+r2TUxg4b5eQ2B9aEVkY3Gta7C4FuFEorgmWUJCQg+j0ZiJTvizt1MYdaFF35HbMjMz4+TqCSvLLd31xJySPUekjQtK9vz41PLGRTkAenpvyZIlMSsqK/u/VVMTL304RCwnB/SO0uabn1i18xAJa7Oce2GOcw/MLD8A95Z9Adc+txt62epAbydxjTYKCCYk5h3bobQ9pOmhtOZaQSNEOeqgp6UKzptX8rfr81dm0i6pctE7NGzHF5jN5mJRW2fUA/v3euRqiiSUqyZoJhr3/AHLw+JaGIE+ZnFNQf7vwQdFvlACFtfU7ZuYwMJ9vQbBeiGRLZgCG4trXYPFtQgnYsU1sqFDh8biwxf6IZmmiAodxKjDhAkTGm+99daBctWEleWvbHjwyZIPj8wtOQCPr/zg+5zlDU/Jb4W0Fa5uvCDfuePfs4o/gpnFH8Ps0o/hkfKDcKfzC7hsQSt0t9aC3hFi4hpND8XPx9hrYIBlPSTYS98bVbDyOrnIPtn48eMz0LhPCSLYr1ebTKbLc3JyJBE72CYa9/wBy8TiWhiBPmZxTUFYXFMPyodXvpjQgft6jYJ1E0yBze911/DzLK4dg8W1CCeixTWyhISEGIPBYMKHsa9FDmLUwWw2/wsfhs/BKgmZqZG+mvWtDQ8+uWr3kadKP4GnVn7wvX1l00nFtRUr1g1cubL0copqk5M0ac8ua+1jW737hcede6QNDWaV7oeHyj+Dm1d9BhcUNkO8teakkWuUrrPT1FCl8Zj6eVKaQE+7mtoaIc5WC2flrYZr85fPyn7B2Usucocmr7d2G8JTQoME+r4R+/SLtCKskYnGPX/AcrG4Fkagj1lcU5AH7udpoWpB+fDKFxM6cF+vYbB+gimw+RW9hp9nce0YLK5FOBEvrpGRwGY0GhPxgewfIicxymM2mw/fdtttj06fPj3spoYuXL7N9MTylkNPrvwYclfu/FvR0s095beEtmTJkoFvLCsZqnVxjXbPfHJZ29Q5K96DWcX7pLXXHqz4HMYu3Q9nOZogzloLUSRuSZFjxyOtt0YIRa7gQBsvRNtcu5t2t9TApdY1X5qKSm+m9eXkIndo48eP75eampqHbZrXcgwODSkpKbTrrmaENTLRuOcPWC4W18II9DGLawrCGxqoA+aBHv7P8cgTE1oo2obw/O41xPrJR8LvnSgjGfIXEgyBjcW1zsPiWoTD4ppsFHEycuTIS4xG4wKEH4yDQz76PqTWHvPFnE5nVN6K5u/mvrPziG15Xa2c3K7hPaF7+c1lC956q6KfnKRJW7JkV8yTK5pvf3zlu/CY8wDMLD0I91f8EVJf/xBOt9VDnLVOEqpEQpYW0TtIXKuXdjjtkbcdbnihbte9Ze/dKBfXJ0tJSbnYZDJtE7RtRkEMBsNXyNikpCTaeEJz0a/eY56/YBlZXAsj0McsrinInXfcIfKFEkS6uKZmv8QEHr/aEH7eLZb5ilsUukg+En6v5xXpoM+CIbD5NTUUP8vi2jFYXItwWFw73nTDhw8fYjQaX0F4WpfK0DpVZrN5sFwXYWWW5bXfzV/W8kvB8rpb5KST2ptvrjqLRDn5T02aJK4trbv9sRVt8FgpRa59Ar+v+BMkLnkPBthqIZY2NLA1SojELK1B4lqstQHiLfVwmm0bXL1w27wHnHv8mhI6bty40enp6d+J2jejHAaDYaIszGtyWrlo3PMHLCOLa2EE+lgVce3xxx7Hy4nzEM5MueUWoT8UgMU1cd6Y0OC4NoR/dySeBULgYXGtE6DfgiGw+VxX+FkW147B4lqEw+LaiaY3mUyXoHNKvJ3FKE9iYiJN6Qq7ddcKVmz9zrF08z9er2yideXCwpbs2hVjK66+/alVDfCY8yOYVfYp3F36R7jquTboba+BKFpvTSBiqQ+tpdZ8VOgTQWuzRdnqobulAXrMr4LzHes/uPH5DWlyUX0yml6Obfh27zbNKIfRaDyEOJDTsAo022+Ixj1/wLKyuBZGoI9VEdcefuhhvJw4D+HMpIkThf5QABbXxHljQgOa0ns0Oglfk4Am+lwgYXGtk5DvvHypNCyudQ4W1yIcFtcERgthGwyGa9BBG7wdxigLNshyOQIlrOz54qo1L6yqeln+M2wsb9WWy+YV1783u+R9eLTsU7h5+UE4r7AB4m01rp1ChWKXNolC4nJr4PT8DTBiUeWG3y6vvUIupk+WlZV1TkZGxieids0EHuwnvkV+i33GSdcw1IKJxj1/wPKyuBZGoI9VEdfuu/c+vJw4D+HMuHHjhP5QAF5zjddcC3WOtiN8zeKahkHfqR29xuJa52BxLcJhca190+OD29UGg+F1PP4LncXrsKmAyWRqGT16dH+5DsLGaFH8bI1P8+yMOVZtuSy3uP69WaUfwIMVn0H6Gx/BYHsNxDhqQ0xco+i2JuhtrYFrizZ8l/3yxll+TsvVpaSkkCD/N+82zQQe7Je/xH75zlAQ1shE454/YJlZXAsj0MeqiGtTpkzBy4nzEK5guWOxLxb6QwEiWlwjKB9e+VIKEvHoWkzXcQuiHLkWYpD/vPypJCyudY6QEtdwvKyh6zGdg/zn7cdRo0ZdJarXziBM9AUNm27cuHG98UFuDj7I/QnhddgUxmQyQVJSkgF9r6nd/tjE5lhVe1l+act7j5Z9DFPLPoXrXtgB/azbIaagDnSOeuimESgv7dMAOlszRFkaYYBty+FrCyvW3vfO1ovlIvpk2DdEY8c6g9qvqF0zAeMI+vpv6Odp1DfL7te8icY9f8Byq/HD1Q2LawqDPlZFXEtNTaVjrCgP4QqWVxXfIoORdn2L72n+AS4QUD688qUEx4lATNcgXyJUb8f5FP9WQ7jxa6F85njId7IPRb4NNCyudY6QEddIEAqkEBSJkP8CLah5Ikz0Ba0bPsz1MhgMD2ND/ARf/+rdOJnAgj5enpmZ2Ud2P5uGbWFJ8xX5JTv3PLT6E5hQfAAufKYRelqrIVoWrURCVzAQi2puaL21JuieXwfn2Df827Ro9eM5b9X4NTV5zJgxA3GQahW1ZyYwYL9wGPkUuR/R9E663iYa9/wBy6/GD1c3LK4pDPpYLQGIaDe6Khyh8nqVXylO6ld8X/MPcIGA8uGVLyXgPkkF0M9qRUVx9FoXIP95+VMpWFzrHCEjrtF1RNdntIMw0RdCwfBhLl5+qKvEBvmtdwNlAsq/MzMzE9HtYbexQTgZTZu0LWtNf3L57i8fqPgCzO/sgdMK6iDeWgfRNNXS0QjdJIEtuEiRaSfD3gBxtnoYZN18+Gp7ecP9zoYEuYg+26hRo8Zgu/3Fqx0zAQL7XRLWPjYYDPekpqb2ld0eMiYa9/wBfcDiWhiBPmZxTSGovF7lVwoW1xDKh1e+lID7JBVAP7O4FgKg/9SKXmNxrXOwuMYEDGGiL4SKkcAm7yT6EvKdZwNlAkt6enrphRdeGCe7nk2DtmTJrpj8pc23P7b8ffit83O45qWd0NdSDXHWRoiyNXutaaZd9PYG6GHdBhdYK765+bUqK62PJxfRV9ObzeaVonbMBAT6Z8YW7HvvCaWpoJ4mGvf8AcvP4loYgT6ORWhaocj/gYbFNWVgcQ2hfHjlSwl4WqgKoI9ZXAsRyIdePlUCFtc6B4trTMAQJvpCiJkuKSnpPGyUzyM/ejZSJnDgg/T3GRkZl8s+Z9Ogkbg2f2nb7XNW7oHsZQfhgqJG6JFXDTGWRoiyNIPO1iQUs4KF3kZHV6QavdbbmqVNDKJsddDHuvHnqxyl6x4vrb9SLp6vphs1atR12F6/ErVjpsv8gCxDbgjFiDW3icY9f8Dys7gWZqCfNSEChRtUXq/yKwWLawjlwytfSsH9ksKgj1lcCxHIh14+VQKf18fDz7G4dgwW15iAIUz0hRA0HT5Mn2MwGAqNRuN+bKC8k2iAQb/+ije9JScnhzc20KgtLGnu/kTpe3MfcB4E06t7YJClBrrn10K0lQSrFqAdOEUiV6BxCWUdQXmqB729Bl/XQ7SjBaKsLRBta4butm1wRm7J329cWDJBLprPNnz48O4pKSlvitow0zWoD0CKb7zxxqtCvR8QjXv+gP5gcS3MQD+zuKYA06ZNE/lACVhcQygfXvlSCu6XFAZ93A+5yMPnSsGbGnQR9J+mhFD8HItrx2BxjQkYwkRfCFHT4YPfaUgm0oiNlAW2AGMymf6alZV1juxvNo1ZTnnToMdXv7fztpKDcNniHdA7rwbirPXS5gB6u3riWofYaJpqI8TYaC24GswbiWttoLc0Q097A/R9uvzwsIVlW7KXOP2OjBo3btwN2E7/I2q/TJdZjX3rb9DNIS+wi8Y9f0BfsLgWZqCfVRHXfj9tGl5OnIdwZPLkyUI/KACLawjlwytfSsH9ksKgj9USbAiOXusC5D8vfyoFi2v+0+V80s6TtJOn4NwBhXcL1T7CRF8IZcOHv2iDwXA9NlK6CVhgCzCpqanPYBvhjQ00ZlQnRWt3mR8ufvf7zKUfwWB7LcRbaiHa1gB6ErQctOaadqaF6jEvMdYGiEZ09mbQF+2UBLc+liq40lL804TXNqTJRfPZ6N7PzMxcL2q3TOfB/vRLPNoTExOHopvDInLVc7zrDOgPFtfCDPSzKuIa/nCmY6woD+EGldNsNp/gA4VgcQ2hfHjlSyl43TWFQf+qKa5x9FoXQN/xtNBOQufxOm+gCUg+OXqNIYSJvhDqlp2dHYUPhJfhw/Z9yPeixst0jvS0tH+np6efJbuaTSOG960uf/1Hyfet/OB/Vz/XDL0s2yHaQdMuSbwiQcstrLnWOAsqjkbQIzRdNdpKEXVtoCtqhVj7djhtfjHc9fbm2ukbN/q9ecb48eOTU1JS+H4PINiP/h2Po7EfHYguDpsp4d5jnr+gT1hcCzPQz2pNCyUiYmooldOr3ErC4hqC+aCdC0n4EuUx0HDfpCDoX7V2oXTD0WudhHzn5Usl8Ll+8LMsrh2DxTUmYAgTfSFMTIcNtKfJZLoNG+vX3o2X6RzozyNjxoxZyWuvactyAPSPrdl398S33j08OH8LxNpqQV/UCLpCl5hFwlo3RwNSrzi6DtAXuPIUbWuBGOsuiLLthNiCeuhnWwvDFxT/9cFVWy+Wi+WzJSQkxJjN5iexjR72brNMp/krYszMzAy7XYJF454/oF9YXAsz0M8srgUYKqdXuZWCdno9aTQgvh8R4hpBefHKm1Jw36Qw6GOeGhoCkO+8fKkELK51DhbXmIAhTPSFcDIS2AwGw1hssKsRniYaAFJSUv4ybty4a2QXs2nAnt148NT/W/XR/mHPNEGfXIpaawR9URN0k8S1es2Ja3oHbWiwA6Lyd0JsfjP0s1bBNQvK4ZHy2kLsg/yednzT2LE3Y9v8t3dbZfzHaDQewuOfkpKSzEOHDo2VXRxW5j3m+Qv6h8W1MAP93JN87eF3JQl7cQ3LGHvvvfeKyq4EHfoTPxMSD3CBgPLilTeliJi+CctKUWTkV0K16ZN4LZ4aGgKg31hc6yR0Hq/zBhoW1zQKrS9H5SVCZa05YaIvhJvRA6LZbB6GjbbauxEzneKn9PT0Fzh6TTuWu36vZfxruw6fnV8D8ZZ60Be2gK6oCXQF9fJ0TO2st6azNUCUoxn0tjaItrRC3/xaOH3uUrj5lXUv5azb1UMuks9Ga61lZGTQvc1RawEA/WnBge48igaUXRx2Jhr3/AH9pNbDK8Himgqgn1WLXJswYQIdw3rdNSyfpiIB8TMh8QAXCCgvXnlTikgS1zx9StNu6e/2CJhAhedSU1wjOHrNT9Bnak3fZXGtcwQkn7ypQeDxFCyp3G6hTYRWfCJM9IVwNHoAx8rDg7ES4WmiXeMI+vDjjIyMy2X3sgXRcpx7Yn+3ckfV1YsaoE9uDUTbmkFX0Ao6R5MrWsxOUWvaEdei7E0Qa2+FaMdOiHO0wqCc9WC2lxwcs2Sd38IaGk3/HoVt8luvNsp0AryvF5vN5sHkV5d7w9NE454/oK/U+OHqhsU1FUA/xyI0vVBUB0oQ1tFrVD6v8ioJi2seUF688qYUEbGpAZVRLqvIByI6Et/cdOg7+gyi5rprHL3mJ+gvtQRQFtc6R8D6ZhJ5BOcPOHQd0fXDDX/82ZH4lpGRMYWOSotwwkRfCGPTJyYm9scHSCvCD+NdwGQyHc7MzFw7fPjw7rJv2YJk1tVNG9Jf3Hp4kGULxOc3QJS9DfSOVnBFqzVAN2kTAXXFNb3teDzTou3NEGVtgriCNojL3wrXL9oMty/Zej12Pn4LOtjZDsC2WIX386+idsr4BvkPWZiUlHQ6ujXsdwMWjXv+gD5T44erGxbXVAJ9zeuuBQAsW+zUqVNFZVYKFtc8wLz4KwZ1hbDvn6iMXmUOFG4R7qRiFr7P0Wsahvzl5T+lYHGtc4ScuBYJ0WuBjgQ0m8076OgpwinhQ2GiL4S56fAhciBWwBLkB3elMP6DDfiLiRMn3kk+dbmWTW3LW9t2+Z1vVH97QcFW6G6phRgrrWXWAnobTbs8XtxSB4qWa4ZoewvEkoBmaYBoaz1E2/BIwhrmi96jnUL72evgnPlOuH9FzcbZrzf2lovks9G05JvG3lRsSjb9LGqfjM8cNJlM06hfRLdGxL0sGvf8AX2mxg9XNyyuqQT6WjVxLSMjg45hOTUUy6WmSEmwuOYF5ccrf0oR9tFrWD6lfXlSH+J7aotrHL3mI+Qn2V8iPwYaFtc6R8iJawRdS5SHcEENXyrhQ2GiL0SAkcB2GnIf8ilWAG900Dl+RF5GH/aT/cqmrukeKml+K6Fo05G++VslAcslpgVzCiiJay2SwBdvaYb4/CaItjbi3wS9boYY/LuXvRZOn7caRj+3btM9Bf4La2RjM8deyfdv10D/fW4wGCaE68YF7Zlo3PMH9J1aD64Ei2sqgb7mqaEBgMrlVU4l6XCnUAI/EzIPcIGA8uOVPyUJ2z4Ky6ZWFGC7PsT31J4aSrDA5gPoIzWFTxbXOkfA+ic1xbVwj15Tw5dK+FCY6AuRYtnZ2VH4YEnrNb2LkFDED+l+kp6e/t3EiROfirSHcy3YQxW7ExKfqfx6cG4lxOdvA729HqHNC9pDJIYpAG1WYG+WhTR63YJptAZcC0TbmqBXfg2cmbf+yLhXtv59wtKmQXJx/LLMzMw+2P6KkO882yPjMz9i3/eR0WjMDueNC9oz0bjnD+g/fnANU9DfqglDt9xyCx3DKnqNyiNH5amFTwIlfo7FNeUI2+g1LJdaftRa9BrB00NPAvpHTdHTL7ETP6tGu404cS3QUxk7Ilyj19T0Y6B9KEz0hUgyEthSU1PPSEpKWoyV8CU+bPKOg/5xxGw2b83MzLwe3cm7h6pkU1a2DL55eeuXZ9qqoJd1K8Taa0BvrwNpA4P2UEtgcxAUPdeCeWrD1ztAV9QGUQVN0N1aB2fMX38kyeE8cPcbm0dNW7LLb2EH79Fo7JhnYtv7t1dbZDqG/oHwH+QZk8l0AfZ/ESmKi8Y9f0D/qfngyuKaiqC/NTelMZSg8niVT2lYXBOA+VFz3TUi7PopLJNmfIjvBUNc4+i1diC/yP4R+U0J/BI68fMsrh0joH2TmtFr4SiuhbpAKUz0hQg0XWpqal98aJ+P1GJl/OJdOcxJ+Q4f1AuysrJOk/3JpqA9UtZwfuaL1a8NsWz8pbu9GmIcJKxVg85xcnGtm0roChrwek34uhVpw7/bILqwGbrbqmFQ/ib4zXznnjFFpaOync7OCDs6bGc3YHtrxXuVNzHwj1/QZ/vxmJ+enn4W+dLl0sgz0bjnD+hDFtfCFPS3qlND7733XjqGRfQalWPixIknlFFhWFxrB8qTVx6VJKz6KSyP2sKaFiPXCBbYBKBP1KwPv+sAP8/i2jFCVlwLx6mhavqPYHEtuKaj6VH40H65wWB4DR9CaS0nYUUxJ4J++xwb8FSkp+xPNgVsZvn7g255dfvbF9k2HuqRXw3RjgaIKiRRrWNxTYef7aYCdB0pck2aCkoRaySsbYVB+WvgCtvqfUmFFZOGLyzp1C6zONCcj22sDNsbTeMWtkVGyGHs03YhkzMzM+PQlRG9CYlo3PMH9Cc/tIYx6HOOXusEVA6vcimNT+utEfg5FteU5aTiUKiBZVHTd8RJ2w++r3aklCcssHlAvpB9IvKVEvg9PRe/o0b7jUhxjaeGdg0qj6icSqCEOClM9IUINx0JRPgQeidWzAfeFcW0yxFsxI1jx441R+pUM6Vt+saDceNeq37yioIN3/XN2wxx9kaIoimYBXWgR1yi1knwnr6pFJQn2thAmhbajPmsgUGWtXBDUdnXE15a+0j2kq195SL5ZbfffvvpeG86TCbTl4L2x7QD9mW0K/JryPhIXF9NZKJxzx/Ql2o+eLG4pjLoc1VFookTJtAxpKPXKP+ZmZknlE1hfBYl8bMh9QAXCDBPmoq+CiWwHGr28T75jT6DsMAWRMgHsi9EPlIKFte6RsD7pVAXiIJFOAiTwkRfYJPWYovFh9IxyHLkkKjSmOMxmUyHU1NTl2dkZJwru5EtQIb3pe6e5XV/uKJw/Z8HWDcdibPRdNA6aZ01vaMe8dghNKBCWkfCnMd1PdL0tmaIog0NbPXQ11oFCc+s+/q2NzYufnhVzblUFrlYPltOTo4+KyvrDuyUOaLUd45g37UNsV5//fW0O3K07M6IN8/xrjOgb1lcC2PQ52rvGkr4HIWlNSjf06ZNE5VJaVhc6wDKl1c+lSbk+yssg9qipM8+w88Ga3ooEdECG5Vd9oHIN0rC4lrXCWi/pKa4RoSDwKa2sEawuKZBo4dRk8l0AR4tCEV/CCuPOY7/ZGVlLRw7duxg2Y1sXbRnNx6Mm17Scr9h0fpPT81fe7i7oxqipA0MaiG6oAFfN4LORiKXLHSdNFLN/bkmSZCTwNdR+P1oWyNSD9FSejOeg3b6rMfz10KUo1YS8XSFddCtgKaY4rkkQQ8/h99zi3DS+aRztUCstR76WLfAxYVrv8l4sXLR71fVnEsimVwsf0yHnfJ12ClvwPb1s1d7Y9pnA/Zbw9FvA8iHLleykYnGPX9A37K4Fuag39We4kiE5PRQyrdXOdTALzESP8vimjqEdPQa5V0ug6hsShEq4hpB4hLlIaJENiqvXHaRT5SkU4ImfofFtWOEdOSam1AX2MJFkBQm+gLbcaYbMWLEIHxIvQ+hzQ6Elcgcw2w2/8dkMk2U13Zi66I96Gybbnp202dnzC//tWf+RoiybQdaQ03vaIQoWRjTSwJXB0hCWrMkfhFRkoiGR3wv1tII8ZYG6G6rg2j8O6pgB9KGrxsgzr4dYhzbIKqwFnRFRB3oCuh8JKy1gM7aCN3srjXdojAtxtEGcXi+/pbNcLG9/JuUFyoWj3+z6izsXDol8GB7GoLtaRG2rW+92xoj5O/YV92PPruKdkOW3cjmYaJxzx/QxyyuhTnod9UFo6ysLDqGVPQa5Rd/xJ5QFhXwS4jEz0equBYMsShkBTbMt9piJOGPuBYskcebTok+oQiVUy6vyA9K43fUGoHfY3HtGAHvm4MRhUWEqsAWDH8pEbVGCBN9ge0Ek9Zhw8q6wWAwvIQPrt94VyJzPPhg/xHeTDeir3gqWhcs45nSy29wrK4+O6/i1/7WTRBPIldBrSRuUXSZe/oliWVCQc0TR4MUnRZrbZCIttGUUlqrrQ5i8HWMFXHUQ2xhM0Tbm5BmiMXv0XtRNopeq5fWdpOmoWI6XbsbXpug1y7hDr+D1+iTtwmuKlz33S1vbn05+511Z8vF8dvGjBkzEDvkp7Ad/UPUzphjYN9Eu4H+CbkV6dXJKMGIMNG45w/obxbXwhz0ezCmhsLNN99Mx5AQ2CifOM6fUAYV8HsKLX4+IsU1gvLmlVc1CDmBjfIr51tUHqXw20/4+WBHr7khwSlso9ioXHL5gilmsrjWdRTpi4IRvUYoJRopRTCENSVFSGGiL7CJjYSipKSk87DiKIrmO8+KZE7gMDbsD6ZMmXIdP+T7b3gf6v5vXetNSS9V1Z5mqfi+r3UT9CBhzV4N0UUNoCtwT8EkQasFjk4JbQ+asml3CWjxllrojsTYajG9BnQUkUZHRy3ELmyE7vZa6G3bDqc/twN6FbVClG0H6Kw7INra7Jo6aiVI1GsBvbUVacN0fB/zEWOph175G+FSx+of71havewhZ8PZVBa5WH7ZPffc03vChAkPYSf5L2xPR7zaF+MF9k/PIsMTEhJ6oPt4GuhJzHO86wzobxbXIgD0fTCmO8Itt9xCR00LbJS/iRMnnpB3lfB7+ix+J5LFtWCIRkTICGyUTzm/onIoid/tBr8TzEgqEWElslE55PIE28d0/U75FL/H4trxBLx/DoZoRCgpHAWaYPlISQFSmOgLbCc1vTxNNBsrcKd3hTLHMJlMh5HFiYmJ/WXfsflgOXv2xN6zbOs9Sc9XHjy9YNMv3W1bIZbWWSuoAX1BrWtnUFlcozXP3OJaN0f9SaEINVpTLd5SD3HWBoiykUhH66jVgx6JsddBb9s2OMNa+avhnV3/uXHV3v/2k9Z024XXehe/2wox1iaXsCaJeq0QRaKbZSfE23ZCTzxn79wNcKm9/H9T3t5if2hty2DsTzor8uiwQ74C24/qnXII8oksrF3I00B9M+8xz1/Q5yyuRQDo+6BErxF33nknHTUpsFG+Jk+efEKeVYTFNT+h/HnlVy00L7BR/uR8ivKvJJ32DX5PK9FrnrhFtpAV2ijfcjlE5VObTkWtEfhdFteOR5H+OVjRa6EgsIWr+ChM9AW2Dk2HD7LxWIEX43EJwtNE2+d7k8n0IvlL9h3bSSzbuSd28stVd11fVPb/Bllo8wIS1EgAa4BuhY3SkYQ1XUETSJsKSBsKtEA3R/vimuvzSCGtr9Ysi2Ot+P1W0BcijgaItddB39xNcOFTxYcnvLyx6ealDXddv3jLh/0t2yDa0Ybf3yGtzRZja4QoK0XBNWG+2jCtDXoV7oZeeTVwak4FDM1b/nXmKxvHztz8fk+5SJ2y1NTUs7HtbEV4p96TgPfVfjyOJwGbI0R9N9G45w/ocxbXIgT0f1Ci14ibbrqJjpoR2CgvyIAxY8ackFcV6dSuqvgdFtfE+VYDzQpslC85f6J8K02n2wx+V2vRa964hTbNi2yURzmvhFZ8SvnotO/wuyyuHY8i/XOwBCSCrkvinhZFtmD6Relps8JEX2Dz2aTNDrAyM/Ah92PksHclMxI/4432HG9wcHKbvWb3Gbe+0/jsUFvlXwbmbjzS014DMXaaslkP3QqbJEGNNjHQk1Bmb4RubnFN4uTTQumz3QpoXbRWZAem7cLz7ARaq613USP0mVcJ585Z9kva0284cta0Dp38/Jq7E6yr/9fPsgWi6ZqFja411/Jpp9J6iJLy0wjR+LqXZTOcOb8M0hev/unWN9dnGHNqurTOXnZ2dt+srKzNeD/9KmhLjAtaX+0Vg8FwDU8D9d9E454/oP9ZXIsQ0P9Bi14j8IciHUngC6rIRtdHguYHmU4JawR+L9LFtWCKSITmBDbKj5wvUX6Vpsv+oO8jWhbYCM2KbJQnOW9a82GXhDUCv8/i2vEo1v8EU0gi6NpaEtiC6Q81fCFM9AU2v4yi2KKRRHzQ/ZOoshlpB9FfJk+eXIj+4ugagd3zemPvca9sfv4Sy9pfes/feqSHnTYTQEjUomg1Wl8N/3bjmgbqosP11mRcmxDQFNIdeL6doHe0QJy1BgbkroeLnlgOGXZnUcK0JTGTF5Z0v2p+yYPnzK+A7vm0zhtdnzYxoA0PGkBvpQ0O6iC2oAZ6WzfCkHkrIGNR2aYnKhrG5uRAl+qXoq8mTJhQQVOKRe2IORqt9gQeL+Rotc6ZaNzzB/Q/i2sRBNZB0KLX3PzuvvvoGBSBja6LBFtYI/yeDuoGvxvR4hqBedSCwEb1EFShha4v5yOYvgiUMEHikOj8WsMtsrlRvQ3QNb3yoFVhstPTQd3gOdT4jRJK4hqhmMAWrOmhbkhU0kIUW7CFRqWj1ghhoi+w+W+01hE+7A7Eyi3C4xd45AXYvcAb7vCkSZMK0F0sCHjYPcsaL5n46tb/njmv9HCvvCqIK2iCqIJmhDYsqAOdrV6KMJM2FLC2SOudUbRaNwdBgplYTPNEb2vA79ZDlBX/tuE58Psxtmrok7sWritwwm0vVCzOznZGOZ0QdcuSzaMSCtf9t59lG36GrkWCHK2xhueRNjNokDZFGGCtgstt5XDb21s2GXPeogGrS9FTdA9NmTJlqclk4ntHzM8IrfN4uxwFytFqnTTvMc9fsA7U+jFIsLgWZLAONCEuZWVl0VG1KDa6Dl1Pvm6w6XTUGoHfVfqeDYn7lPLple9gEDSRja4pX1+UL7UI2EM+nQfRqkh0MrzFNjeB9Iv3uUPBT5THLvsAz6HGfR5q4hqhSN8TbHHNjRqRWyLomuSDYAprBItrYWokEJhMJrp5DyIsEniBvvl5woQJabzwerdu05bsikksKr3h8lznN6fNXwd9bFsh2rod9NJU0DqENi1wRapFWZsgxtosQSIb7RLqEtjcAporukyC1mijqaOy8BZFO3za6iHWWgvR+NkoRwt0tzbAKU+vgattJX96qGT7wpycHGkq5z2vr+mduWD1s+fmr4fuFBVX+C5eYzeeZydetxXi7C3Qx1oPg3PWgXHhpv/OLG1zPuDc00sqUBfsjjvu6HnbbbdVsrAmxuiaIrspIyPjdI5W67qJxj1/wLpQ88cgi2saAOtBK9FbcMftt9NRMZGNzkvnD/Laap50SVgj8PtK37OhIq5pQVxyo8iDrgi6hnytYJedrh/Q8uL5SDgSXSsUaU9085dQFRwDJS6q8RslFMU1N+6+pzOcUEfBjtjyRG2BTStlV6vcwkRfYOuy0VTRC5FZWOH/RHiKmwepqamH77zzzqLs7OxY2V8RZdOWLIm55+1tJuOCsh0X2dZAn/wqiLfUQIytVppuqbeTSOZeV8017VOKGvPCMzLNJcbRTqJu8DyYRjuCxlobId6KRzx//MIW6LOgGQY/teFIqmPdF5OfW3seZuloBNSY59ZdmlS45tDAvE0QY8fvPPMexBW9DzHWNuhhaYB+uVvggvmrf7ntjep/TC9uGJ4DXZsGSnbPPff0njhx4iaz2czC2okcNhgMtGHKg9ifnIvu4mi1AJho3PMHrA/6gSWqLyVgcU0jYF0EfXqoJ9hv0pHy1GWhjb5P55k7Zy79SD3hWkGm09NB3eA5lL5nQ+Y+pbx65T3YKCay0TnlcwdbVHMT8HaC5wzV6DXmeLo8HdQNnkuNezyUxbWuICy31gQ2iuIilBCc6Jzu80eSsEYIE32BLTCWkJAQk5qaegZW/GbkB4TFAxn0y/c3T56ck5mZ2Ud2V1hbttMZle2s6TXihbJzRj9X9ty1BWt+OG3+Wug9f7MUUSZtEoCQsOYW16RNCI5GponFNfdnXCJcI56jTpruGWsjsa4Oom1NeGzDvylarRoG5FbBJfmV3095o7Htjlc3n4c3/FGxZvaaxt5Jz6x9ddBTZdCDdgm11EKMtQ6659fAAPzuqfMqfhxWtPYvd725pfDht2r6yV/riukmTJgwSF5jjTcvOB7qK743Go3b8Hg79SWyz9gCYKJxzx+wTtT8McjimkbAutBM9Jo3+MMSZs6YSa/dYpsnR4U3eu313oBpv5vmnnKqRboctUbgOZS+Z0NJXNNS9JonntEknRba6Lse59FSOSkvARcQCTovwgJb6BKwqDUCz6V0f0cESlzTan/UHu2WW0sCmxtPoa2zYpunmEZorYyUJ1G+lUCY6AtsATWKYuuHlf8MHt/H43eeDSKSwZvzv2PGjMnDH/Wnyb4KS3to5bbBU17dYDa9WGm99pmKbwfllEGf/C0Qm1sD8bZWadqmS1Bzc0xYk8Q1imI7+t7xdJMgoa0F9LY2acpoLEWpWeshxkY0QpytBXrZGuC0/MqfEhds+OO017c98/jKlsHYQxwV1mgjgvve2vKHCx2VR7pbt0G0ow7i7DXQ17oJzsqvOHSlpfT/pSxa89rkl9ZcQkKh/LVOG01tnDxu8tCxo8euM5lMv4jaRwTzX2QHUpSYmNgf3cXRagE20bjnD1g3avxwdcPimobA+tCswNYe6enpMGnSJJhw002QYtZcVNrJCIiwRuB5lL5nQ+o+xfxq/YHWU2jzF62WS9E2gudngS00CaiwRuD56D4QXSuQBKw907m8zq1lTlpuEnoE39EM3mKbL2hNTPOE8qZW1BohTPQFtoCbjiJPTCbTVUaj8QXkU2wQHMWGoE/+jTeuIykp6Szyk8tdoW94H+mGLyzpfr1l+dVjX9zw7PCiir+daV17uK/dFREWW7ADohyt4Nq9swlcghoJZXT0Fte8RTWaAuoW1mj6aIMczUYbFbRCtKMNaZUi13rkb4VBeZuODC3c9G3i4g0l9y+tuenRkubucjaP2pPlrWZDYcUn/fOqoIe9Fro7tkE/61q4tLDse+MzpVvufH3dnTklzafIH++S0Xp7+LA3MmtUVg0La8dBfcKXeC9Y0C/nYD9B6+CxsKaAeY95/oL1pOYPwZB6aI8EsE4o4ktUV0xg6fJ0UDd4LqXv2ZC7TzHPoRYxEsqQnxWJWvOEroGwwBY6BFxYI/CcLK4px0nLrcXotXBFbWGNECb6Aptiph8xYsQgfGj+LVKLDYMiVIQNJpIwmUx/RxyjR4/+zdSpU+NlX4Wk4f2je3T9e0PuXrb9ZsOz6568yF5RfY5l7Q/9cjdBfH4dxFhJUGsDvaMNuhU0g66wSRLIXEKaW1g7Hreo5hLTjuEptuml9dbqQV/UAlFFOyC2oAX6W7fCOflrvkywltfd8kqV7feras6l/MlZPWqPVX7Y/7bXtuw+N38dDMzfDIPzKuF82+r/XfvM6h3Zb2x4bmZ57fWBWFuNjASjpKQkM1qtKTn5kKg9RCLoF9pheL3BYJiDrwMx5ZbtJOY53nUGrCsW1yIYrJOQi14LQQIWtUbguVhcE4D5ZoFNeVQR1tzQtRAW2LSPIsIagedlcU05OryfWWBTB4qqE/lfSYSJvsCmrOHDcy98iB6Bx9nYOGj6F4sMycn/wM5o7fjx47PQLyEnsDmdzqjflzQPuenVTTebntvw9vWLN/z5/MKN0K9gG8Q76iHG3gzRtp0QY90F0dY2iLI2gzTds9AVfXY0Sk3anOD4aaAuIY3SBMIbRa3Z6yDKUQMxRTUQW1gDPezb4DTbxkPXP7Pxi4znNjgmvVB5wx1LN/eUs3qCTVhYPuWa+SU/nTGvHC63rP5lZGFFa+ozpU/ct3K72bZ17wD5Y122cePGnZGRkfGgyWRqxjr+SdAGIg70A62r9j5yP3JpZmZmnOwuNgVNNO75A9Ydi2sRDtYLC2zKEVBhjcDzsbjWDpR3r7IwgUNVYc0NXRNhgU27KCasEXhuFteUpcOys8CmLMGIWiOEib7AporpExISephMpixsJC/hg/VnSEQv6o43ypG0tLRmZNSYMWN6yH7SvD1RXnf63W9tvvfGBeuXXWxf/8mZ9o1HelmqINZeDdEFDRBV0AJ6RyvobM0QZWuRNhmIlqZxkmAmC2oyro0KjhfXJGGN0jw+d+zzeH4kzlYLva1VMDB39a+XOFZ/NnLRuiWTX9twy5zy1jPlbJ5gtO7ZH54vGTdizgu7rnnq7S8zF6/9c2pBydz7lm1LEk0d7YLRuoPnYv3aEYpS5CnRrh2Em9EXuQaDYdSwYcNoYw+eAqqSicY9f8C6Y3GNoXbAAlvgCbiwRuA5WVxrB8w7R68pR9DaBV6bBTbtErCdQUXg+VlcUxafyk6RVYLvMl0kWMIaIUz0BTb1jCJVUlNTz8YH7AlICzaaSI9i+9VkMrVOnjw554477hgku0mzNn3l9unpL6wvv6pw3Ren52841MtaDfH2BmkjgVgbbSyAr631oLfVgc5O0zfxtYOizZowrRmhaaL0ulHa2MC1uQG9T7uG4ueRoyLbccJaA0TjOWmn0e6WajgldzNcaqk8nPJc1U7zsxVj73xn69k5NTW0Zle7VrR0c89HXyl+L/uZZQcMuW9MuH/Z5vRpS9YFWtTUTZo0KQkHmOVYt/9EIl5YMxqNXyNrkDHIQFqDTvYVm0omGvf8AeuRxTVGAuuHBbbAoYiwRuB5WVw7CZh/FtgCT1Ci1jyh6yMssGkLRaPWCDw/i2vK4lPZOXot8ARTWCOEib7AprrpSGRLTEy8ARvOXKQJidiF3o1G46+pqalf4g30MvqFIq80FdFDUV2/W1Y9Ov2FDVVXLKz621mOjb/0sWyBOGsNRNkbkWZp184YaxPSgK9d4ppeWhuNxLVG0Be4Nh+Isu+AaAt9nj7rimiLstVjeh1SC3p7DYJH2sQA06ILmyDKQeJdLfTK30I7gMIFuasPJdgr1t75xnbrb5fVXduRqOa2d1aVjpnxanHGA2+suSEQO4B6G23igfWXjkYbF3wvqusI4wi27S+Q+chvKHIV3cTRakEw0bjnD1iXLK4xR8E6YoGt6ygmrBF4bhbXOgDLwAJb4Ai6sOaG8oGwwBZ8qA7ORBRvF3iNUBPXQq3v8bnsLLAFjmALa4Qw0RfYgmM0TQ8fuvuZXLuK3on8Q9S4Iohv09LS2pAJ6AtNrMN2z5J1k1IXlFdfnlf6+Tm2SuiRvwViLdshykpCmGd0mRyZZpenhDqaPd6j6LQmKUotGo8uQY1wRbDpbA2SABdVgH9TlJujDqILaN22Woi3VkOv+Rvh9PwN8Bvrapjwyqb3bnpp49SMRRXn2ra2+bU+2tKlSwfh/a6IuDN27NjB2AE+jR3hfqzHn73qNRL5CtvwEoPBkJSYmHgquiggG0Swdc68xzx/wfpkcY05DqwnFtg6j6LCGoHnZ3HNB7AcLLB1Hc0Ia24oPwgLbMFD8Wg1T/BaISWuEXQ+r/NrGb/KToIQTRFlka3zaEFYI4SJvsAWdNPTmmP4MJ5pMplexGPE7iqK5adonz/h8babbrqJxCPVI30oqmtCUfmgVFvJO9fmlf75fEvlr/3nV0EPilSz1kK0JICRsCZP4Ty6PhptWrADuhXsxGMrdLO3YBqJaq5othhrHcRSRJtblCtoQpqlXUSlyLaCVjx3M8Th+Xtat0P/vA1wgWUdXGcr+2/q4g3Pjnt544TpbzdeEOD10bpqOprmbDab87HOqN1G9DRQbLu0tto+PE5GzuQpoNow0bjnD1inLK4xJ4B1xQKb/ygurBF4DRbXfATLwgJb59GcsOaG8oWwwKY+qgprBF4vFMW1UOp3OnWfcxRb59CKsEYIE32BTRtG0+rwgXwgNiwjssizoUUYJLD9x2w214wYMYLWYVNNYMtf+/55t7xSaUuYX3zg/Plrfur39KYjPS2NEEMbExS0SGKavsC1o2c3e53rSAKbgwS2JtBLAhtFr5Gw1oq0SBFqMdZGiLXW4Xlomih+jjY9KMTPFtJr93pqNdDTUg0D8jbCJfnlMO6FSrj5tS0Lp7yy4brxb9X0y3Y6Y+VsasX0d9555/wUc8p7WGffetVhRILt9kWTyXQBHinykqeAasRE454/YN2yuMYIwfpigc13VBHWCLwOi2t+gOVhgc1/NCusuaH8ITQ1kUU2dVBdWCPwmiEnrhF0Tq9raJlOlZ8FNv/QkrBGCBN9gU1bRtEuiYmJ/bGRzcOH9L3eDS9SwLL/ijfZl6mpqf+HPlFUWMoB0N/5RrUxuai05YL5ZT8Mzqs60iO/DuIcOyHKTpFoO6EbRaM5GqFbQR3oChokPMU1ikiLstVClJXWS2sEmh6qd+zA9B2utdZstOZaC8QswL8dzRCL34+zbYdelo0wIG8tDMlxwuWW0q+SF63fPHnJljtvW779nDuWbu5JeZOzqRkbPnx4d5PJtMBsNv+AdUXRWsI6jBSwra5DrktPTz8F3cOimsZMNO75A9Yxi2tMu2CdscB2csg3AxBVhDUCr8Ximp9gmUiIIb+xyHZyyD/kJ00La55QXhEW2JSDfKvK+moi8Lpq/EZRQlwLJVG/0+Vnga1jyD80lVZLwhohTPQFNm0aRb8YDIaLsNHNQf7s2QgjCfTDD2az+Y9I0rRp02Jk9wTM5pS3nplRVGG/PL/iLwPnrz/c114HsRRx5tgNOscupE1aT40i0yRxTRLUvMQ1ewNEUfSZvQZirDWgs5Hg1oqfoXO8C9EF70OMfTdEW1qgO77Xy7IdTrFshCG55XCFtfiXtGdXfzb2lcppIwtWnjHu9TW95U0KtCbS0EYcfbA+MrFevkAiem019MP/8FiXlJQ0LCEhoS/6h9dV06iJxj1/wHpmcY05KVhvJLCRgMQi2/GoFq3mCV6TxbVOgmXjKLb20Xy0WntQvhGOYgs8QYlW8wSvH5LiGkHn9bqOVulS+Uk04nXYxGgtWs0TYaIvsGnadCSymc3modgAl+Br2oUxEte1OmIymb7Hm6/0lltuuYD84nJP540ErD8srflN2oI15Rflrj3UP3/7ke6086e9DfTSumk7QW9vlTchoN08SUwjcc2NZ+RaPX62DvTWaoiy1UibE0Q52iC6YAfE4/nic+ug+9NbofcT62HI0+VwSV7JoeRFa/8z8dWNG+94s2rknPKaM5XYwTOApsM2OATroArr4ieqD4+6iTgMBsNmZFRqaiqLaiFgonHPH7DOWVxjfALrj6PYjhEUYY3A67K41gWwfBzFdiIhK6x5QmVAWGTrOkGNVvME8xDK4lqoiPkBKT9HsR2PloU1QpjoC2whYTqaiocNcRLykWfDjCSMRuOvJpPp4/T09Lu6MlU0J6cm+oFl22/NWLzuL2fnrD3SN68GYvMbQW9pkqZuRjkQexPE0GYECG1KoEek6Z60w6e0gcHx0E6fUQX1EG2vw+/UQpylFuJzqmDgvPVw9hPFcE1+KUxasgWMeUsfve+NjRNITMvJoSmfyuzgGSDT0VTHtLQ0K/r9G6yDSN+wgHzwfFJS0lnoGxbVQsRE454/YJ2zuMb4DNZhpEexUblVnQbqDV6bxbUAgOXkKDZX+ak9hbyw5gmVB2GRzX80I6q5wbyErLhG4LlDoZ8JmLjOUWwutC6sEcJEX2ALHcOH+2hsjAPwmGswGP6JjTMixQ6TyXQkNTV1c0ZGRjLttCq7xyejiLW731g/wbSg+J+nP10KfSx1EGdpgVhbK8Q4miHaVg8xFpreWYeQqNYMOhttUEDQ9NDmowKbXpoOSoIbHu34eXs1xFuqoE/eejjTug6uXbgRprxT+/lD71SVzF6x5SKja7pnKJjuzjvvPBs7vRno63+gzyNaVEP+jvfcG8hvaOMR2UdsIWKicc8fsP5ZXGP8BusykkQ2t6AWVFHNDeaBxbUAgWWlB1/yZ6SJbGEpqnlD5UNYZOsYzYlqbjBPIS2uEXj+UBDYAuqDSBTZqKxUZkLrwhohTPQFtpAzmioaPWLEiHNMJtOL+PpzbLA/IpEogHyN2BITEy8eOXJkb9oMQvaR0LKdEHXzm1syEheu+feg3LXQO3879MxvgLj8Roi2IQW0e6dLLIu2NUGsvRX09jbQ2XaAzurakIAi2qLyKDKtBnpat0Nvy1bob91y5HT7psMXF67/4RJbybuZL1W+alyw7tKxi9cOfrb1YB+8dCgsck9rqsWNGzfuEsSWmpr6R/Ttrx6+jjQO4731F8SB7etKPNIOoGwhaKJxzx+wLbC4xnQarNNwF9moXEEX1DzB/LC4FmCwzJEiskWEqOYNlRdhke143IKaJkU1N5i3kBfXCLyG1gU2RXxAIlMkCGyhEKnmjTDRF9hC1nQURSOvxzbHYDC8j8cvERLahA07jPk8KyurChlvNBrPRE6MEAPQTX27OiF5UeVf+81bC/H2BohxtLoi1uSdPClKTW9rgWg8xlqbISaPhLcm6I6v42342rod+ti2wsC89b/0f2zVvy54etU/RxZWHJz02tZd95c0b3tk/a7bZ6/Z11u+YkhYTk6OPi0trafJZLokIyNjfmpqKm2eEamiGgnUdP/8Hf3RiO3oVllU4x1AQ9hE454/YHtgcY3pMli34bYemztaTVPCGoF5YnFNIbDs4SyyUZkiSlTzhsqPuAWlSBbaqOwh0RYwn2EhrhF4HS0LbIr5IJyj2NzRaqEmrBHCRF9gC30jkS0pKek8bMT3Go3G1/H4JR4PezbuSMBsNh9GKk0m082TJ082ZmRknC5Hs+meKNk2ZOzi8vozni6H3vZ6iCvcATGFbRBjb4Y4ayPEWxqhe34D9Myvg7551XBq3lYYYtkKZ+VvgvOtVXCWZf2/LipY98/z8ksahxeuLk60ldx5x8vrb59TUnuFqxZCynTklzFjxpydlZU1Gn03E/22CxH6NUL4DtmL980beLwF29AF+DpUpvGyncRE454/YHtQ68dexD/YhTtYv+4oNjehKLZpVlRzg3ljcU1h0AfhIrJR/qkcBPe/HpA/kEgS2kIiUs0byiui5H2o6m8Tuhaixb5F8X7fLbKFg9AWyqKaG2GiL7CFldGU0V7IbGQN8lds4Ie8G3wE8Ou4ceP+azabaYfVW2+dOnXSA4tef2H4E68cOS+3AgZZNsMA6xYYaKmCQfkbDg/OXf/9kPnrvj53/tqvr3Bs/Ppy6/rWy+etqrg+v7h8RP7K8okvbSyf8Oq2Rx5Zu3v6Q2tbBmt8Z8+TmbRBwZQpU8ahX+7Ajm8p/k3tI+KEWDfoh1+ROoPB8AweM2njEPKTy11s4WCicc9fsK2E1Y9XRhtgnYfClFG3mOZGs6KaG8yjkg9nfK96QL5A3OKU1h6G24Py6c4z16UPkJ+QcBPaPMW0kBLUvKG8I2HV39F1ES31K6r+UyXUhDa3mOYmlEU1N8JEX2ALO5Oiksxm8xCDwfCA0WhcgXwmuhHCHZPJROLJ12PH3/Tfifc98GvKHx6D5Cde/GZYQcXGa2ylFQnW0tUjHKXFCfkrbFc99c5so2XF7Ntf3z572orGEbcuWTfwtpcq+9+/oqG/vfLD/rJvQ9FIcI3PysrC5mC4GdvFwtGjR/8V02jnS6HfIogt6Idi9Ms1w4YNC5W18dj8NNG41xmwvXg+RAYafsCLYLD+PSPatCK0aT5CrSMw70rcs3yvtgP5xstXhFYejFlQCxDkP8RTlCJCQXALGzFNBJUH8bz3AkHQfUR5kPMS7L4kaBHLnkIboQWxLRzFNG+Eib7AFrYmrckmT2+7EyGR7S+iGyQSMJnNkDYqC0Zl3/Z11pTfLh/9298/PGH6k5mPvrb2wqLNm3vKPgsX09MuqmnJyTekp6ffhm1gIXaCreiHz/F1pO/8+S3eB414zDObzdfS/UHrzsl+YwtDE417DKNVsG/ynjrqRknRzTs6jQhZUY3RDtiO1BbcPEU0T1hQUxDyL+IpXHmitvDmLaK54TYQwlD9IXQvB6P/IDTTfrzFNkJpwS0SxDRvhIm+wBb2Ju0CKYtsUxAL3iT1ePze+8aJFNAX3yWbTB+iDyqxs3h1dObo+7PS00feddddF0yePDnkpgWSOHT33XefOn78+CvGoWGn58ByvmlKTm7AslLUYiTv+kn8YjAYvqD6xuND/7+9uw2R66rjOO7GmNaaNm3a2mq1aJqEdtRIXIPD5M45996Z2a6YzYNRhEQbH8AGk9g3LagvsgqiFEHE4hvRKsW+6YNFaG1FqEirotSkoWjqC6nUKLEPaY1oElPU33/3bl1m/yVn081mc+f7gw/nztlsdubcs3vgz7n36vV6Lv8cnHjrHnCu0d+tVyq6zQUKaZg3mm9nYofNFAooC4ydE/GKXWcKc6DG7PyK97s/F87ZueMV3DxTRbj+YtmpDEIxrZ/bmYIMTCZ2sjWbzeUxxmYI4TP65bKbt/9Zrwf1nlu2i+uloigOlUX5mP54/LQsy+9u3br1Nv3ReVf1QIQlVrzS78qCKMTYezE6Z0uzLFuxadOm68fGxr7V7Xbv1nt+VO//oNjN+Qe9oGaXBJ+QJ+VWjdUGte/UObZdihTVBij9ax4AAAAwaKaKcINYLJsttzMFGbzYPdmsyFAUhW1/3SS3xQHeyTaNFRmPdTodG4v9an9VlqXd6P5LIyMj43v27PmYjVuj0VhSDeUZz9S50ntYunl09JputzseQrBLGm/Xe/yNzuEfdGzv1x5MMOiXfE73e/mKxqqn8blC42dP/qSoNoDx1j0AAAAA8LidKchAxx5+sKTVar05xpjleb5Lrd2PiiKNFEVh7VE5ouMjvV7Pnr66v9ft7tuyefM9nU7nkxove+rmRzZu3Lhlx44dm3fu3NnUmC5T//mpdu/e/fZdO3ddv3379i36GXbp7kfNtm3bHhwbG3tMP3u//VyxgtEReUEohs70T43bw7InhLA2y7LLbbdmNdfJgMZb9wAAAADA43amIEQZsksNbYdUCGFVWZZjMcbHc4psLiu6aYxO6PhZeUb+pr7D3W73sPqf0tgdkH2p9D0H9b2HOp3OYfu/qv/zGfWd1Nfc94D/0xg+J3YfwQ/b/LV5rDnNQwrIRLx1DwAAAAA8bmcKQvoyZJc9rl+//uo4+QCEL+Z5/oJaKya5xQ3MYEVJu8Q0FUXM2TspzxdF8Y0QQqH5eaWcb/N3choTMhlv3QMAAAAAj9uZgpBXyMQDEFqt1oWdTmd1u93eEWO8V/7VV+QA5pXm4G/V7rICsOboMrsvXTVnCZkRb90DAAAAAI/bmYKQhAzFGBdbISPP8w/o+DvyJx2z4wrz5VnNuR+UZdmYdi81dqmRU8Zb9wAAAADA43amIGSWee3KlSvPs11D7Xa7FWP8YZ7nT4ldpucVRYDTorl1SPYVRfFBtSs3bNhwgeYfBTUyq3jrHgAAAAB43M4UhLyKLLJCW5ZlK2KMG0IIj+R5fqy/SAKk0Bz6jxzX8S/kJh1fu2bNGns4AZd9ktOOt+4BAAAAgMftTEHIHGXIdhZZkU2+LM/lk7vZ7NJRLh+Fx4pp9kCHk2rtMuO90rF5xH3UyFzFW/cAAAAAwON2piBkrjM+Pr6o2WwuL8vyqhjjnSGEn1mxTV6siileoQU1V517K7ieqObDPh1/Qe1l3W53mc2bagoRMmfx1j0AAAAA8LidKQg5gxmyHUgjIyPL7YmjIYQtMcaf55P3aDsq7GirP9uddkLt02p/rTlwn46/rfa6drv9Jgpq5EzHW/cAAAAAwON2piBkvmKFthjjW9rt9kgI4atyv14fyCeLbUeEXW31cETn1S7z3K9z/BO5Q8bUd+3o6Oh5ahdrOvBgAjIv8dY9AAAAAPC4nSkIOQsZajQaS5rN5uuzLItFUWzL83xvjPH7cqcckEPqe74q1mBhs51pf9U5+538SMd7dU5v0LFOb3aJFdTsnE+eekLmN966BwAAAAAetzMFIQsgQ1aAabVaF6q9KITw/jzPPxVj/Jza29U+KS/q+B/C7razTOfiuPxdx3fL9+SbcmNRFBu73e7V04ppFNTIWY+37gEAAACAx+1MQcgCjBVlFlVPH71SNlvxRu3Nau+QB3VsO9vc4g/mnsb7L3Kvju8KIXxN7Y1qV6nPzs9lwqWeZEHGW/cAAAAAwON2piBkocfu1WaGh4cvyLJsRQhhbZ7nW4ui+HSM8VZ5SB6Vw8LOtlfHnuZ5sBrPB+Tmapw/pHG/TuO/utfrvdHOh04NxTSy4OOtewAAAADgcTtTEHIOZijGuHh4ePh1WZZdXhTFOzqdjhXcNqn/43r9CbW36PUTViiSp8UrJA0sjc9Lam1szMN6/XkbN7mh3W6XVsDU2DZ6vd4bbJxtvKuxJ+ScirfuAQAAAIDH7UxBSF1iRSC735fR8TIrEBVFMVw9nfSzxopuco/YEy2N3cvt31ZwqiP7fNM+q13WeZONg9pdatfZ+FgRTV+7eGrsql1phNQi3roHAAAAAB63MwUhNc7ETfWrnVdLzejo6EVq35ZlmRWVhvM818t4Swjh60av7Qb9R8UtVi1gx/Q5HlI78TmmfZ7O1Ge1zy0T42A70qbGp0JILeOtewAAAADgcTtTEDKAebmoZLu0qoLTxaYsyyuyLFsTQljbr91uv0dfH8vz/Mf99L32RNNfyuNyXH1eAexUntD3/lHtIzLjZxi9h/v0b97b/96Koni3vvZWvfdL9PWJz1KZetAARTQykPHWPQAAAADwuJ0pCCEzYkWoRZ5Go7Gk2Wwun67T6VyaZdlqK24VRXGNjt+n49ZshRBW6XsbZVletW7dukv7f46x4pn3vioUzwjpi7fuAQAAAIDH7UxBCJmTvLw7bHx8fNHpmP5/CCFkDuKtewAAAADgcTtTEEIIIXWNt+4BAAAAgMftTEEIIYTUNd66BwAAAAAetzMFIYQQUtd46x4AAAAAeNzOFIQQQkhd4617AAAAAOBxO1MQQgghdY237gEAAACAx+1MQQghhNQ13roHAAAAAB63MwUhhBBS13jrHgAAAAB43M4UhBBCSF3jrXsAAAAA4HE7UxBCCCF1jbfuAQAAAIDH7UxBCCGE1DXeugcAAAAAHrczBSGEEFLXeOseAAAAAHjczhSEEEJIXeOtewAAAADgcTtTEEIIIXWNt+4BAAAAgMftTEEIIYTUNd66BwAAAAAetzMFIYQQUtd46x4AAAAAzPTf1/wPhmNg14hbmaUAAAAASUVORK5CYII=',
             	    width: 205, height: 40, style: 'header'},
            		{ text:'Users Summary Report', style: 'h1',lineHeight: 1},
            		{ text:'Users during: '+dateform+'\n\n', style: 'tc'},
            		{
            	    table: {
                    // headers are automatically repeated if the table spans over multiple pages
                    // you can declare how many rows should be treated as headers
                    headerRows: 1,
                    widths: [ 50, 125, 100, 150, 50],
    
                    body: [
                      [ 'User #', 'Fullname', 'Username', 'Email Address','Reports Sent:'],
                      
                      <?
                        $pt2 = mysqli_query($con,$ActiveUSQL);
                              while ($row = mysqli_fetch_array($pt2)){            
                      ?>
                      
                      [ {text:'<? echo $row['id']; ?>'}, {text:'<? echo $row['Firstname'].' '.$row['Lastname']; ?>'}, {text:'<? echo $row['username']; ?>'},{text:'<? echo $row['email']; ?>'},{text:'<? echo  $row['cnt']; ?>'}],
                      
                      <? } ?>
                    ]
                  }
            		},
            		
            		
            	],
            	styles: {
                 header: {
                   fontSize: 16,
                   alignment: 'center'
                 },
                 h1: {
                   fontSize: 22,
                   bold: true,
                   alignment: 'center'
                 },
                 tc: {
                   alignment: 'center'
                 },
               }
         };
          pdfMake.createPdf(docDefinition).download('<?echo 'ForsiteReport_'.$startDateVal.'_'.$endDateVal?>.pdf');
    }

    
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
                '<td><b>Total reports sent:</b></td>'+
                '<td><b><a href=\"Rflood?f='+d.IDNumber+'#table1\">'+d.Total_Rep+' <font color="red">'+d.Total_Rep_Brgy+'</font></a></b></td>'+
            '</tr>'+
            '<tr>'+
                '<td>Valid reports sent:</td>'+
                '<td>'+d.Valid_Rep+' <font color="red">'+d.Valid_Rep_Brgy+'</font></td>'+
                '<td>Invalid reports sent:</td>'+
                '<td>'+d.Invalid_Rep+' <font color="red">'+d.Invalid_Rep_Brgy+'</font></td>'+
                '<td>Unconfirmed reports sent:</td>'+
                '<td>'+d.Unconfirm_Rep+' <font color="red">'+d.Unconfirm_Rep_Brgy+'</font></td>'+
            '</tr>'+
        '</table>';
    }
    
    $(document).ready(function() {
        var table = $('#userstable').DataTable({
        responsive: true,
        "data": <? echo $usersdata; ?>,
        "columns": [
            { "data": "IDNumber" },
            { "data": "Firstname" },
            { "data": "Lastname" },
            { "data": "Username" },
            { "data": "Email" },
            { "data": "Created_at" },
            { "data": "Account_Status" },
            { "data": "Last_Login" },
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            }
        ],
        "order": [[1, 'asc']]
            
        });
        var table2 = $('#alluserstable').DataTable({
        responsive: true,
        "oSearch": {"sSearch": "<? echo $u?>"},
        "data": <? echo $alldata; ?>,
        "columns": [
            { "data": "IDNumber" },
            { "data": "Firstname" },
            { "data": "Lastname" },
            { "data": "Username" },
            { "data": "Email" },
            { "data": "Created_at" },
            { "data": "Account_Status" },
            { "data": "Last_Login" },
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            }
        ],
        "order": [[1, 'asc']]
        
            
        });
        
        $('#userstable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
    
    $('#alluserstable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if (e.target.hash == '#tab2') {
                table2.columns.adjust().draw()
        }
        })
        
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");

    });
    
    
    });
    

    
    
    
    
</script>
    <?php }
    else{
        header("Location: login.php");
    }
    
    ?>
							</body>
						</html>