<?php
session_start();
include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
date_default_timezone_set('Asia/Manila');
$time = date('Y-m-d H:i:s');

if(isset($_SESSION['connect'])){
    unset($_SESSION['connect']);
	
	   $adminuser = $_SESSION['usr'];
       $FloodID = mysqli_real_escape_string($con,$_GET['fid']);
       $Choice = mysqli_real_escape_string($con,$_GET['choice']);
       $Remarks = mysqli_real_escape_string($con,$_GET['remarks']);
	   
        $searchsql = "SELECT * FROM `flood_reports` WHERE `Dup` =  '$FloodID'";
        $result = mysqli_query($con,$searchsql);
        $numRows = mysqli_num_rows($result);
 
        if($numRows  >= 1){
            $upsql = "UPDATE flood_reports SET `Status` = 'V-D' WHERE `Dup` = '$FloodID'";
            if(mysqli_query($con,$upsql)){
                
                $auditsql = "INSERT INTO `audit_logs` (`audituser`,`audittime`,`auditdetails`,`auditremarks`)
                VALUES ('$adminuser','$time','Dismissed report #$FloodID ($Choice)','$Remarks')";
                mysqli_query($con,$auditsql);
                
                $_SESSION['disres'] = "success";
                header("location:floodreports");
            }else{
                $_SESSION['disres'] = "failed";
                header("location:floodreports");
            }
        }else{
            $_SESSION['disres'] = "notfound";
            header("location:floodreports");
        }
        

}else{
     header("location:javascript://history.go(-1)");
}


?>