<?php
require_once 'DatabaseConfig.php';
 $conn = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
date_default_timezone_set('Asia/Manila');
$startDate = date('Y-m-d h:i:s', strtotime('06/01/2018 00:00:01'));
$endDate = date('Y-m-d h:i:s', strtotime('now'));

$startDateVal = date('Y-m-d', strtotime('07/01/2018'));
$endDateVal = date('Y-m-d', strtotime('now'));
//When the user clicked the show button
if(isset($_POST['Show']))
{
    $emptyStartDate = empty($_POST['startDate']);
    $emptyEndDate = empty($_POST['endDate']);
    
    if($emptyStartDate || $emptyEndDate)
    {
        $error = "Input both start date and end date";
    }
    else
    {
    	//converting startDate and endDate to DATE
        $startDate = date('Y-m-d 00:00:01', strtotime($_POST['startDate'])); 
        $endDate = date('Y-m-d 23:59:59', strtotime($_POST['endDate']));
        
        $startDateVal = date('Y-m-d', strtotime($_POST['startDate']));
        $endDateVal = date('Y-m-d', strtotime($_POST['endDate']));
        
        if(!strcmp($_POST['hidden'],"ADMIN")==0){
            $accsslvl = $_POST['hidden'];
            $condtion = "AND (Barangay = '$accsslvl')";
        }

    }
}
if(isset($_GET['street'])){
    $Condition = $_GET['street'];
    $startDate = $_GET['start'];
    $endDate = $_GET['end'];
    
    $startDateVal = date('Y-m-d', strtotime($_GET['start']));
    $endDateVal = date('Y-m-d', strtotime($_GET['end']));
}else{
    $Condition = "Aglipay";
}
    $BadassSQL="SELECT location.LocName AS 'Barangay',COUNT(flood_reports.Barangay) AS 'Reports' FROM location LEFT OUTER JOIN flood_reports ON location.LocName = flood_reports.Barangay AND (flood_reports.DateTime BETWEEN '$startDate' AND '$endDate') GROUP BY location.LocName";
    $barangays = array();
    
    $stmt = $conn->prepare($BadassSQL);
    $stmt->execute();
    $stmt->bind_result($brgy,$rep);
        
    while($stmt->fetch()){    
        $temp = [
            'Barangay'=>$brgy,
            'Reports'=>$rep
        ];
        
         array_push($barangays, $temp);
    }

$brgyjson = json_encode($barangays);

    $Invalid = "0";
    $Notvalid = "0";
    $Valid = "0";
    $DismissValid = "0";
    
    $RagingSQL="SELECT `Status`, COUNT(`Status`) AS 'TotalStatus' FROM flood_reports WHERE (DateTime BETWEEN '$startDate' AND '$endDate') $condtion GROUP BY `Status` ";
    $result = mysqli_query($conn, $RagingSQL);
    while ($row = mysqli_fetch_array($result)){ 
        switch($row['Status']){
            case "IV":
                $Invalid = $row['TotalStatus'];
            break;
            case "NV":
                $Notvalid = $row['TotalStatus'];
            break;
            case "V":
                $Valid = $row['TotalStatus'];
            break;
            case "V-D":
                $DismissValid = $row['TotalStatus'];
            break;
        }
    }
    
    
    $Validated = $Valid + $DismissValid;
    
    $ReportingSQL = "SELECT DISTINCT `Street`,
    COUNT(`Street`) AS 'No of Reports',
    `Barangay`,
    
    COUNT(
        DISTINCT DATE_FORMAT(`DateTime`, '%c-%d-%Y'),
        ', '
    ) AS 'No of occurances',
    AVG(`FloodLevel`) AS 'Avg flood level',
    GROUP_CONCAT(
        DISTINCT DATE_FORMAT(`DateTime`, '%c-%d-%Y'),
        ', '
    ) AS 'Dates'
FROM
    flood_reports
WHERE (`DateTime` BETWEEN '$startDate' AND '$endDate') $condtion AND (Status = \"V\")
GROUP BY
    `Street`,`Barangay`  
ORDER BY `No of occurances`  DESC";

$Reporting2SQL = "SELECT DISTINCT `Street`,
    COUNT(`Street`) AS 'No of Reports',
    `Barangay`,
    
    COUNT(
        DISTINCT DATE_FORMAT(`DateTime`, '%c-%d-%Y'),
        ', '
    ) AS 'No of occurances',
    AVG(`FloodLevel`) AS 'Avg flood level',
    GROUP_CONCAT(
        DISTINCT DATE_FORMAT(`DateTime`, '%c-%d-%Y'),
        ', '
    ) AS 'Dates'
FROM
    flood_reports
WHERE (`DateTime` BETWEEN '$startDate' AND '$endDate') $condtion AND (Status = \"V\")
GROUP BY
    `Street`,`Barangay`  
ORDER BY `Avg flood level` DESC";


$LineSql = "SELECT DATE_FORMAT(`DateTime`, '%c-%d-%Y') AS \"Dates\", `Street`, ( SELECT AVG(s.`FloodLevel`) FROM flood_reports s WHERE s.Street = '$Condition' AND DATE_FORMAT(s.`DateTime`, '%c-%d-%Y') = Dates ) AS AVG FROM flood_reports WHERE (`DateTime` BETWEEN '$startDate' AND '$endDate') AND `Street` = '$Condition' AND STATUS = \"V\" GROUP BY DATE_FORMAT(`DateTime`, '%c-%d-%Y') ORDER BY `flood_reports`.`DateTime` ASC ";
$Linresult = mysqli_query($conn, $LineSql); 

function transform($num, $total){
    $percent = ($num/$total)*100;
    return $percent;
}

?>

