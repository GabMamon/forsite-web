<?php
session_start();
 include 'DatabaseConfig.php';
 $conn = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
if (isset($_SESSION['stat'])){
  $uid = $_SESSION['id'] ;
  $ufullname = $_SESSION['fullname'] ;
  $usrname = $_SESSION['username'];
  $accslvl= $_SESSION['accesslevel'];
 date_default_timezone_set('Asia/Manila');
 $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
 $re = mysqli_query($conn,$Sql_Query);
 $ro = mysqli_fetch_assoc($re);
 
 $expiretime = $ro['Expiretime'];
 $stime = date('Y-m-d H:i:s', strtotime("-$expiretime hours"));
 $etime = date('Y-m-d H:i:s');
 
 $time = "AND (DateTime BETWEEN '$stime' AND '$etime')";

?>
<html>
	<head>
	    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
		<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link href="CSS/simple-sidebar.css" rel="stylesheet">
			
			<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
							<title>
      Forsite Admin
  </title>
							<style type="text/css">
      .navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
      .username{
          margin-right: 40px;
      }
      .nav-link[data-toggle].collapsed:after {
        content: "▾";
      }
      .nav-link[data-toggle]:not(.collapsed):after {
        content: "▴";
      }
      table tbody th tr td {
        font-size: 10px;
      }
  </style>
						</head>
						<body>
							<nav class="navbar navbar-expand-lg navbar-light bg-light">
							    <a href="#menu-toggle" class="btn btn-outline-secondary" id="menu-toggle">
									<i class="fa fa-bars"></i>
								</a>
								<a class="navbar-brand" href="#">
								<img src="https://forsitefloodapp.000webhostapp.com/Admin/CSS/Images/forsiteiconvar2.png" height="40">
								</a>
								<span class="navbar-text">
									Dashboard 
								</span>
									<div class="collapse navbar-collapse" id="navbarText">
										<ul class="navbar-nav ml-auto">
											<span class="navbar-text username">
												<i class="fa fa-user"></i> Hello <?php echo $ufullname; ?> 
											</span>
											<li class="nav-item">
						<a type="button" class="btn btn-outline-secondary" href="logout">Log-out <i class="fa fa-sign-out"></i></a>
											</li>
										</ul>
									</div>
								</nav>
								<div id="wrapper">
									<div id="sidebar-wrapper">
										<ul class="sidebar-nav nav-pills">
											
											</li>
											<li class="nav-item">
												<a href="admin-home" class="nav-link active rounded-0">Dashboard</a>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#CurReportSub" data-toggle="collapse">Flood Monitoring</a>
												    <div class="collapse" id="CurReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodmap">Flood Map</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodreports">Flood Incidents</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#ReportSub" data-toggle="collapse">Reports</a>
												    <div class="collapse" id="ReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rflood">Flood Reports</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rusers">User Reports</a></li>
                                                        </ul>
                                                    </div>
											</li>
												<?if(strcmp($accslvl,"ADMIN")==0){?>
										<li class="nav-item">
										    <a href="SettingAdmin" class="nav-link rounded-0">Settings</a>
										    </li>
										    <?}?>
										</ul>
									</div>
									<div id="page-content-wrapper">
										<div class="container-fluid">
										    
										    
										    <div class="row">
										        <div class="card col-6">
										            <div class="card-body">
										                    <h5 class="card-title">Recently verified reports</h5>
										                    <?
										                    $FetchPageSQL = "SELECT * FROM flood_reports  WHERE Status =\"V\" $time ORDER BY `DateTime` DESC LIMIT 5";
		                                                    $res = mysqli_query($conn,$FetchPageSQL);
		                                                    $numRows = mysqli_num_rows($res);
		                                                    echo '<table class="table table-hover">';
                                                    		echo "  <thead>";
                                                            echo "    <tr>";
                                                            echo "      <th scope=\"col\">Flood Level</th>";
                                                            echo "      <th scope=\"col\">Street Address</th>";
                                                            echo "      <th scope=\"col\">Date Reported</th>";
                                                            echo "    </tr>";
                                                            echo "  </thead>";
                                                            echo "  <tbody>";
                                                            
                                                            if($numRows===0){
                                                                echo "    <tr id=\"confirmedrecords\">\n";
                                                                echo "      <td colspan=\"3\">No reports found.</td>\n";
                                                                echo "    </tr>\n";
                                                            }
                                                            else{
                                                                while ($row=mysqli_fetch_array($res)){
                                                        		    $streetname = $row['StreetNum']." ".$row['Street'].",".$row['Barangay'];
                                                        		    $floodlvl = "";
                                                        		    $reptime = strtotime($row['DateTime']);
                                                                    $newformat = date('g:i a',$reptime);
                                                        		    if (!is_null($row['FloodLevel'])){
                                                        		        switch ($row['FloodLevel']){
                                                        		            case 1:
                                                        		                $floodlvl = "Code A";
                                                        		                break;
                                                        		            case 2:
                                                            		            $floodlvl = "Code B";
                                                        		                break;
                                                        		            case 3:
                                                            		            $floodlvl = "Code C";
                                                        		                break;
                                                        		            case 4:
                                                            		            $floodlvl = "Code D";
                                                        		                break;
                                                        		            case 5:
                                                            		            $floodlvl = "Code E";
                                                        		                break;
                                                        		        }
                                                        		    }
                                                        		    
                                                                    echo "    <tr id=\"confirmedrecords\">\n";
                                                                    echo "      <td>".$floodlvl."</td>\n";
                                                                    echo "      <td>".$streetname."</td>\n";
                                                                    echo "      <td>".$newformat."</td>\n";
                                                                    echo "    </tr>\n";
                                                        		}
                                                            }
                                                    		echo "  </tbody>\n";
                                                            echo "</table>";?>
										                </div>
										            </div>
										        <div class="card col-6 ">
										            <div class="card-body">
										                    <h5 class="card-title">Recently received reports</h5>
										                    <?
										                    $FetchPageSQL = "SELECT * FROM flood_reports  WHERE Status =\"NV\" $time ORDER BY `DateTime` DESC LIMIT 5";
		                                                    $res = mysqli_query($conn,$FetchPageSQL);
		                                                    $numRows = mysqli_num_rows($res);
		                                                    echo '<table class="table table-hover">';
                                                    		echo "  <thead>";
                                                            echo "    <tr>";
                                                            echo "      <th scope=\"col\">Flood Level</th>";
                                                            echo "      <th scope=\"col\">Street Address</th>";
                                                            echo "      <th scope=\"col\">Date Reported</th>";
                                                            echo "    </tr>";
                                                            echo "  </thead>";
                                                            echo "  <tbody>";
                                                            
                                                            if($numRows===0){
                                                                echo "    <tr id=\"confirmedrecords\">\n";
                                                                echo "      <td colspan=\"3\">No reports found.</td>\n";
                                                                echo "    </tr>\n";
                                                            }
                                                            else{
                                                                while ($row=mysqli_fetch_array($res)){
                                                        		    $streetname = $row['StreetNum']." ".$row['Street'].",".$row['Barangay'];
                                                        		    $floodlvl = "";
                                                        		    $reptime = strtotime($row['DateTime']);
                                                                    $newformat = date('g:i a',$reptime);
                                                        		    if (!is_null($row['FloodLevel'])){
                                                        		        switch ($row['FloodLevel']){
                                                        		            case 1:
                                                        		                $floodlvl = "Code A";
                                                        		                break;
                                                        		            case 2:
                                                            		            $floodlvl = "Code B";
                                                        		                break;
                                                        		            case 3:
                                                            		            $floodlvl = "Code C";
                                                        		                break;
                                                        		            case 4:
                                                            		            $floodlvl = "Code D";
                                                        		                break;
                                                        		            case 5:
                                                            		            $floodlvl = "Code E";
                                                        		                break;
                                                        		        }
                                                        		    }
                                                        		    
                                                                    echo "    <tr id=\"confirmedrecords\">\n";
                                                                    echo "      <td>".$floodlvl."</td>\n";
                                                                    echo "      <td>".$streetname."</td>\n";
                                                                    echo "      <td>".$newformat."</td>\n";
                                                                    echo "    </tr>\n";
                                                        		}
                                                            }
                                                    		echo "  </tbody>\n";
                                                            echo "</table>";?>
										                </div>
										            </div>
										        </div>
										        
										    </div>
										    </br></br>
										    <? $SQL = "SELECT 
      a.datecreated AS DATE,SUM(a.ctr) AS NUM
  FROM 
  (

    SELECT COUNT(*) as ctr, date_format(DateTime, '%d/%m/%y ') as datecreated 
    FROM flood_reports
    GROUP BY DATE(DateTime)
UNION ALL 
    select 0 as ctr, date_format(selected_date, '%d/%m/%y ') as datecreated 
       from 
     (select adddate('2018-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
    where selected_date 
    between (SELECT MIN(DateTime) FROM flood_reports)
        and (SELECT MAX(DateTime) FROM flood_reports)
   ) a
   group by a.datecreated
   order by month(a.datecreated), date(a.datecreated)";
										        $activity = array();
										        $stmt = $conn->prepare($SQL);
                                                $stmt->execute();
                                                $stmt->bind_result($date,$num);
                                                while($stmt->fetch()){    
                                                    $temp = [
                                                        'Date'=>$date,
                                                        'No_Reports'=>$num
                                                    ];
                                                    
                                                     array_push($activity, $temp);
                                                }
                                                $actvtyjson = json_encode($activity);
                                            	?>
										    <div class="row">
										        <div class="card col-12">
										            <div class="card-body">
										                <h5 class="card-title">Reporting History</h5>
										                <canvas id="linechart" height="50"></canvas>
										            </div>
										        </div>
										        
										    </div>
										    <script>
										    
										    var lineLabels = [];
										    var lineData = [];
						
										        var linejson;
                                                linejson = <?php echo $actvtyjson ?>;
                                                
                                                linejson.forEach(function(item) {
                                                    lineLabels.push(item.Date);
                                                    lineData.push(item.No_Reports); });
                                                    
                                                
                                                //var lineLabels = linejson.map(function(e) {return e.Date;});
                                                //var lineData = linejson.map(function(e) {return e.No_Reports;});
                                                
                                               
  
                                                var canvas = document.getElementById("linechart");
                                            
                                                
                                                var lineDataset = {
                                                    label: 'Reports',
                                                    pointRadius: 0,
                                                    data: lineData,
                                                    backgroundColor: "#ADD8E6",
                                                }
                                                
                                                var option = {
	                                                showLines: true,
	                                                tooltips:{
	                                                     mode: 'index',
	                                                     intersect: false
	                                                },
	                                                hover: {
                                                        mode: 'index',
                                                        intersect: false
                                                     },
	                                                scaleShowValues: true,
                                                            scales: {
                                                                yAxes: [{
                                                                    ticks: {
                                                                        suggestedMin: 1,
                                                                        userCallback: function(label, index, labels) {
                                                                            if (Math.floor(label) === label) {
                                                                                return label;
                                                                            }
                                                
                                                                        },
                                                                    }
                                                                }],
                                                                xAxes: [{
                                                                    gridLines: {
                                                                        display: false,
                                                                        drawOnChartArea: false,
                                                                        drawTicks: false
                                                                      },
                                                                    ticks: {
                                                                        
                                                                        autoSkip: true,
                                                                        display: false                  }
                                                                }]
                                                            }
                                                };
                                                
                                                var myLineChart = new Chart(canvas, {
                                                    type: 'line',
                                                    data: {
                                                            labels: lineLabels,
                                                            datasets: [lineDataset]
                                                        },
                                                    options:option
                                                });
                                                
                                                
                                                
                                                
										    </script>
										    
											
										</div>
									</div>
									<!-- /#page-content-wrapper -->
								</div>
								<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    <?php }
    else{
        header("Location: login");
    }
    
    ?>
							</body>
						</html>