<?php
session_start();
 include 'DatabaseConfig.php';
 $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 $dismissresponse = "";
 $validateresponse = "";
 $invalidateresponse = "";
 
 date_default_timezone_set('Asia/Manila');
 $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
 $re = mysqli_query($con,$Sql_Query);
 $ro = mysqli_fetch_assoc($re);
 
 $expiretime = $ro['Expiretime'];
 $stime = date('Y-m-d H:i:s', strtotime("-$expiretime hours"));
 $etime = date('Y-m-d H:i:s');
 
 $time = "AND (DateTime BETWEEN '$stime' AND '$etime')";
 
if (isset($_SESSION['stat'])){
  $uid = $_SESSION['id'] ;
  $ufullname = $_SESSION['fullname'] ;
  $usrname = $_SESSION['username'];
  $accslvl= $_SESSION['accesslevel'];
  $condtion = "";
  
  if(!strcmp($accslvl,"ADMIN")==0){
      $condtion = "AND (Barangay = '$accslvl')";
  }
  
  if (isset($_SESSION['disres'])){
      $dismissresponse = $_SESSION['disres'];
  }
?>
<html>
	<head>
	    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
       
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="CSS/simple-sidebar.css" rel="stylesheet">
		
		
   <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
				
	<title>
      Forsite Admin| Flood Incidents
    </title>
	<style type="text/css">
	#alert {
        z-index: 10;
	    position: fixed;
	    width: 100%;
        height:50px;
        left:50%;
        transform:translateX(-50%);
        bottom:10px;
    }
      .navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
      .username{
          margin-right: 40px;
      }
      .nav-link[data-toggle].collapsed:after {
        content: "▾";
      }
        .nav-link[data-toggle]:not(.collapsed):after {
        content: "▴";
        }
      td { font-size: 13px; }
      th { font-size: 15px; }
  </style>
						</head>
						<body>
							<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
							    <a href="#menu-toggle" class="btn btn-outline-secondary" id="menu-toggle">
									<i class="fa fa-bars"></i>
								</a>
								<a class="navbar-brand" href="http://forsitefloodapp.xyz/Admin/admin-home">
									<img src="http://forsitefloodapp.xyz/Admin/CSS/Images/forsiteiconvar2.png" height="40">
									</a>
									<span class="navbar-text">
									Flood Incidents 
								</span>
									<div class="collapse navbar-collapse" id="navbarText">
										<ul class="navbar-nav ml-auto">
											<span class="navbar-text username">
												<i class="fa fa-user"></i> Hello <?php echo $ufullname; ?> 
											</span>
											<li class="nav-item">
												<a type="button" class="btn btn-outline-secondary" href="logout">Log-out <i class="fa fa-sign-out"></i></a>
												</button>
											</li>
										</ul>
									</div>
								</nav>
								<div id="wrapper">
									<div id="sidebar-wrapper">
										<ul class="sidebar-nav nav-pills">
											
											</li>
											<li class="nav-item">
												<a href="admin-home" class="nav-link rounded-0">Dashboard</a>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0 active" href="#CurReportSub" data-toggle="collapse">Flood Monitoring</a>
												    <div class="collapse" id="CurReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodmap">Flood Map</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0 active" href="floodreports">Flood Incidents</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#ReportSub" data-toggle="collapse">Reports</a>
												    <div class="collapse" id="ReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rflood">Flood Reports</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rusers">User Reports</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<?if(strcmp($accslvl, "ADMIN")==0){?>
										<li class="nav-item">
										    <a href="SettingAdmin" class="nav-link rounded-0">Settings</a>
										    </li>
										    <?}?>
										</ul>
									</div>
									<div id="page-content-wrapper">
										<div class="container-fluid">
										    
	<div class="row border rounded bg-light px-1 py-3">
        <div class="col-lg-12">
	    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Verified Reports</h4>
                        </div>
	    
			<?php

		$FetchPageSQL = "SELECT FloodID,FloodLevel,Latitude,Longtitude,GROUP_CONCAT(StreetNum SEPARATOR ', ') AS StreetNum,Street,Barangay,DateTime,Status,RemarkCategory,Remarks,COUNT(UserReported) AS 'COUNT',ImagePath FROM flood_reports WHERE
                                                                         (Status =\"V\") $time $condtion GROUP BY Dup";
                                                                        
		$res = mysqli_query($con,$FetchPageSQL);
		
		echo '<table class="table table-hover" id="table1">';
		echo "  <thead>";
        echo "    <tr>";
        echo "      <th scope=\"col\">Report ID</th>";
        echo "      <th scope=\"col\">Flood Level</th>";
        echo "      <th scope=\"col\">Street Address(es)</th>";
        if(strcmp($accslvl,"ADMIN")==0){
        echo "      <th scope=\"col\">Barangay Affected</th>\n";   
        }
        echo "      <th scope=\"col\">Date Reported</th>";
        echo "      <th scope=\"col\">Report Details</th>";
        echo "      <th scope=\"col\">User(s) Reported</th>";
        echo "      <th scope=\"col\">Attached Image</th>";
        echo "      <th scope=\"col\">Action</th>";
        echo "    </tr>";
        echo "  </thead>";
        echo "  <tbody>";
        
		while ($row=mysqli_fetch_array($res)){
		    $FID = $row['FloodID'];
		    $lat = $row['Latitude'];
		    $lon = $row['Longtitude'];
		    $floodlvl = "";
		    
		    $StreetArr = explode(', ',$row['StreetNum']);
            $StreetArr = array_unique($StreetArr); 
            $StreetNum = implode(', ',$StreetArr);
            
		    $streetname = $StreetNum." ".$row['Street'];
		    
		    if (!is_null($row['FloodLevel'])){
		        switch ($row['FloodLevel']){
		            case 1:
		                $floodlvl = "Code A";
		                break;
		            case 2:
    		            $floodlvl = "Code B";
		                break;
		            case 3:
    		            $floodlvl = "Code C";
		                break;
		            case 4:
    		            $floodlvl = "Code D";
		                break;
		            case 5:
    		            $floodlvl = "Code E";
		                break;
		        }
		    }
		    $UserReported = $row['COUNT'];
            echo "    <tr id=\"confirmedrecords\">\n";
            echo "      <td class=\"dfi\">".$FID."</a></td>\n";
            echo "      <td class=\"dfl\">".$floodlvl."</a></td>\n";
            echo "      <td class=\"dst\"><a href=\"floodmap?lat='$lat'&lon='$lon' \">".$streetname."</a></td>\n";
                        if(strcmp($accslvl,"ADMIN")==0){
            echo "      <td>".$row['Barangay']."</td>\n";  
            }
            echo "      <td>".$row['DateTime']."</td>\n";
            echo "      <td><b>".$row['RemarkCategory']."</b><br>".$row['Remarks']."</td>\n";
            echo "      <td>".$UserReported."</td>\n";
            if(!isset($row['ImagePath'])){
            echo "<td></td>";
            }else{
            
            echo "      <td><a href=\"".$row['ImagePath']."\" target=\"_blank\">Link</a></td>\n";
            }
            echo "      <td><button type=\"button\" class=\"btn btn-danger dismiss\">Dismiss</button></td>\n";
            echo "    </tr>\n";

		}
		echo "  </tbody>\n";
        echo "</table>";

	?>


</div>
</div>
</div>
</br></br>
<div class="row border rounded bg-light px-1 py-3">
        <div class="col-lg-12">
	    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Unconfirmed Flood Reports</h4>
                        </div>
        
	<?php

		$FetchPageSQL = "SELECT FloodID,FloodLevel,Latitude,Longtitude,GROUP_CONCAT(StreetNum SEPARATOR ', ') AS StreetNum,Street,Barangay,DateTime,Status,RemarkCategory,Remarks,COUNT(UserReported) AS 'COUNT',ImagePath FROM flood_reports WHERE
                                                                         (Status =\"NV\") $time $condtion GROUP BY Dup";
		$res = mysqli_query($con,$FetchPageSQL);
		
		echo '<table class="table table-hover" id="table2">';
		echo "  <thead>\n";
        echo "    <tr>\n";
        echo "      <th scope=\"col\">Report ID</th>";
        echo "      <th scope=\"col\">Flood Level</th>\n";
        echo "      <th scope=\"col\">Street Address(es)</th>\n";
        if(strcmp($accslvl,"ADMIN")==0){
        echo "      <th scope=\"col\">Barangay Affected</th>\n";   
        }
        echo "      <th scope=\"col\">Date Reported</th>\n";
       echo "      <th scope=\"col\">Report Info</th>";;
        echo "      <th scope=\"col\">User(s) Reported</th>";
        echo "      <th scope=\"col\">Attached Image</th>";
        
        echo "      <th scope=\"col\">Action</th>";
        echo "    </tr>\n";
        echo "  </thead>\n";
        echo "  <tbody>\n";
        
		while ($row=mysqli_fetch_array($res)){
		    $FID = $row['FloodID'];
		    
		    $StreetArr = explode(', ',$row['StreetNum']);
            $StreetArr = array_unique($StreetArr); 
            $StreetNum = implode(', ',$StreetArr);
            
		    $streetname = $StreetNum." ".$row['Street'];
		    $lat = $row['Latitude'];
		    $lon = $row['Longtitude'];
		    
		    $floodlvl = "";
		    
		    if (!is_null($row['FloodLevel'])){
		        switch ($row['FloodLevel']){
		            case 1:
		                $floodlvl = "Code A";
		                break;
		            case 2:
    		            $floodlvl = "Code B";
		                break;
		            case 3:
    		            $floodlvl = "Code C";
		                break;
		            case 4:
    		            $floodlvl = "Code D";
		                break;
		            case 5:
    		            $floodlvl = "Code E";
		                break;
		        }
		    }
		    
		    $UserReported = $row['COUNT'];

            echo "    <tr id=\"unconfirmedrecords\">\n";
            echo "      <td class=\"cfi\">".$row['FloodID']."</a></td>\n";
            echo "      <td class=\"cfl\">".$floodlvl."</a></td>\n";
            echo "      <td class=\"cst\"><a href=\"floodmap?lat='$lat'&lon='$lon' \">".$streetname."</a></td>\n";
            if(strcmp($accslvl,"ADMIN")==0){
            echo "      <td>".$row['Barangay']."</td>\n";  
            }
            echo "      <td>".$row['DateTime']."</td>\n";
            echo "      <td><b>".$row['RemarkCategory']."</b><br>".$row['Remarks']."</td>\n";
            echo "      <td>".$UserReported."</td>\n";
            if(!isset($row['ImagePath'])){
            echo "<td></td>";
            }else{
            echo "      <td><a href=\"".$row['ImagePath']."\" target=\"_blank\">Link</a></td>\n";
            }
            echo "      <td class=\"align-middle\" >
            <button type=\"button\" class=\"btn btn-primary validate\">Confirm</button>
            <button type=\"button\" class=\"btn btn-danger invalidate\">Invalidate</button></td>\n";
            echo "    </tr>\n";

		}
		echo "  </tbody>\n";
        echo "</table>";

	?>
		</div>
		</div>
		</div>

<!-- DISMISS MODAL -->
<div class="modal fade" id="dismissModal" tabindex="-1" role="dialog" aria-labelledby="dismissModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dismissModalLabel">Dismiss Report?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div class="modal-body">
        <p id="dismissMsg"></p>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="visual"> Confirmed through visual contact.
        </label>
        </br>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="inquiry"> Confirmed through inquiry.
        </label>
        </br>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="radio"> Confirmed through radio.
        </label>
        </br>
        <label for="comment">Remarks:</label>
        <textarea class="form-control" rows="3" id="discomment" maxlength="60"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="dismissReport">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- VALIDATE MODAL -->
<div class="modal fade" id="validateModal" tabindex="-1" role="dialog" aria-labelledby="validateModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="validateModalLabel">Validate Report?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div class="modal-body">
        <p id="validateModalMsg"></p>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="visual"> Confirmed through visual contact.
        </label>
        </br>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="inquiry"> Confirmed through inquiry.
        </label>
        </br>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="radio"> Confirmed through radio.
        </label>
        </br>
        <label for="comment">Remarks:</label>
        <textarea class="form-control" rows="2" id="valcomment" maxlength="60"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success " disabled id="validateReport">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- INVALIDATE MODAL -->
<div class="modal fade" id="invalidateModal" tabindex="-1" role="dialog" aria-labelledby="invalidateModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="invalidateModalLabel">Invalidate Report?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div class="modal-body">
        <p id="invalidateModalMsg"></p>
        <label for="comment">Remarks:</label>
        <textarea class="form-control" rows="3" id="invacomment" maxlength="60"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" id="invalidateReport">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Alerts -->
<div id="alert" class='container'>
        

<script>
    var $fid = "";
    var $flv = "";
    var $fst = "";
    var option = "";
    var response = "";
    var usr = "";
    
    function checkDismissAlert(){
        var alerts = "<?php echo  $dismissresponse; ?>";
        if(alerts==="success"){
            var MsgString = "Record has been succesfully updated!";
            var alrtclr = "alert-success";
            showAlert(MsgString,alrtclr);
            
        }else if(alerts==="failed"){
            var MsgString = "An error occured! Try again later.";
            var alrtclr = "alert-danger";
            showAlert(MsgString,alrtclr);
            
        }else if(alerts==="notfound"){
            var MsgString = "Unable to find record!";
            var alrtclr = "alert-warning";
            showAlert(MsgString,alrtclr);
            
        }else{
            var MsgString = "";
            var alrtclr = "";
        }
    }
    function showAlert(message,color) {
          $('#alert').html("<div class='alert "+color+" alert-dismissible fade show' role=\"alert\">"+message+"<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>");
          $('#alert').show();
          <?php unset($_SESSION['disres'])?>
        }
        
    window.onload = checkDismissAlert;
        

    $(".dismiss").click(function() {
            var Row = document.getElementById("confirmedrecords");
            var Cells = Row.getElementsByTagName("td");
        
            $fid = Cells[0].innerText; // Find the id
            $flv = Cells[1].innerText; // Find the flevel
            $fst = Cells[2].innerText; // Find the street
            document.getElementById("dismissMsg").innerHTML = "Dismiss flood report ID:"+$fid+" "+$flv+": "+$fst+"?";
            $('#dismissModal').modal('show'); 

     $("#dismissReport").click(function() {
            <?php
            $_SESSION['connect'] = "YES";
            $_SESSION['usr'] = $usrname;
            ?>
            response = document.getElementById("discomment").value;
            
            var url = "http://forsitefloodapp.xyz/Admin/ProsdismissReport.php?fid="+$fid+"&choice="+option+"&remarks="+response;
            window.location.href = encodeURI(url);
        });
    });
    
     $(".validate").click(function() {
            var Row = document.getElementById("unconfirmedrecords");
            var Cells = Row.getElementsByTagName("td");
        
            $fid = Cells[0].innerText; // Find the id
            $flv = Cells[1].innerText; // Find the flevel
            $fst = Cells[2].innerText; // Find the street
            document.getElementById("validateModalMsg").innerHTML = "Confirm to validate flood report ID:"+$fid+" "+$flv+": "+$fst+"?";
            $('#validateModal').modal('show'); 

     $("#validateReport").click(function() {
            
            <?php
            $_SESSION['connect'] = "YES";
            $_SESSION['usr'] = $usrname;
            ?>
            response = document.getElementById("valcomment").value;
            
            var url = "http://forsitefloodapp.xyz/Admin/ProsvalidateReport.php?fid="+$fid+"&choice="+option+"&remarks="+response;
            window.location.href = encodeURI(url);
        });
    });
    
    $(".invalidate").click(function() {
            var Row = document.getElementById("unconfirmedrecords");
            var Cells = Row.getElementsByTagName("td");
        
            var $fid = Cells[0].innerText; // Find the id
            var $flv = Cells[1].innerText; // Find the flevel
            var $fst = Cells[2].innerText; // Find the street
            document.getElementById("invalidateModalMsg").innerHTML = "Confirm to invalidate this flood report ID:"+$fid+" "+$flv+": "+$fst+"?";
            $('#invalidateModal').modal('show'); 

     $("#invalidateReport").click(function() {
            
            <?php
            $_SESSION['connect'] = "YES";
            $_SESSION['usr'] = $usrname;
            ?>
            response = document.getElementById("invacomment").value;
            var url = "http://forsitefloodapp.xyz/Admin/ProsinvalidateReport.php?fid="+$fid+"&remarks="+response;
            window.location.href = encodeURI(url);
        });
    });
        
    $(document).ready(function() {
      $('[data-toggle="popover"]').popover({ trigger: "hover" });
     });
    
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
    $(document).ready(function() {
        $('#table1').DataTable({
            responsive: true
        });
    });
    
    $(document).ready(function() {
        $('#table2').DataTable({
            responsive: true
        });
    });
    
    $('input[type=radio]').click(function() {
        option = $(this).val();
        $("#validateReport").removeAttr("disabled");
    });
   
    
</script>


 <?php }
    else{
        header("Location: login.php");
    }
    
    ?>
							</body>
						</html>