<?php
session_start();
 include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
if (isset($_SESSION['stat'])){
  $uid = $_SESSION['id'] ;
  $ufullname = $_SESSION['fullname'] ;
  $usrname = $_SESSION['username'];
  $accslvl= $_SESSION['accesslevel'];
  $t = "";

    if(isset($_GET['start'])&&isset($_GET['end'])){
        $s = mysqli_escape_string($con, $_GET['start']);
        $e = mysqli_escape_string($con, $_GET['end']);
        
        $t = "?s=$s&e=$e";
    }
    
     if(isset($_GET['lat']) && isset($_GET['lon'])){
           $l1 = $_GET['lat'];
           $l2 = $_GET['lon'];
           $z=20;
       }
       else{
            $l1=14.5820402;
            $l2=121.0347713;
            $z=14;
       }
?>
<html>
	<head>
	    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
		<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
						<link href="CSS/simple-sidebar.css" rel="stylesheet">
							<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
							<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
							<script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
							<title>
      Forsite Admin | Flood Map
  </title>
							<style type="text/css">
      .navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
      .username{
          margin-right: 40px;
      }
      .nav-link[data-toggle].collapsed:after {
        content: "▴";▴
      }
        .nav-link[data-toggle]:not(.collapsed):after {
        content: "▾";
        }
      #map {
        width: 100%;
        height: 600px;
        background-color: grey;
      }
      
  </style>
						</head>
						<body>
							<nav class="navbar navbar-expand-lg navbar-light bg-light">
							    <a href="#menu-toggle" class="btn btn-outline-secondary" id="menu-toggle">
									<i class="fa fa-bars"></i>
								</a>
								<a class="navbar-brand" href="http://forsitefloodapp.xyz/Admin/admin-home">
									<img src="http://forsitefloodapp.xyz/Admin/CSS/Images/forsiteiconvar2.png" height="40">
									</a>
									<span class="navbar-text">
									Flood Map 
								</span>
									<div class="collapse navbar-collapse" id="navbarText">
										<ul class="navbar-nav ml-auto">
											<span class="navbar-text username">
												<i class="fa fa-user"></i> Hello <?php echo $ufullname; ?> 
											</span>
											<li class="nav-item">
						                        <a type="button" class="btn btn-outline-secondary" href="logout">Log-out <i class="fa fa-sign-out"></i></a>
											</li>
										</ul>
									</div>
								</nav>
								<div id="wrapper">
									<div id="sidebar-wrapper">
										<ul class="sidebar-nav nav-pills">
											
											</li>
											<li class="nav-item">
												<a href="admin-home" class="nav-link rounded-0">Dashboard</a>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0 active" href="#CurReportSub" data-toggle="collapse">Flood Monitoring</a>
												    <div class="collapse" id="CurReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0 active" href="floodmap">Flood Map</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodreports">Flood Incidents</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#ReportSub" data-toggle="collapse">Reports</a>
												    <div class="collapse" id="ReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rflood">Flood Reports</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rusers">User Reports</a></li>
                                                        </ul>
                                                    </div>
											</li>
										<?if(strcmp($accslvl, "ADMIN")==0){?>
										<li class="nav-item">
										    <a href="SettingAdmin" class="nav-link rounded-0">Settings</a>
										    </li>
										    <?}?>
										</ul>
									</div>
									<div id="page-content-wrapper">
										<div class="container-fluid">
											<h3>Flood Flood Map</h3> 
											<p id="demo"></p>
											<div id="map"></div>
										</div>
									</div>
									<!-- /#page-content-wrapper -->
								</div>
								<!-- /#wrapper -->

    <script>
      var customLabel = {
        1: {
          label: 'A'
        },
        2: {
          label: 'B'
        },
        3: {
          label: 'C'
        },
        4: {
          label: 'D'
        },
        5: {
          label: 'E'
        }
      }; 
      
    var l1= <?php echo $l1 ?>;
    var l2= <?php echo $l2 ?>;
    var zo1= <?php echo $z ?>;
    var mpstyle = [
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

        function initMap() {
        var uluru = {lat: parseFloat(l1), lng: parseFloat(l2)}; //centers mandaluyong city
        var map = new google.maps.Map(document.getElementById('map'), {
           zoom: zo1,
           center: uluru,
           styles: mpstyle
       });
       
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
          downloadUrl('http://forsitefloodapp.xyz/Admin/mapxml.php<?echo $t;?>', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
                var id = markerElem.getAttribute('id');
                var street = markerElem.getAttribute('str');
                var time = markerElem.getAttribute('ti');
                var floodtype = markerElem.getAttribute('floodtype');
                var noofusers = markerElem.getAttribute('noofusers');
                var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));
                
                var infowincontent = document.createElement('div');
                var strong = document.createElement('strong');
                strong.textContent = "Address: "+street
                infowincontent.appendChild(strong);
                infowincontent.appendChild(document.createElement('br'));
                infowincontent.appendChild(document.createElement('br'));
                
                var frmttm = moment(time).format("h:mm a MMM DD YYYY");
                
                var text = document.createElement('text');
                text.textContent = "Time: "+frmttm
                infowincontent.appendChild(text);
                infowincontent.appendChild(document.createElement('br'));
                
                var text2 = document.createElement('text');
                text2.textContent = noofusers+" user/s reported"
                infowincontent.appendChild(text2);
                
                var icon = customLabel[floodtype] || {};

              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: {
                    text:icon.label,
                    color: "white"
                }
              });
              marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
              });
        
            });
          });
        }

        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmJ2ro1PkFkl00mbeBRePSO4-So3kfjXM&callback=initMap">
    
    </script>
    <?php }
    else{
        header("Location: login.php");
    }
    
    ?>
							</body>
						</html>