<?php
session_start();
if (isset($_SESSION['stat'])){
  $uid = $_SESSION['id'] ;
  $ufullname = $_SESSION['fullname'] ;
  $usrname = $_SESSION['username'];
  $accslvl= $_SESSION['accesslevel'];

if(!strcmp($accslvl,"ADMIN")==0){
      $condtion =$accslvl;
  }else{
      $condtion = "";
  }
include 'Prosbarangay.php';

$accsslvl = $accslvl;
?>
<html>
    <head>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <link href="CSS/simple-sidebar.css" rel="stylesheet">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato"/>

                <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
                
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
    <title>
      Reports
    </title>
                            <style type="text/css">
      .navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
      .username{
          margin-right: 40px;
      }
      .nav-link[data-toggle].collapsed:after {
        content: "▾";
      }
        .nav-link[data-toggle]:not(.collapsed):after {
        content: "▴";
      }

  </style>
                        </head>
                        <body>
                            <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
                                
                                <a class="navbar-brand" href="#">
                                    <img src="https://forsitefloodapp.000webhostapp.com/Admin/CSS/Images/forsiteiconvar2.png" height="40">
                                    </a>
                                    <div class="collapse navbar-collapse" id="navbarText">
                                        <ul class="navbar-nav ml-auto">
                                            <span class="navbar-text username">
                                                <i class="fa fa-user"></i> Hello <?php echo $ufullname; ?> 
                                            </span>
                                        </ul>
                                    </div>
                                </nav>
                                <div id="wrapper">
                                    <div id="page-content-wrapper">
                                        <div class="container-fluid">
                                                      <div class="card">
                                                          <div class="card-header">
                                                              <h4>Admin Audit Logs</h4>
                                                          </div>
                                                        <div class="card-body">
                                                                                                                  <?php
                                                        include 'DatabaseConfig.php';
                                                         $conn = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
                                                             if (!$conn)
                                                              {  
                                                                die ('Fail to connect to MySQL: ' . mysqli_connect_error()); 
                                                                 }

                                                         

                                                                   $query = "SELECT * FROM audit_logs";     
                                                                         $result = mysqli_query($conn, $query);   if (!$result) 
                                                                         {  
                                                                            die ('SQL Error: ' . mysqli_error($conn)); } 

                                                            echo '<table class="table table-hover" id="table1">';
                                                            echo "  <thead>";
                                                            echo "    <tr>";
                                                            echo "      <th scope=\"col\">Admin #</th>";
                                                            echo "      <th scope=\"col\">User</th>";
                                                            echo "      <th scope=\"col\">Date/Time</th>";
                                                            echo "      <th scope=\"col\">Audit Description</th>";
                                                            echo "      <th scope=\"col\">User's Remarks</th>";
                                                            echo "    </tr>";
                                                            echo "  </thead>";
                                                            echo "  <tbody>";
                                                        $num=0; 
                                                        while ($row = mysqli_fetch_array($result)){  
                                        ?> 
                                        <tr>
                                        <td><font face="Arial, Helvetica, sans-serif"><?php echo $row['auditno'];?></font></td>
                                        <td><font face="Arial, Helvetica, sans-serif"><?php echo $row['audituser'];?></font></td>
                                        <td><font face="Arial, Helvetica, sans-serif"><?php echo $row['audittime'];?></font></td> 
                                        <td><font face="Arial, Helvetica, sans-serif"><?php echo $row['auditdetails'];?></font></td> 
                                        <td><font face="Arial, Helvetica, sans-serif"><?php echo $row['auditremarks'];?></font></td> 
                                        </tr> 
                                        <?php
                                        } 
                                        echo "</table>";
                                        ?>
                                                            
                                              
                                        </div>
                                                    </div>

                                    </div>
                                                </div>
                                            
                                    
                                <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $(document).ready(function() {
        $('#table1').DataTable({
            responsive: true
        });
    });
    
    document.getElementById("total").innerHTML = "Total Reports: " + t;
    
    </script>
    <?php }
    else{
        header("Location: login");
    }
    
    ?>
                            </body>
                        </html>