<?php
session_start();
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html>
<head>
	<style type="text/css">
		.wrap
{
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 99;
}

p.form-title
{
    font-family: 'Arial' , sans-serif;
    font-size: 30px;
    font-weight: 600;
    text-align: center;
    color: #FFFFFF;
    margin-top: 5%;
    text-transform: uppercase;
    letter-spacing: 4px;
}

form
{
    width: 450px;
    margin: 0 auto;
}

form.login input[type="text"], form.login input[type="password"],form.login input[type="email"], form.login select
{
    width: 100%;
    margin: 0;
    padding: 5px 15px;
    font-style: italic;
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 1px;
    margin-bottom: 5px;
}

form.login input[type="submit"]
{
    width: 100%;
    font-size: 18px;
    text-transform: uppercase;
    font-weight: 500;
    margin-top: 16px;
    outline: 0;
    cursor: pointer;
    letter-spacing: 1px;
}

form.login .forgot-pass-content
{
    min-height: 20px;
    margin-top: 10px;
    margin-bottom: 10px;
}
form.login .remember-forgot
{
	float: right;
    width: 100%;
    margin: 10px 0 0 0;
}
form.login label, form.login a
{
    font-size: 12px;
    font-weight: 400;
    color: #FFFFFF;
}

form.login a
{
    transition: color 0.5s ease;
}

form.login a:hover
{
    color: #2ecc71;
}

.pr-wrap
{
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 999;
    display: none;
}

.show-pass-reset
{
    display: block !important;
}

.pass-reset
{
    margin: 0 auto;
    width: 350px;
    position: relative;
    margin-top: 22%;
    z-index: 999;
    background: #FFFFFF;
    padding: 20px 15px;
}

.pass-reset label
{
    font-size: 14px;
    font-weight: 400;
    margin-bottom: 15px;
}

.pass-reset input[type="submit"]:hover
{
    transition: background-color 0.5s ease;
}
	</style>
<meta charset="utf-8">
<link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration</title>
    
<body background="CSS\Images\blues.jpg">
<div class="jumbotron text-center">
  <img src="CSS\Images\forsiteiconadmin.png" class="avatar" height="100px">
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <p class="form-title">
                    Admin Registration</p>
                <form class="login" method="POST" action="Prosregister">
                <input type="text" class="form-control" placeholder="Username" name="username" maxlenght="40" required />
                <input type="text" class="form-control" placeholder="FirstName" name="fname" maxlenght="40" required />
                <input type="text" class="form-control" placeholder="LastName" name="lname" maxlenght="40" required />
                <input type="email" class="form-control" placeholder="Email Address" name="contactno" maxlenght="20" required />
                <input type="password" class="form-control" placeholder="Password" name="password" pattern=".{8,40}" required title="8 to 40 characters"/>
                
                 <div class="form-group">
                  <label for="sel1">Requested Access Level:</label>
                  <select class="form-control" name="brgy">
                    <option value="Admin">Admin</option>
                    <option value="Addition Hills">Addition Hills</option>
                    <option value="Bagong Silang">Bagong Silang</option>
                    <option value="Barangka Drive">Barangka Drive</option>
                    <option value="Barangka Ibaba">Barangka Ibaba</option>    
                    <option value="Barangka Ilaya">Barangka Ilaya</option>   
                    <option value="Barangka Itaas">Barangka Itaas</option>
                     <option value="Buayang Bata">Buayang Bato</option>
                     <option value="Burol">Burol</option>
                     <option value="Daang Bakal">Daang Bakal</option>
                     <option value="Hagdang Bato Itaas">Hagdang Bato Itaas</option>
                     <option value="Hagdang Bato Libis">Hagdang Bato Libis</option>
                     <option value="Harapin ang Bukas">Harapin ang Bukas</option>
                     <option value="Highway Hills">Highway Hills</option>
                     <option value="Hulo">Hulo</option>
                     <option value="Addition Hills">Addition Hills</option>
                     <option value="Mabini J Rizal">Mabini-J. Rizal</option>
                     <option value="Malamig">Malamig</option>
                     <option value="Mauway">Mauway</option>
                     <option value="Namayan">Namayan</option>
                     <option value="New Zaniga">New Zaniga</option>
                     <option value="Old Zaniga">Old Zaniga</option>
                     <option value="Pag-Asa">Pag-Asa</option>
                     <option value="Plainview">Plainview</option>
                     <option value="Pleasant Hills">Pleasant Hills</option>
                     <option value="Poblacion">Poblacion</option>
                     <option value="San Jose">San Jose</option>
                     <option value="Vergara">Vergara</option>
                     <option value="Wack-Wack Greenhills">Wack-Wack</option>
                  </select>
                  </select>
                </div> 
                
                <input type="text" class="form-control" placeholder="Registration Code" name="regcode" maxlenght="20" required />
                </br>
                <center>
                <img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" /></br>
                <a href="#" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a></br>
                </center>
                <input type="text" placeholder="Type the text" name="captcha_code" size="10" maxlength="6" required/>
                <input type="hidden" id="id" name="id" value="true">
                <input type="submit" value="Create" class="btn btn-success btn-sm" />

                </form>
                <br><br>
            </div>
        </div>
    </div>
</div>
</body>
</head>
</html>