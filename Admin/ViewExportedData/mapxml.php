<?php
 include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 date_default_timezone_set('Asia/Manila');
 $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
 $re = mysqli_query($con,$Sql_Query);
 $ro = mysqli_fetch_assoc($re);

if(isset($_GET['s']) && isset($_GET['e'])){
    $stime= mysqli_escape_string($con,$_GET['s']);
    $etime= mysqli_escape_string($con, $_GET['e']);
}else{
 $expiretime = $ro['Expiretime'];
 $stime = date('Y-m-d H:i:s', strtotime("-$expiretime hours"));
 $etime = date('Y-m-d H:i:s');
}
    

 $time = "AND (DateTime BETWEEN '$stime' AND '$etime')";
 
    function parseToXML($htmlStr){
        $xmlStr=str_replace('<','&lt;',$htmlStr);
        $xmlStr=str_replace('>','&gt;',$xmlStr);
        $xmlStr=str_replace('"','&quot;',$xmlStr);
        $xmlStr=str_replace("'",'&#39;',$xmlStr);
        $xmlStr=str_replace("&",'&amp;',$xmlStr);
        return $xmlStr;
    }

    $query = "SELECT * FROM flood_reports_imported WHERE (status='V' OR status='NV') $time";
    
    $result = mysqli_query($con,$query);
    if (!$result) {
        die('Invalid query: ' . mysqli_error($con));
    }
    
    header("Content-type: text/xml");

    // Start XML file, echo parent node
        echo "<?xml version='1.0' ?>";
        echo '<markers>';
        $ind=0;
    // Iterate through the rows, printing XML nodes for each
        while ($row = mysqli_fetch_assoc($result)){
    // Add to XML document node
        $FID = $row['FloodID'];
		
        $street = $row['StreetNum']." ".$row['Street']."";
        
        echo '<marker ';
        echo 'id="' . $row['FloodID'] . '" ';
        echo 'str="' . parseToXML($street) . '" ';
        echo 'ti="' . parseToXML($row['DateTime']) . '" ';
        echo 'lat="' . $row['Latitude'] . '" ';
        echo 'lng="' . $row['Longtitude'] . '" ';
        echo 'floodtype="' . $row['FloodLevel'] . '" ';
        echo 'noofusers="' . $row['UserReported'] . '" ';
        echo '/>';
         $ind = $ind + 1;
    }

    // End XML file
        echo '</markers>';

 ?>