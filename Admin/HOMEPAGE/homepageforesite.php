<?php

/**
 * @author cotton
 * @copyright 2018
 */



?>
<!DOCTYPE html>
<html>
<title>HOMEPAGE</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="CSS/simple-sidebar.css" rel="stylesheet">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif;}
body, html {
    height: 100%;
    color: #777;
    line-height: 1.8;
}

/* Create a Parallax Effect */
.bgimg-1, .bgimg-2, .bgimg-3 {
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

/* First image (Logo. Full height) */
.bgimg-1 {
    background-image: url("/w3images/parallax1.jpg");
    min-height: 100%;
}

/* Second image (Portfolio) */
.bgimg-2 {
    background-image: url("/w3images/parallax2.jpg");
    min-height: 400px;
}

/* Third image (Contact) */
.bgimg-3 {
    background-image: url("/w3images/parallax3.jpg");
    min-height: 400px;
}

.navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
/* Turn off parallax scrolling for tablets and phones */
@media only screen and (max-device-width: 1024px) {
    .bgimg-1, .bgimg-2, .bgimg-3 {
        background-attachment: scroll;
    }
}
</style>
<body>

<!-- Navbar (sit on top) -->
<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
							   
<a class="navbar-brand" href="#">
	<img src="http://forsitefloodapp.xyz/Admin/CSS/Images/forsiteiconvar2.png" height="40">
		</a>
</nav>

<!-- First Parallax Image with Logo Text -->
<div class="bgimg-1 w3-display-container w3-opacity-min" id="home">
  <div class="w3-display-middle" style="white-space:nowrap;">
    <img src="foresite2.png" style="width:100%">
  </div>
</div>

<!-- Container (About Section) -->
<div class="row col-12 px-5" id="about">
 <h3 class="w3-center">ABOUT</h3>
  <p>ForeSite is a mobile application for flood monitoring via crowdsourcing which can be used by android users.
This application will let you report of flood incidents near in your area specifically in Mandaluyong City. 
The users will also be notify or inform whether parts of Mandaluyong are flooded. And lastly you can see also the map that shows the pinned point flooded areas.


     </p>
</div>

<!-- Container (Contact Section) -->
<div class="row col-12 px-5" id="contact">
  <h3>Contact Us</h3>  
  <div class="w3-row w3-padding-32 w3-section">
    <div class="w3-col m8 w3-panel">
      <div class="w3-large w3-margin-bottom">
        <i class="fa fa-map-marker fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i> Manila, Philippines<br>
        <i class="fa fa-envelope fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i>   Email: forsite@forsitefloodapp.xyz<br> <br />
      </div>
        </button>
      </form>
    </div>
  </div>
</div>
<!-- Footer -->
<footer class="page-footer font-small blue">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 bg-light">
    <a href="https://mdbootstrap.com/bootstrap-tutorial/">forsitefloodapp.xyz</a>
  </div>
  <!-- Copyright -->

</footer>
 

</body>
</html>
