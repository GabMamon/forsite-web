<?php
require_once 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
date_default_timezone_set('Asia/Manila');
$startDate = date('Y-m-d h:i:s', strtotime('06/01/2018 00:00:01'));
$endDate = date('Y-m-d h:i:s', strtotime('now'));

$startDateVal = date('Y-m-d', strtotime('07/01/2018'));
$endDateVal = date('Y-m-d', strtotime('now'));
//When the user clicked the show button
if(isset($_POST['Show'])){
    $emptyStartDate = empty($_POST['startDate']);
    $emptyEndDate = empty($_POST['endDate']);
    
    if($emptyStartDate || $emptyEndDate)
    {
        $error = "Input both start date and end date";
    }
    else
    {
    	//converting startDate and endDate to DATE
        $startDate = date('Y-m-d 00:00:01', strtotime($_POST['startDate'])); 
        $endDate = date('Y-m-d 23:59:59', strtotime($_POST['endDate']));
        
        $startDateVal = date('Y-m-d', strtotime($_POST['startDate']));
        $endDateVal = date('Y-m-d', strtotime($_POST['endDate']));
        
        if(!strcmp($_POST['hidden'],"ADMIN")==0){
            $accsslvl = $_POST['hidden'];
            $condtion = "$accsslvl";
            $contionz = "AND (`Barangay` = '$condtion')";
        }else{
            $condtion = "";
            $contionz = "";
        }

    }
}

$ActiveUSQL = "SELECT u.id,u.Firstname,u.Lastname,u.username,u.email,u.created_at,u.Active,l.LastDate,IFNULL(f.cnt,0) AS cnt,IFNULL(f.cntb,0) AS cntb,
                            IFNULL(f.cntv,0) AS cntv,IFNULL(f.cntvb,0) AS cntvb,
                            IFNULL(f.cntnv,0) AS cntnv,IFNULL(f.cntnvb,0) AS cntnvb,
                            IFNULL(f.cntiv,0) AS cntiv,IFNULL(f.cntivb,0) AS cntivb
                            FROM users u 
                            LEFT JOIN 
                            	(SELECT UserReported,COUNT(`FloodID`) AS cnt,SUM(CASE WHEN `Barangay` = '$condtion' THEN 1 ELSE 0 END) AS cntb,
                                 SUM(CASE WHEN `Status` = 'V' OR `Status` = 'V-D' THEN 1 ELSE 0 END) AS cntv, 
                                 SUM(CASE WHEN (`Status` = 'V' OR `Status` = 'V-D') AND (`Barangay` = '$condtion') THEN 1 ELSE 0 END) AS cntvb,
                                 
                                 SUM(CASE WHEN `Status` = 'NV' THEN 1 ELSE 0 END) AS cntnv,
                                 SUM(CASE WHEN (`Status` = 'NV') AND (`Barangay` = '$condtion') THEN 1 ELSE 0 END) AS cntnvb,
                                 
                                 SUM(CASE WHEN `Status` = 'IV' THEN 1 ELSE 0 END) AS cntiv,
                                 SUM(CASE WHEN (`Status` = 'IV') AND (`Barangay` = '$condtion') THEN 1 ELSE 0 END) AS cntivb
                                 
                                
                                 FROM flood_reports 
                                 WHERE (DateTime BETWEEN '$startDate' AND '$endDate') 
                                 GROUP BY UserReported) AS f 
                            ON u.email = f.UserReported 
                            INNER JOIN 
                            	(SELECT UserLogEmail,MAX(UserLogDate) AS LastDate FROM user_logs WHERE (UserLogDate BETWEEN '$startDateVal' AND '$endDateVal') 
                                 GROUP BY UserLogEmail) AS l 
                            ON u.email = l.UserLogEmail 
                            GROUP BY u.email";
                            
$AllUSQL = "SELECT u.id,u.Firstname,u.Lastname,u.username,u.email,u.created_at,u.Active,l.LastDate,IFNULL(f.cnt,0) AS cnt,IFNULL(f.cntb,0) AS cntb,
                            IFNULL(f.cntv,0) AS cntv,IFNULL(f.cntvb,0) AS cntvb,
                            IFNULL(f.cntnv,0) AS cntnv,IFNULL(f.cntnvb,0) AS cntnvb,
                            IFNULL(f.cntiv,0) AS cntiv,IFNULL(f.cntivb,0) AS cntivb
                            FROM users u 
                            LEFT JOIN 
                            	(SELECT UserReported,COUNT(`FloodID`) AS cnt,SUM(CASE WHEN `Barangay` = '$condtion' THEN 1 ELSE 0 END) AS cntb,
                                 SUM(CASE WHEN `Status` = 'V' OR `Status` = 'V-D' THEN 1 ELSE 0 END) AS cntv, 
                                 SUM(CASE WHEN (`Status` = 'V' OR `Status` = 'V-D') AND (`Barangay` = '$condtion') THEN 1 ELSE 0 END) AS cntvb,
                                 
                                 SUM(CASE WHEN `Status` = 'NV' THEN 1 ELSE 0 END) AS cntnv,
                                 SUM(CASE WHEN (`Status` = 'NV') AND (`Barangay` = '$condtion') THEN 1 ELSE 0 END) AS cntnvb,
                                 
                                 SUM(CASE WHEN `Status` = 'IV' THEN 1 ELSE 0 END) AS cntiv,
                                 SUM(CASE WHEN (`Status` = 'IV') AND (`Barangay` = '$condtion') THEN 1 ELSE 0 END) AS cntivb
                                 
                                
                                 FROM flood_reports 
                                 WHERE (DateTime BETWEEN '$startDate' AND '$endDate') 
                                 GROUP BY UserReported) AS f 
                            ON u.email = f.UserReported 
                            LEFT JOIN 
                            	(SELECT UserLogEmail,MAX(UserLogDate) AS LastDate FROM user_logs WHERE (UserLogDate BETWEEN '$startDateVal' AND '$endDateVal') 
                                 GROUP BY UserLogEmail) AS l 
                            ON u.email = l.UserLogEmail 
                            GROUP BY u.email";
                            
function getusers($CountSQL,$conn,$accslvl){
    $users = array();
    $res = mysqli_query($conn,$CountSQL);
		
		while ($row=mysqli_fetch_array($res)){
		    $active = "Inactive";
		    $cntb = "";
		    $cntvb = "";
		    $cntivb = "";
		    $cntnvb = "";
		    
		    if ($row['Active'] == '1'){
		        $active = "Verified";
		    }
		    if ($row['LastDate'] === NULL){
		        $lastlog = "Unavailable";
		    }else{
		        $lastlog = date_create($row['LastDate']);
		        $lastlog = date_format($lastlog,"M d Y");
		    }
		    
		    
		    if(!strcmp($accslvl,"ADMIN")==0){
              $cntb = ": ".$row['cntb'];
		      $cntvb = ": ".$row['cntvb'];
		      $cntivb = ": ".$row['cntivb'];
		      $cntnvb = ": ".$row['cntnvb'];
            };
		    
            $data = [
		        'IDNumber'=>"No:".$row['id'],
		        'Firstname'=>$row['Firstname'],
		        'Lastname'=>$row['Lastname'],
		        'Username'=>$row['username'],
		        'Email'=>$row['email'],
		        'Created_at'=>$row['created_at'],
		        'Account_Status'=>$active,
		        'Last_Login'=>$lastlog,
		        'Total_Rep'=>$row['cnt'],
		        'Valid_Rep'=>$row['cntv'],
		        'Invalid_Rep'=>$row['cntiv'],
		        'Unconfirm_Rep'=>$row['cntnv'],
		        
		        //PER BARANGAY
                    'Total_Rep_Brgy'=>$cntb,
		            'Valid_Rep_Brgy'=>$cntvb,
		            'Invalid_Rep_Brgy'=>$cntivb,
		            'Unconfirm_Rep_Brgy'=>$cntnvb
		        
		        ];
            array_push($users, $data);
		}
	return json_encode($users);
}

$usersdata = getusers($ActiveUSQL,$con,$accslvl);
$alldata = getusers($AllUSQL,$con,$accslvl);
?>

