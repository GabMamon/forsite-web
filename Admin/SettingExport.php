<?php
session_start();
 include 'DatabaseConfig.php';
 $conn = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
if (isset($_SESSION['stat'])){
    
    $start = $_SESSION["sdate"];
    $end = $_SESSION["edate"];
    
    if(isset($_SESSION["sdate"])&&isset($_SESSION["edate"])){

        $sql = "SELECT * FROM `flood_reports` WHERE `DateTime` BETWEEN '$start' AND '$end'"; 
        $query = mysqli_query($conn,$sql);
        
        if($query->num_rows > 0){
            $DateRange = $_SESSION["sdatename"]."-". $_SESSION["edatename"];
            
            $delimiter = ",";
            $filename = "Flood_Data_" . $DateRange . ".csv";
            
            //create a file pointer
            $f = fopen('php://memory', 'w');
            
            //set column headers
            $fields = array('FloodID','FloodLevel','Latitude','Longtitude','StreetNum','Street','Barangay','DateTime','UserReported','Status','Dup','RemarkCategory','Remarks','ImagePath');
            fputcsv($f, $fields, $delimiter);
            
            //output each row of the data, format line as csv and write to file pointer
            while($row = $query->fetch_assoc()){

                $lineData = array($row['FloodID'], $row['FloodLevel'], $row['Latitude'], $row['Longtitude'], $row['StreetNum'], $row['Street'], $row['Barangay'], $row['DateTime'], $row['UserReported'], $row['Status'], $row['Dup'], $row['RemarkCategory'], $row['Remarks'],$row['ImagePath']);
                fputcsv($f, $lineData, $delimiter);
            }
            
            //move back to beginning of file
            fseek($f, 0);
            
            //set headers to download file rather than displayed
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            
            //output all remaining data on a file pointer
            fpassthru($f);
        }
        exit;

        
        

        
    }
    else{
        header("history.go(-1)");
    }
}
else{
        header("Location: login.php");
    }
    
    ?>
						