<?php
session_start();
include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
?>
<!------ Include the above in your HEAD tag ---------->

<html>
<head>
	<style type="text/css">
		.wrap
{
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 99;
}

p.form-title
{
    font-family: 'Arial' , sans-serif;
    font-size: 30px;
    font-weight: 600;
    text-align: center;
    color: #FFFFFF;
    margin-top: 5%;
    text-transform: uppercase;
    letter-spacing: 4px;
}

form
{
    width: 450px;
    margin: 0 auto;
}

form.login input[type="text"], form.login input[type="password"]
{
    width: 100%;
    margin: 0;
    padding: 10px 15px;
    background: 0;
    border: 0;
    border-bottom: 1px solid #FFFFFF;
    outline: 0;
    font-style: italic;
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 1px;
    margin-bottom: 5px;
    color: #FFFFFF;
    outline: 0;
}

form.login input[type="submit"]
{
    width: 100%;
    font-size: 18px;
    text-transform: uppercase;
    font-weight: 500;
    margin-top: 16px;
    outline: 0;
    cursor: pointer;
    letter-spacing: 1px;
}

form.login input[type="submit"]:hover
{
    transition: background-color 0.5s ease;
}

form.login .forgot-pass-content
{
    min-height: 20px;
    margin-top: 10px;
    margin-bottom: 10px;
}
form.login .remember-forgot
{
	float: right;
    width: 100%;
    margin: 10px 0 0 0;
}
form.login label, form.login a
{
    font-size: 12px;
    font-weight: 400;
    color: #FFFFFF;
}

form.login a
{
    transition: color 0.5s ease;
}

form.login a:hover
{
    color: #2ecc71;
}

.pr-wrap
{
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 999;
    display: none;
}

.show-pass-reset
{
    display: block !important;
}

.pass-reset
{
    margin: 0 auto;
    width: 350px;
    position: relative;
    margin-top: 22%;
    z-index: 999;
    background: #FFFFFF;
    padding: 20px 15px;
}

.pass-reset label
{
    font-size: 14px;
    font-weight: 400;
    margin-bottom: 15px;
}

.pass-reset input[type="submit"]:hover
{
    transition: background-color 0.5s ease;
}
	</style>
<meta charset="utf-8">
<link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<title>Registration</title>
    
<body background="CSS\Images\blues.jpg">
<div class="jumbotron text-center">
  <img src="CSS\Images\forsiteiconadmin.png" class="avatar" height="100px">
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <?
                include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
                $securimage = new Securimage();
                

                if(isset($_POST['captcha_code'])){
                    if ($securimage->check($_POST['captcha_code']) == false) {
                      // the code was incorrect
                      // you should handle the error so that the form processor doesn't continue
                      // or you can use the following code if there is no validation or you do not know how
                      echo "The security code entered was incorrect.<br /><br />";
                      echo "Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
                      exit;
                    }else{
                        $user = mysqli_escape_string($con, $_POST['username']);
                        $fname = mysqli_escape_string($con, $_POST['fname']);
                        $lname = mysqli_escape_string($con, $_POST['lname']);
                        $contact = mysqli_escape_string($con, $_POST['contactno']);
                        $pass = mysqli_escape_string($con, $_POST['password']);
                        $regcode = mysqli_escape_string($con, $_POST['regcode']);
                        $brgy = mysqli_escape_string($con, $_POST['brgy']);
                        $hash_pass = password_hash($pass, PASSWORD_DEFAULT);
                        
                        $MatchSQL = "SELECT `RegCode` FROM `location` WHERE `LocName` = '$brgy'";
                        $result = mysqli_query($con, $MatchSQL);
	                    $row = mysqli_fetch_assoc($result);
	                    if(strcmp($regcode,$row['RegCode']) == 0){
	                        $FINDSQL = "SELECT * FROM `admin_official_users` WHERE (`aouusername` = '$user') OR (`aoucontactno` = '$contact')";
                            $findres = mysqli_query($con, $FINDSQL);
                            if(mysqli_num_rows($findres)>0){
                                echo "<div class=\"alert alert-warning\">
                                  <strong>Already Taken!</strong> Email/Username has already been taken.
                                  </br>Please go <a href='javascript:history.go(-1)'>back</a> and try again.
                                </div>";
                            }
                            else{
                                $INSERTSQL = "INSERT INTO admin_official_users (`aouusername`,`aoufname`,`aoulname`,`aoupassword`,`aoucontactno`,`aouaccesslevel`) VALUES('$user','$fname','$lname','$contact','$hash_pass','$brgy')";
    	                        mysqli_query($con, $INSERTSQL);
    	                        echo "<div class=\"alert alert-success\">
                                  <strong>Success!</strong> Registration Complete.
                                  </br>Click <a href='login'>here</a> to login.
                                </div>";
                            }
	                    }
	                    else{
	                        $Match2 = "SELECT `AdminCode` FROM `Settings` WHERE `Setting_ID` = 1";
                            $res2 = mysqli_query($con, $Match2);
                            $rw2 = mysqli_fetch_assoc($res2);
                                if(strcmp($regcode,$rw2['AdminCode']) == 0){
                                    $FINDSQL = "SELECT * FROM `admin_official_users` WHERE (`aouusername` = '$user') OR (`aoucontactno` = '$contact')";
                                    $findres = mysqli_query($con, $FINDSQL);
                                    if(mysqli_num_rows($findres)>0){
                                        echo "<div class=\"alert alert-warning\">
                                          <strong>Already Taken!</strong> Email/Username has already been taken.
                                        </div>";
                                    }
                                    else{
                                        $INSERTSQL = "INSERT INTO admin_official_users (`aouusername`,`aoufname`,`aoulname`,`aoupassword`,`aoucontactno`,`aouaccesslevel`) VALUES('$user','$fname','$lname','$hash_pass','$contact','ADMIN')";
                                    mysqli_query($con, $INSERTSQL);
                                    echo "<div class=\"alert alert-success\">
                                      <strong>Success!</strong> Registration Complete.
                                      </br>Click <a href='login'>here</a> to login.
                                    </div>";
                                    }
                                }
                                else{
                                    echo "<div class=\"alert alert-danger\">
                                          <strong>Invalid Code!</strong> Entered registration code is invalid.
                                          </br>Please go <a href='javascript:history.go(-1)'>back</a> and try again.
                                        </div>";
                                }
	                    }
	                    
                    }
                }else{
                    echo "Id not set.<br/>";
                }
                ?>
                <br><br>
            </div>
        </div>
    </div>
</div>
</body>
</head>
</html>