<?php
session_start();
include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
date_default_timezone_set('Asia/Manila');
$user = null;
$pass = null;
$accslvl = null;

$time = date('Y-m-d H:i:s',strtotime("now"));
$user = mysqli_escape_string($con, $_POST['username']);
$pass = mysqli_escape_string($con, $_POST['password']);

$SQL = "SELECT `aouid`,`aoufname`,`aoulname`,`aouusername`,`aoupassword`,`aouaccesslevel` FROM admin_official_users WHERE `aouusername`='$user'";
$result = mysqli_query($con, $SQL);
$numRows = mysqli_num_rows($result);
	$row = mysqli_fetch_assoc($result);   
    if($numRows  == 1){

		if(password_verify($pass,$row['aoupassword'])){

            $uid = $row['aouid'];
		    $usrname = $row['aouusername'];
		    $ufullname = $row['aoufname']." ".$row['aoulname'];
		    $access = $row['aouaccesslevel'];

                $_SESSION['id']=$uid;
                $_SESSION['fullname']=$ufullname;
                $_SESSION['username']=$usrname;
                $_SESSION['accesslevel']=$access;
                $_SESSION['stat']="OK";
                
		        header("Location: admin-home");
		        die();
			
		}
		else{
		    $_SESSION['LoginStat'] = "YES";
		    header("Location: login");
			die();
		}
    }
    else{
		$_SESSION['LoginStat'] = "ACE";
		header("Location: login");
	    die();
	}


?>
