<?php
session_start();
include 'DatabaseConfig.php';
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
$time = date('Y-m-d H:i:s');

if(isset($_SESSION['connect'])){
    unset($_SESSION['connect']);
    if(isset($_GET['fid'])){
    
        $adminuser = $_SESSION['usr'];
        $FloodID = mysqli_real_escape_string($con,$_GET['fid']);
        $Choice = mysqli_real_escape_string($con,$_GET['choice']);
        $Remarks = mysqli_real_escape_string($con,$_GET['remarks']);
        
        $searchsql = "SELECT * FROM `flood_reports` WHERE `Dup` =  '$FloodID'";
        $result = mysqli_query($con,$searchsql);
        $numRows = mysqli_num_rows($result);
 
        if($numRows  >= 1){
            $upsql = "UPDATE flood_reports SET `Status` = 'V', `Remarks` = '$Remarks' WHERE `Dup` = '$FloodID'";
            if(mysqli_query($con,$upsql)){
                $auditsql = "INSERT INTO `audit_logs` (`audituser`,`audittime`,`auditdetails`,`auditremarks`)
                VALUES ('$adminuser','$time','Validated report #$FloodID ($Choice)','$Remarks')";
                mysqli_query($con,$auditsql);
                
                $_SESSION['disres'] = "success";
                header("location:floodreports");
            }else{
                $_SESSION['disres'] = "failed";
                header("location:floodreports");
            }
        }else{
            echo "notfound";
        }
    }
}
else{
    header("location:javascript://history.go(-1)");
}



?>