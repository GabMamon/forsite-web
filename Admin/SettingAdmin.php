<?php
session_start();
 include 'DatabaseConfig.php';
 $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
if (isset($_SESSION['stat'])){
  $uid = $_SESSION['id'] ;
  $ufullname = $_SESSION['fullname'] ;
  $usrname = $_SESSION['username'];
  $accslvl= $_SESSION['accesslevel'];
    if(strcmp($accslvl,"ADMIN")==0){
        
        $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
         $result = mysqli_query($con,$Sql_Query);
         $row = mysqli_fetch_assoc($result);
         $a = $row['LockDownTime'];
         $s = $row['Expiretime'];
         $d = $row['Catcharea'];
         $f = $row['ReportThreshold'];
         $g = $row['ReportStatus'];
         
         $Stat = "";
         if (isset($_SESSION['SettingStat'])){
            $Stat = $_SESSION['SettingStat'];
            }
         
         if ($Stat == "Invalid") {
	        $message = "Invalid password.";
            }
        elseif ($Stat == "Success") {
	        $message = "Changes saved.";
        }else{
            $message = "";
        }
        unset($_SESSION['SettingStat']);
         
?>
<html>
	<head>
	    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="CSS/simple-sidebar.css" rel="stylesheet">

		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
		
		
							<title>
      Forsite Admin | System Configuration
  </title>
							<style type="text/css">
      .navbar-brand{
          margin-left: 5px;
          margin-right: 15px;
      }
      .username{
          margin-right: 40px;
      }
      .nav-link[data-toggle].collapsed:after {
        content: "▴";▴
      }
        .nav-link[data-toggle]:not(.collapsed):after {
        content: "▾";
      }
      .card {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
        width:100%;
        }
      #Save{
          font-size:16px;
          padding:5px 100px;
      }
      span { 
    	width: 75px;
    }
    #alert {
        z-index: 10;
	    position: fixed;
	    width: 100%;
        height:50px;
        left:50%;
        transform:translateX(-50%);
        bottom:10px;
    }
  </style>
						</head>
						<body>
							<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
							    <a href="#menu-toggle" class="btn btn-outline-secondary" id="menu-toggle">
									<i class="fa fa-bars"></i>
								</a>
								<a class="navbar-brand" href="http://forsitefloodapp.xyz/Admin/admin-home">
									<img src="https://forsitefloodapp.000webhostapp.com/Admin/CSS/Images/forsiteiconvar2.png" height="40">
									</a>
									<span class="navbar-text">
									System Settings
								</span>
									<div class="collapse navbar-collapse" id="navbarText">
										<ul class="navbar-nav ml-auto">
											<span class="navbar-text username">
												<i class="fa fa-user"></i> Hello <?php echo $ufullname; ?> 
											</span>
											<li class="nav-item">
												<a type="button" class="btn btn-outline-secondary" href="logout">Log-out <i class="fa fa-sign-out"></i></a>
											</li>
										</ul>
									</div>
								</nav>
								<div id="wrapper">
									<div id="sidebar-wrapper">
										<ul class="sidebar-nav nav-pills">
											
											</li>
											<li class="nav-item">
												<a href="admin-home" class="nav-link rounded-0">Dashboard</a>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#CurReportSub" data-toggle="collapse">Flood Monitoring</a>
												    <div class="collapse" id="CurReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodmap">Flood Map</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="floodreports">Flood Incidents</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<li class="nav-item">
												<a class="nav-link rounded-0" href="#ReportSub" data-toggle="collapse">Reports</a>
												    <div class="collapse" id="ReportSub">
                                                        <ul class="flex-column pl-2 nav">
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rflood">Flood Reports</a></li>
                                                            <li class="nav-item"><a class="nav-link rounded-0" href="Rusers">User Reports</a></li>
                                                        </ul>
                                                    </div>
											</li>
											<li class="nav-item">
												<a href="SettingsAdmin" class="nav-link rounded-0 active">Settings</a>
											</li>
										</ul>
									</div>
									<div id="page-content-wrapper">
										<div class="container-fluid">
										    <div class="row col-12">
										        <div class="card">
										            <div class="card-header"><h5>System Configuration</h5></div>
										            <div class="card-body px-5">
										                <h5>Reporting Settings</h5>
										                <hr>
<div class="row">
 <div class="col-6">
<form method="POST" action="ProsSetting">
  <div class="form-group row col-6">
    <label for="RStat" class="col-12 col-form-label">Reporting Status:</label> 
    <div class="col-6">
      <select id="RStat" name="RStat" class="custom-select">
        <option value="AUT">Auto</option>
        <option value="ON">On</option>
        <option value="OFF">Off</option>
      </select>
    </div>
  </div>
  <div class="form-group row col-6">
    <label for="Prtl" class="col-12 col-form-label">Per report time limit:</label> 
    <div class="col-12">
      <div class="input-group">
        <input id="Prtl" name="Prtl" class="form-control here" required="required" type="number" min="0" max = "240" value="<?echo $a;?>"> 
        <div class="input-group-append"><span class="input-group-text">min/s</span></div>
      </div>
    </div>
  </div>
  <div class="form-group row col-6">
    <label for="Rtr" class="col-12 col-form-label">Reports time range:</label> 
    <div class="col-12">
      <div class="input-group">
        <input id="Rtr" name="Rtr" class="form-control here" required="required" type="number" min="1" max = "240" value="<?echo $s;?>"> 
        <div class="input-group-append"><span class="input-group-text">hour/s</span></div>
      </div>
    </div>
  </div>
  <div class="form-group row col-6">
    <label for="Rgt" class="col-12 col-form-label">Report grouping distance:</label> 
    <div class="col-12">
      <div class="input-group">
        <input id="Rgt" name="Rgt" class="form-control here" required="required" type="number" min="5" max = "500" value="<?echo $d;?>"> 
        <div class="input-group-append"><span class="input-group-text">meter/s</span></div>
      </div>
    </div>
  </div>
  
  <div class="form-group row col-6">
    <label for="text" class="col-12 col-form-label">Auto validation threshold:</label> 
    <div class="col-12">
      <div class="input-group">
        <input id="Avt" name="Avt" class="form-control here" required="required" type="number" min="2" max = "99" value="<?echo $f;?>"> 
        <div class="input-group-append">
            <span class="input-group-text">reports</span>
      </div>
    </div>
  </div> 
  
										            </div>
										            <div class="form-group row col-4">
    <label for="Rgt" class="col-12 col-form-label">Admin Password:</label> 
    <div class="col-12">
      <div class="input-group">
        <input id="Pss" name="Pss" class="form-control here" required="required" type="password"> 
        
      </div>
    </div>
    <div class="form-group row col-6">
    <div class="offset-6 col-12">
        </br>
      <center>
										                <button id="Save" class="btn btn-success btn-sm">
										                    <i class="fa fa-check"></i> Save
										                </button>
										            </center>
    </div>
  </div>
  </div>
</form>
</div>   

 <div class="col-6">
     <h4>Import/Export/Delete</h4><br>
     
     <button id="Export" class="btn btn-primary btn-sm"><i class="fa fa-bar-chart"></i> Check Imported Data</button><br><br>
 </div>
</div>

										           
										        </div>
										    </div>
										</div>
									</div>
									<!-- /#page-content-wrapper -->
								</div>
								<!-- /#wrapper -->
								<div id="alert" class='container'>
								<script>
								function checkDismissAlert(){
        var alerts = "<?php echo $message; ?>";
        if(alerts==="Invalid password."){
            var MsgString = alerts;
            var alrtclr = "alert-danger";
            showAlert(MsgString,alrtclr);
        }else if(alerts==="Changes saved."){
            var MsgString = "Changes saved.";
            var alrtclr = "alert-success";
            showAlert(MsgString,alrtclr);
            
        }else{
            var MsgString = "";
            var alrtclr = "";
        }
    }
    var selop = <? echo "'$g'"; ?>

    $(function() {
        $("#RStat").val(selop);
    });
    
     function showAlert(message,color) {
          $('#alert').html("<div class='alert "+color+" alert-dismissible fade show' role=\"alert\">"+message+"<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>");
          $('#alert').show();
    }
    
    window.onload = checkDismissAlert;
								
        $(document).ready(function() {
$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");

    });
});
								</script>
    <?php }
    else{
        echo "Not Admin";
    }
}
else{
        header("Location: login.php");
    }
    
    ?>
							</body>
						</html>