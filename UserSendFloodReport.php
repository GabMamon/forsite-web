<?php
 include 'DatabaseConfig.php';
 $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 date_default_timezone_set('Asia/Manila');
 
 $Sql_Query = "SELECT * FROM `Settings` WHERE `Setting_ID` = 1 ";
 
 $re = mysqli_query($con,$Sql_Query);
 $ro = mysqli_fetch_assoc($re);
 
 $expiretime = $ro['Expiretime'];
 $reportthreshold = $ro['ReportThreshold'];
 $catcharea = $ro['Catcharea'];
 $timeloc  = $ro['LockDownTime']; 
 $Barangay = "";
 $SendStatus = false;
 $Response2 = "";
 
 $stime = date('Y-m-d H:i:s', strtotime("-$expiretime hours"));
 $time = date('Y-m-d H:i:s');
 
function geoDistance($lat1, $lon1, $lat2, $lon2) {
   $R = 6371; // Radius of the earth in km
   $dLat = deg2rad($lat2 - $lat1);
   $dLon = deg2rad($lon2 - $lon1);
   $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLon/2) * sin($dLon/2);
   $c = 2 * atan2(sqrt($a), sqrt(1-$a));
   $d = $R * $c; // Distance in km
   $e = $d * 1000; //to meters
   return $e;
}

function getBarangay($lat, $lon){
    $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false&key=AIzaSyCRMkhf9cJXrTd5gS-vPmEZmVQunGemnPg";
    $json = @file_get_contents($url);
    $data = json_decode($json,true);
    $status = $data['status'];
    $street_number=null;
    
    if($status == "OK"){
        $street_number = $data['results'][3]['address_components'][0]['long_name'];
        switch($street_number){
            case "Barangay Burol":
                $street_number = "Burol";
                break;
            case "Wack-wack Greenhills":
                $street_number = "Wack-Wack";
                break;
            case "Mandaluyong":
                $street_number = $data['results'][4]['address_components'][0]['long_name'];
                break;
        }
    }else{
        $url2 = "https://us1.locationiq.com/v1/reverse.php?key=fe632c5ed9a492&lat=$lat&lon=$lon&format=json";
        $json2 = @file_get_contents($url2);
        $data2 = json_decode($json2,true);
        $street_number = $data2['address']['suburb'];
    }
    return $street_number;
}

if(isset($_POST["ReportStatus"])) {

 $Sender = mysqli_real_escape_string($con,$_POST['Sender']);
 $Floodlevel = mysqli_real_escape_string($con,$_POST['FloodLevel']);
 $Lat = mysqli_real_escape_string($con,$_POST['Latitude']);
 $Lon = mysqli_real_escape_string($con,$_POST['Longitude']);
 $Streetnum= mysqli_real_escape_string($con,$_POST['StreetNum']);
 $Street = mysqli_real_escape_string($con,$_POST['Street']);
 $ImgData = $_POST['ImgData'];
 $ImgName = $_POST['ImgName'];
 $ImgStatus = mysqli_real_escape_string($con,$_POST['ImgStatus']);
 $ReportCategory = mysqli_real_escape_string($con,$_POST['ReportCategory']);
 $ReportInfo = mysqli_real_escape_string($con,$_POST['ReportInfo']);
 
 //---------------------------------------------------------------------------------------
 
 $SQL1 = "SELECT `StreetNum`,`Street`,`Latitude`,`Longtitude`,`Barangay`,`DateTime`,`Status` FROM `flood_reports` WHERE `UserReported` = '$Sender' ORDER BY `DateTime` DESC LIMIT 1"; 
    // get users last report
 $result = mysqli_query($con,$SQL1);
 $numRows = mysqli_num_rows($result);

 if($numRows  == 1) {
    $row = mysqli_fetch_assoc($result);
    $laststatus = $row["Status"];
    $laststreetnum = $row["StreetNum"];
    $laststreet = $row["Street"];
    $lastbrgy = $row["Barangay"];  
    $lastlat = $row["Latitude"];
    $lastlon = $row["Longtitude"];
    $lastdatetime = $row["DateTime"];
    
    if (strpos($Sender, 'User') !== false) {
        $timeloc*3;
    }
       
    //Checks if user last report falls within time limit
    if ($time>=date('Y-m-d H:i:s', strtotime($lastdatetime.sprintf("+%d minutes", $timeloc)))){
    //If user resends the same report on same location
    
        $d = geoDistance($Lat,$Lon,$lastlat, $lastlon);
            
        if($d<=$catcharea&&strcmp($Street,$laststreet)==0&&$time<date('Y-m-d H:i:s', strtotime($lastdatetime.sprintf("+%d hours", $expiretime)))){
            if(strcmp($laststatus,"V")==0){
                $Response = "VALID"; //Inform user that his/her report is validated;
            }
            else{
                $Response = "LIMIT1"; 
                }
        }
        else {
            $Response = "PASS";
        }
    }
    else{
        $Response = "LIMIT";
    }

 }
 else{
    if($Floodlevel == "null" || $Lat == "null" || $Lon == "null" || $Sender == "null" || $Streetnum == "null" || $Street == "null"){
        $Response = 'CORRUPTED REPORT';
    }
    else{
        $Response = "PASS";  
    }
 }
 
 if($Response === "PASS"){
    $ReplaceCheck = "NONE";
    $DuplicateCheck = false;
    $Barangay = getBarangay($Lat,$Lon);
    
    $ZQL = "SELECT * FROM flood_reports WHERE (`Street` = '$Street' AND `Barangay` = '$Barangay' AND `FloodLevel` = '$Floodlevel') AND (`DateTime` BETWEEN '$stime' AND '$time') AND (`FloodID` = `Dup`) ORDER BY `DateTime` LIMIT 1";
    $rez = mysqli_query($con,$ZQL);
    $nmrwz = mysqli_num_rows($rez);
    
    $PREV = "SELECT * FROM flood_reports WHERE (`Street` = '$Street' AND `Barangay` = '$Barangay' AND `FloodLevel` != '$Floodlevel') AND (`DateTime` BETWEEN '$stime' AND '$time') AND (`FloodID` = `Dup`) ORDER BY `DateTime` LIMIT 1";
    $prv = mysqli_query($con,$PREV);
    $Pmrwz = mysqli_num_rows($prv);
    
    if($nmrwz > 0){
        $rwz = mysqli_fetch_assoc($rez);
        $f = geoDistance($Lat ,$Lon ,$rwz["Latitude"], $rwz["Longtitude"]);
        
        if($Pmrwz > 0){
        $pwz = mysqli_fetch_assoc($prv);
        $h = geoDistance($Lat ,$Lon ,$pwz["Latitude"], $pwz["Longtitude"]);
            if($h<=$catcharea){
                if($pwz["Status"] === "V" ){
                    $ReplaceCheck = $pwz['Dup'];
                }
            }
        }
        
        if($f<=$catcharea){
            if($rwz["Status"] === "V" ){
                $Response2 = "LIMIT2";
                $DuplicateReport = "";
                $SendStatus = false;
            }

        else if($rwz["Status"] === "NV") {
            $DuplicateCheck = true;
            $DuplicateReport = $rwz['Dup'];
            $SendStatus = true;
        }

        else {
            $DuplicateCheck = false;
            $DuplicateReport = "";
            $SendStatus = true;
        }
    }

	    else {
		    $DuplicateCheck = false;
		    $DuplicateReport = "";
		    $SendStatus = true;
	    }
    }
    else {
        $DuplicateCheck = false;
	    $DuplicateReport = "";
	    $SendStatus = true;
    }
}
 else{
     echo $Response2;
}

 if($SendStatus){
    if($ImgData === ""||is_null($ImgData)||$ImgData === "NONE"){
        $Sql_Query = "INSERT INTO flood_reports (`FloodLevel`,`Latitude`,`Longtitude`,`StreetNum`,`Street`,`Barangay`,`DateTime`,`UserReported`,`RemarkCategory`,`Remarks`) VALUES('$Floodlevel','$Lat','$Lon','$Streetnum','$Street','$Barangay','$time','$Sender','$ReportCategory','$ReportInfo')";
     
    }else{
        $ImagePath = "Images/Useruploaded/$ImgName.jpg";
        file_put_contents($ImagePath,base64_decode($ImgData));
        $TruePath = "http://forsitefloodapp.xyz/".$ImagePath;
        $Sql_Query = "INSERT INTO flood_reports (`FloodLevel`,`Latitude`,`Longtitude`,`StreetNum`,`Street`,`Barangay`,`DateTime`,`UserReported`,`RemarkCategory`,`Remarks`,`ImagePath`) VALUES('$Floodlevel','$Lat','$Lon','$Streetnum','$Street','$Barangay','$time','$Sender','$ReportCategory','$ReportInfo','$TruePath')";
    }
    $Update_Query = "";
    if($DuplicateCheck){
        $Update_Query = "UPDATE flood_reports SET `Dup`='$DuplicateReport' WHERE `Dup` IS NULL;";
    }
    else {
        $Update_Query = "UPDATE flood_reports SET `Dup`=`FloodID` WHERE `Dup` IS NULL;";
    }

    if(mysqli_query($con,$Sql_Query)) {
        mysqli_query($con,$Update_Query);
        $ValidateSQL="SELECT `FloodID`,COUNT(`Dup`) AS AsOf FROM flood_reports WHERE (`Dup` = '$DuplicateReport') AND `Status` = 'NV' GROUP BY `Dup`";
        $VS = mysqli_query($con,$ValidateSQL);
        $numRows = mysqli_num_rows($VS);
          
        $count = 0;
        if($numRows  >= 1){
            $row = mysqli_fetch_assoc($VS);
            $count = $row["AsOf"];
        }
          
        //Auto validate
        if($count>=$reportthreshold) {
            $AutoValidate="UPDATE flood_reports SET `Status` = 'V' WHERE `Dup` = '$DuplicateReport'";
            if(mysqli_query($con,$AutoValidate)){
                $time = date('Y-m-d H:i:s');
                $auditsql = "INSERT INTO `audit_logs` (`audituser`,`audittime`,`auditdetails`,`auditremarks`) VALUES ('SYSTEM','$time','Validated report #$DuplicateReport','Auto validate')";
                mysqli_query($con,$auditsql);
                
                if(!strcmp($ReplaceCheck,"NONE") == 0){
                    $DismissPrev = "UPDATE flood_reports SET `Status` = 'V-D' WHERE `Dup` = '$ReplaceCheck'";
                    mysqli_query($con,$DismissPrev);
                    
                    $auditsql = "INSERT INTO `audit_logs` (`audituser`,`audittime`,`auditdetails`,`auditremarks`) VALUES ('SYSTEM','$time','Dismissed report #$ReplaceCheck','Auto validate')";
                    mysqli_query($con,$auditsql);
                }
            }
        }

    echo "SUCCESS";
    }
    
     
 }
 else{
     echo $Response;
 }

}

mysqli_close($con);

?>