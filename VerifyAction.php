<? 
session_start();

?>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="CSS/Images/forsiteiconsolo1.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title>Sign up</title>
    
</head>
    <body background="Admin\CSS\Images\blues.jpg">
    <div class="jumbotron text-center">
        <img src="Admin\CSS\Images\forsiteiconvar2.png" class="avatar" height="75px">
    </div>
    </br></br>
    <div class="container-fluid">
    <div class="row col-12">
    <div class="card w-100">
        <div class="card-body">
    <?php
         
        include 'DatabaseConfig.php';
        $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

        
        include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
        $securimage = new Securimage();
        
        if(isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            if(isset($_POST['captcha_code'])){
                if ($securimage->check($_POST['captcha_code']) == false) {
                  // the code was incorrect
                  // you should handle the error so that the form processor doesn't continue
                  // or you can use the following code if there is no validation or you do not know how
                  echo "The security code entered was incorrect.<br /><br />";
                  echo "Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
                  exit;
                }else{
                $updtsql="UPDATE users SET active='1', hash='' WHERE id='".$id."' AND active='0'";
                mysqli_query($con,$updtsql);
                
                echo "Your account has been succesfully activated, you may now login.<br/>";
                    
                }
            }
        }else{
            
        }
        
    ?>
     
 
        </div>
    </div>
    </div>
    </div>
</body>
</html>